# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: inventario
# Generation Time: 2020-09-08 08:20:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table almacen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `almacen`;

CREATE TABLE `almacen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` varchar(50) NOT NULL,
  `fecha` date NOT NULL DEFAULT '2019-01-01',
  `stock` decimal(10,0) NOT NULL,
  `id_users` int(11) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_producto`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `almacen` WRITE;
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;

INSERT INTO `almacen` (`id`, `id_producto`, `fecha`, `stock`, `id_users`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'1','2020-08-16',20,1,0,'2020-08-17 02:21:43','2020-08-17 02:21:43'),
	(2,'2','2020-08-19',10,1,0,'2020-08-19 04:10:27','2020-08-19 04:10:27'),
	(3,'3','2020-08-19',5,1,0,'2020-08-19 20:56:36','2020-08-19 20:56:36'),
	(4,'5','2020-08-28',6,13,0,'2020-08-29 00:14:25','2020-08-29 00:14:25'),
	(5,'6','2020-08-28',5,1,0,'2020-08-29 02:26:25','2020-08-29 02:26:25');

/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `telefono` varchar(18) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `direccion` text NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;

INSERT INTO `cliente` (`id`, `nombre`, `telefono`, `correo`, `direccion`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'juan lopez yana','7894661','juan@gmail.com','C.los tajivos N°12',0,'2020-08-13 04:01:56','2020-08-13 04:01:56'),
	(2,'diego cruz gutierrez','7856123','diego@gmail.com','C. Grau entre Laos N°78',0,'2020-08-19 22:46:45','2020-08-19 22:46:04'),
	(3,'juan perez alanoca','78946133','juan@gmail.com','C. Uruguay entre los Palmos N°4',0,'2020-08-25 22:34:28','2020-08-25 22:29:17'),
	(4,'Ducelia','78946513','dulce@hotmail.com','Av. Puerto Escobar N°56',0,'2020-08-25 22:35:56','2020-08-25 22:35:44'),
	(5,'juan alberto gutierres','79846512','alberto@gmail.com','C. Uruguay entre los Palmos N°4',0,'2020-08-27 04:24:40','2020-08-25 22:50:28'),
	(6,'favian','7946513','favi@hotmail.com','C. atalamos N°99',0,'2020-08-27 04:27:06','2020-08-25 23:06:52'),
	(7,'ailyn condori ticona','78945612','condori@hotmail.com','C. cruz papal entre el Tam N°59',0,'2020-08-27 04:26:05','2020-08-25 23:12:00'),
	(8,'juan lopez hidalgo','78945612','juan@hotmail.com','C. Los tajivos entre pacajes N°12',0,'2020-08-26 02:05:00','2020-08-26 02:05:00'),
	(9,'Luis Callisaya Kuno','78945645','luis@gmailcom','C.Ayacucho N°45',0,'2020-08-27 04:08:03','2020-08-27 04:08:03'),
	(10,'Carlos Gutierrez Alanca','78945233','carlos@gmail.com','C. Tiquina N°78',0,'2020-08-29 02:27:45','2020-08-29 02:27:45');

/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertcliente` BEFORE INSERT ON `cliente` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"cliente");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updatecliente` BEFORE UPDATE ON `cliente` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' ;Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"cliente");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"cliente",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deletecliente` AFTER DELETE ON `cliente` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"cliente");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table compras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `compras`;

CREATE TABLE `compras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `id_users` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`id_producto`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table empresa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `red_social` varchar(50) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `telefono` int(30) DEFAULT NULL,
  `direccion` varchar(150) NOT NULL,
  `logo` varchar(36) DEFAULT 'default.png',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;

INSERT INTO `empresa` (`id`, `nombre`, `red_social`, `correo`, `telefono`, `direccion`, `logo`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'VictoryM Bolivia','victory motors','victorymotors012@gmail.com',24456128,'Av. Juan Pablo II frente al TAM','1572117248victorymotors.png',1,'2020-08-29 02:20:04','2019-09-28 19:13:03'),
	(2,'victoryMotors','victorymotor','victory@gmail.com',78945122,'Av, juan palo II','159841064702-KEYTON-CRISCAR-1.png',1,'2020-08-27 06:17:48','2020-08-26 01:58:01'),
	(3,'victoryMotors','victorymotor','victory@gmail.com',78945122,'Av, juan palo II','159840708004-KING-LONG-CRISCAR.png',1,'2020-08-26 02:57:11','2020-08-26 01:58:02'),
	(4,'Vcitory','victory','victory@gmail.com',78945612,'En la ruz papal frente al Tam','159865347002-KEYTON-CRISCAR-1.png',1,'2020-08-28 22:25:13','2020-08-28 22:24:32'),
	(5,'vicrtory','victory','victory@gmail.com',78945122,'En la ruz papal frente al Tam','159866758304-KING-LONG-CRISCAR.png',0,'2020-08-29 02:19:45','2020-08-29 02:19:45');

/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertempresa` BEFORE INSERT ON `empresa` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;red_social=',ifnull(new.red_social,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"empresa");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateempresa` BEFORE UPDATE ON `empresa` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;red_social=',ifnull(old.red_social,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;red_social=',ifnull(new.red_social,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.red_social <> new.red_social THEN set @res = CONCAT (@res, ' ;Cambió el valor de red_social: ',ifnull(OLD.red_social,''), ' a: ',ifnull(new.red_social,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' ;Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"empresa");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"empresa",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteempresa` AFTER DELETE ON `empresa` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;red_social=',ifnull(old.red_social,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"empresa");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table expedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `expedido`;

CREATE TABLE `expedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `expedido` WRITE;
/*!40000 ALTER TABLE `expedido` DISABLE KEYS */;

INSERT INTO `expedido` (`id`, `nombre`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(5,'BN',1,0,'2019-10-22 01:36:16','2019-10-22 01:36:16'),
	(2,'CBBA',1,0,'2019-10-22 01:35:54','2019-10-22 01:35:54'),
	(1,'LP',1,0,'2019-10-22 01:36:04','2019-09-27 23:43:25'),
	(3,'OR',1,0,'2019-10-22 01:35:54','2019-10-22 01:35:54'),
	(4,'PD',1,0,'2019-10-22 01:36:16','2019-10-22 01:36:16'),
	(9,'PT',1,0,'2019-10-22 01:36:55','2019-10-22 01:36:55'),
	(7,'SC',1,0,'2019-10-22 01:36:31','2019-10-22 01:36:31'),
	(6,'STC',1,0,'2019-10-22 01:36:31','2019-10-22 01:36:31'),
	(8,'TJ',1,0,'2019-10-22 01:36:55','2019-10-22 01:36:55');

/*!40000 ALTER TABLE `expedido` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table fabrica
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fabrica`;

CREATE TABLE `fabrica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `fabrica` WRITE;
/*!40000 ALTER TABLE `fabrica` DISABLE KEYS */;

INSERT INTO `fabrica` (`id`, `nombre`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(2,'asddsa',1,0,'2019-09-28 15:43:35','2019-09-27 00:43:49'),
	(6,'Camioneta',1,0,'2020-08-29 02:28:23','2020-08-29 02:28:23'),
	(4,'Kinglong',1,0,'2020-08-19 03:49:58','2020-08-19 03:49:58'),
	(3,'Minibus',1,0,'2020-08-19 04:02:36','2019-09-28 20:04:39'),
	(5,'TKing',1,0,'2020-08-27 05:11:45','2020-08-27 05:11:45'),
	(1,'Vagoneta',1,0,'2020-08-19 04:02:58','2019-09-27 00:43:06');

/*!40000 ALTER TABLE `fabrica` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table grupo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `icos` varchar(25) NOT NULL DEFAULT 'ico- file',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;

INSERT INTO `grupo` (`id`, `nombre`, `icos`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(2,'Adminstrador','fa-cog',1,0,'2020-08-14 02:20:00','2019-09-28 11:57:08'),
	(6,'Alamcen','fa-list',1,0,'2020-08-28 20:30:53','2020-08-28 20:30:53'),
	(10,'Empresa','fa-table',1,0,'2020-08-28 22:40:23','2020-08-28 22:40:23'),
	(1,'Grupo Menu','wi-file',1,0,'2020-08-19 03:39:23','2019-09-28 11:56:24'),
	(5,'Productos','fa-table',1,0,'2019-09-28 15:39:12','2019-09-28 15:39:12'),
	(4,'Reportes','fa-table',1,0,'2019-09-28 15:38:55','2019-09-28 15:38:55'),
	(7,'Stock','fa-edit',1,0,'2020-08-28 20:31:23','2020-08-28 20:31:23'),
	(9,'Usuarios','fa-table',1,0,'2020-08-28 22:39:32','2020-08-28 22:39:32'),
	(8,'Ventas','fa-list',1,0,'2020-08-28 20:31:52','2020-08-28 20:31:52');

/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table inventario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventario`;

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` varchar(50) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `stock` decimal(10,0) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_producto`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;

INSERT INTO `inventario` (`id`, `id_producto`, `precio`, `stock`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'1',14800.00,6,1,0,'2020-08-30 03:47:17','2020-08-17 04:19:09'),
	(2,'2',17500.00,6,1,0,'2020-08-29 02:30:04','2020-08-19 04:11:08'),
	(3,'3',59800.00,4,1,0,'2020-08-19 21:04:00','2020-08-19 21:04:00'),
	(4,'5',79800.00,5,1,0,'2020-08-29 00:15:42','2020-08-29 00:15:42'),
	(5,'6',78560.00,0,1,0,'2020-08-30 03:54:21','2020-08-29 02:30:52');

/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertinventario` BEFORE INSERT ON `inventario` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;precio=',ifnull(new.precio,''),
    ' ;stock=',ifnull(new.stock,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"inventario");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateinventario` BEFORE UPDATE ON `inventario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;precio=',ifnull(old.precio,''),
    ' ;stock=',ifnull(old.stock,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;precio=',ifnull(new.precio,''),
    ' ;stock=',ifnull(new.stock,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.id_producto <> new.id_producto THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_producto: ',ifnull(OLD.id_producto,''), ' a: ',ifnull(new.id_producto,'')); END IF;
	IF OLD.precio <> new.precio THEN set @res = CONCAT (@res, ' ;Cambió el valor de precio: ',ifnull(OLD.precio,''), ' a: ',ifnull(new.precio,'')); END IF;
	IF OLD.stock <> new.stock THEN set @res = CONCAT (@res, ' ;Cambió el valor de stock: ',ifnull(OLD.stock,''), ' a: ',ifnull(new.stock,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"inventario");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"inventario",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteinventario` AFTER DELETE ON `inventario` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;precio=',ifnull(old.precio,''),
    ' ;stock=',ifnull(old.stock,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"inventario");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `old` text,
  `new` text,
  `valor_alterado` text,
  `usuario` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `fecha` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  `eliminado` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `url` varchar(40) NOT NULL,
  `icos` varchar(25) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`,`url`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` (`id`, `nombre`, `titulo`, `url`, `icos`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(13,'almacen','Almacen','almacen','fa-table',1,0,'2019-09-28 16:39:48','2019-09-28 16:39:48'),
	(4,'cliente','Lista Cliente','cliente','fa-table',1,0,'2019-09-28 14:20:48','2019-09-28 14:20:48'),
	(7,'compras','Compras','compras','fa-table',1,0,'2019-09-28 14:21:50','2019-09-28 14:21:50'),
	(15,'empresa','Empresa','empresa','fa-table',1,0,'2019-09-28 16:40:22','2019-09-28 16:40:22'),
	(8,'fabrica','Listar Categoria','fabrica','fa-table',1,0,'2020-08-19 04:01:20','2019-09-28 14:22:13'),
	(5,'grupo','Grupo Menu','grupo','fa-table',1,0,'2019-09-28 14:21:10','2019-09-28 14:21:10'),
	(14,'inventario','Stock','inventario','fa-table',1,0,'2019-10-06 20:23:00','2019-09-28 16:40:07'),
	(3,'menu','Menu','menu','fa-table',1,0,'2019-09-28 12:07:21','2019-09-28 12:07:21'),
	(12,'perfil','perfil Usuario','perfil','fa-table',1,1,'2019-09-28 20:11:20','2019-09-28 16:39:22'),
	(10,'permiso','Permisos','permiso','fa-table',1,0,'2019-09-28 16:35:56','2019-09-28 14:22:51'),
	(1,'persona','Listar Persona','persona','fa-table',1,0,'2019-09-28 12:03:49','2019-09-28 12:03:30'),
	(6,'producto','Lista Producto','producto','fa-table',1,0,'2019-09-28 14:21:33','2019-09-28 14:21:33'),
	(2,'proveedor','Proveedor','proveedor','fa-table',1,0,'2019-09-28 13:36:30','2019-09-28 12:06:17'),
	(17,'reporte','Reportes','reporte','fa-file',1,0,'2020-08-13 21:32:41','2020-08-13 21:32:41'),
	(11,'rol','Rol/Categoria','rol','fa-table',1,0,'2020-08-28 21:50:20','2019-09-28 15:28:32'),
	(16,'users','Usuario','users','fa-table',1,0,'2019-10-06 01:34:08','2019-10-06 01:34:08'),
	(9,'venta','Lista Ventas','venta','fa-table',1,0,'2019-09-28 14:22:30','2019-09-28 14:22:30');

/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(3,'2014_10_12_000000_create_users_table',1),
	(4,'2014_10_12_100000_create_password_resets_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table perfil
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perfil`;

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(100) NOT NULL,
  `vertical` varchar(100) NOT NULL,
  `fondo` varchar(100) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`header`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;

INSERT INTO `perfil` (`id`, `header`, `vertical`, `fondo`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'2','2','2',1,0,'2019-09-27 23:42:41','2019-09-27 23:42:41');

/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permiso
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permiso`;

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `isdelete` tinyint(1) NOT NULL DEFAULT '1',
  `isupdate` tinyint(1) NOT NULL DEFAULT '1',
  `isnew` tinyint(1) NOT NULL DEFAULT '1',
  `isreporte` tinyint(1) NOT NULL DEFAULT '0',
  `isviewall` tinyint(1) NOT NULL DEFAULT '0',
  `isview` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_grupo`,`id_menu`,`id_rol`),
  KEY `id` (`id`),
  KEY `fk_permiso_rol` (`id_rol`),
  KEY `fk_permiso_menu` (`id_menu`),
  CONSTRAINT `fk_permiso_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`),
  CONSTRAINT `fk_permiso_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`),
  CONSTRAINT `fk_permiso_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `permiso` WRITE;
/*!40000 ALTER TABLE `permiso` DISABLE KEYS */;

INSERT INTO `permiso` (`id`, `id_grupo`, `id_menu`, `id_rol`, `isdelete`, `isupdate`, `isnew`, `isreporte`, `isviewall`, `isview`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(32,1,1,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:48','2020-08-21 02:29:01'),
	(13,1,5,1,1,1,1,0,0,0,1,0,'2019-09-28 15:38:00','2019-09-28 15:38:00'),
	(23,1,13,2,1,1,1,0,0,0,1,0,'2019-09-28 20:14:17','2019-09-28 20:14:17'),
	(5,2,3,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:47','2019-09-28 14:15:47'),
	(28,2,4,2,1,1,1,0,0,0,1,0,'2019-11-10 19:56:17','2019-11-10 19:56:17'),
	(27,2,9,2,1,1,1,0,0,0,1,0,'2019-11-10 19:56:12','2019-11-10 19:56:12'),
	(7,2,10,1,1,1,1,0,0,0,1,0,'2020-08-17 03:21:42','2019-09-28 14:24:07'),
	(12,2,11,1,1,1,1,0,0,0,1,0,'2020-08-17 03:21:42','2019-09-28 15:28:45'),
	(19,2,13,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:47','2019-09-28 16:40:37'),
	(25,2,13,3,1,1,1,0,0,0,1,0,'2019-09-28 20:16:41','2019-09-28 20:16:41'),
	(20,2,15,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:48','2019-09-28 16:40:40'),
	(26,2,16,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:48','2019-10-06 01:34:23'),
	(30,4,8,2,1,1,1,0,0,0,1,0,'2020-08-13 00:19:46','2020-08-13 00:19:46'),
	(31,4,17,1,1,1,1,0,0,0,1,0,'2020-08-13 22:02:00','2020-08-13 22:02:00'),
	(35,4,17,4,1,1,1,0,0,0,1,0,'2020-08-28 22:34:57','2020-08-28 22:28:47'),
	(41,4,17,5,1,1,1,0,0,0,1,0,'2020-08-28 22:51:35','2020-08-28 22:51:35'),
	(14,5,4,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:47','2019-09-28 15:39:28'),
	(17,5,6,1,1,1,1,0,0,0,1,0,'2019-09-28 15:39:39','2019-09-28 15:39:39'),
	(38,5,6,4,1,1,1,0,0,0,1,0,'2020-08-28 22:35:07','2020-08-28 22:35:07'),
	(16,5,8,1,1,1,1,0,0,0,1,0,'2020-08-17 03:21:42','2019-09-28 15:39:36'),
	(39,5,8,5,1,1,1,0,0,0,1,0,'2020-08-28 22:50:44','2020-08-28 22:50:44'),
	(18,5,9,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:47','2019-09-28 15:39:41'),
	(22,5,14,1,1,1,1,0,0,0,1,0,'2020-08-28 23:50:47','2019-09-28 16:40:48'),
	(33,6,13,4,1,1,1,0,0,0,1,0,'2020-08-28 22:34:57','2020-08-26 02:00:52'),
	(37,7,14,4,1,1,1,0,0,0,1,0,'2020-08-28 22:34:09','2020-08-28 22:34:09'),
	(40,8,4,5,1,1,1,0,0,0,1,0,'2020-08-28 22:51:02','2020-08-28 22:51:02'),
	(42,8,6,5,1,1,1,0,0,0,1,0,'2020-08-28 22:52:05','2020-08-28 22:52:05');

/*!40000 ALTER TABLE `permiso` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table persona
# ------------------------------------------------------------

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `paterno` varchar(30) DEFAULT '',
  `materno` varchar(30) DEFAULT '',
  `ci` varchar(15) NOT NULL,
  `id_expedido` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ci`),
  KEY `id` (`id`),
  KEY `fk_persona_expedido` (`id_expedido`),
  CONSTRAINT `fk_persona_expedido` FOREIGN KEY (`id_expedido`) REFERENCES `expedido` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;

INSERT INTO `persona` (`id`, `nombre`, `paterno`, `materno`, `ci`, `id_expedido`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(6,'genara','luna','chura','12344646',2,1,0,'2020-08-26 00:34:45','2020-08-26 00:34:45'),
	(2,'vallll','dsjaj','jdfh','56656565',1,1,1,'2019-09-28 15:27:24','2019-09-28 15:26:41'),
	(3,'cliente','5555','555','66564464',1,1,1,'2020-08-27 05:15:22','2019-09-28 15:27:14'),
	(1,'Esther','Calle','Limachi','66644678',1,1,0,'2020-08-21 03:06:26','2019-09-27 23:43:15'),
	(8,'Luis','Figueroa Roque','Roque','7098545',1,1,0,'2020-08-28 20:34:52','2020-08-28 20:34:52'),
	(9,'Angy','Arizaga','Benz','7412589',2,1,0,'2020-08-28 20:35:25','2020-08-28 20:35:25'),
	(4,'aaa','sss','dddd','7894512',7,1,1,'2020-08-27 05:15:28','2020-08-17 02:58:54'),
	(11,'Gabriel','Luna','Condori','7894523',1,1,0,'2020-08-29 02:20:46','2020-08-29 02:20:46'),
	(7,'pepe','hialri','poma','78945612',1,1,0,'2020-08-26 02:01:56','2020-08-26 02:01:56'),
	(5,'lola','figueroa','calsina','7894652',2,1,0,'2020-08-19 03:24:57','2020-08-19 03:24:57'),
	(10,'Guillermo','Lopez','Perez','79845612',3,1,0,'2020-08-28 22:26:05','2020-08-28 22:26:05');

/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertpersona` BEFORE INSERT ON `persona` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;paterno=',ifnull(new.paterno,''),
    ' ;materno=',ifnull(new.materno,''),
    ' ;ci=',ifnull(new.ci,''),
    ' ;id_expedido=',ifnull(new.id_expedido,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"persona");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updatepersona` BEFORE UPDATE ON `persona` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;paterno=',ifnull(old.paterno,''),
    ' ;materno=',ifnull(old.materno,''),
    ' ;ci=',ifnull(old.ci,''),
    ' ;id_expedido=',ifnull(old.id_expedido,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;paterno=',ifnull(new.paterno,''),
    ' ;materno=',ifnull(new.materno,''),
    ' ;ci=',ifnull(new.ci,''),
    ' ;id_expedido=',ifnull(new.id_expedido,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.paterno <> new.paterno THEN set @res = CONCAT (@res, ' ;Cambió el valor de paterno: ',ifnull(OLD.paterno,''), ' a: ',ifnull(new.paterno,'')); END IF;
	IF OLD.materno <> new.materno THEN set @res = CONCAT (@res, ' ;Cambió el valor de materno: ',ifnull(OLD.materno,''), ' a: ',ifnull(new.materno,'')); END IF;
	IF OLD.ci <> new.ci THEN set @res = CONCAT (@res, ' ;Cambió el valor de ci: ',ifnull(OLD.ci,''), ' a: ',ifnull(new.ci,'')); END IF;
	IF OLD.id_expedido <> new.id_expedido THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_expedido: ',ifnull(OLD.id_expedido,''), ' a: ',ifnull(new.id_expedido,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"persona");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"persona",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deletepersona` AFTER DELETE ON `persona` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;paterno=',ifnull(old.paterno,''),
    ' ;materno=',ifnull(old.materno,''),
    ' ;ci=',ifnull(old.ci,''),
    ' ;id_expedido=',ifnull(old.id_expedido,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"persona");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(15) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `chasis` varchar(50) NOT NULL,
  `motor` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `id_fabrica` int(11) NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `logo` varchar(80) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`),
  KEY `fk_producto_fabrica` (`id_fabrica`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;

INSERT INTO `producto` (`id`, `codigo`, `nombre`, `modelo`, `chasis`, `motor`, `descripcion`, `id_fabrica`, `costo`, `logo`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(6,'78914522GHL','Jinkey','2019','GHK-745','78451266GH78','motor de cc2335',3,78543.00,'1598667962a1.png',1,0,'2020-08-29 02:26:04','2020-08-29 02:26:04'),
	(2,'1213AA12','Kinglong','2016','9BWCC5X918003018','JDH-09692','motor cilindrada 2300cc',3,17500.00,'15978100524-1-4-kingwin-cng-van_0.jpg',1,0,'2020-08-19 04:46:26','2020-08-19 04:07:34'),
	(3,'178912CVB123','Jincheng','2019','99FGB561674DC','KLM-0521','motor cilindrada 2900cc corona montaña',3,58600.00,'1597870570a4.png',1,0,'2020-08-19 20:56:12','2020-08-19 20:56:12'),
	(5,'482473362ML','Tking','2019','789512488KL','KLH-784512','motor cc2300',1,78760.00,'1598660001a12.png',1,0,'2020-08-29 00:13:23','2020-08-29 00:13:23');

/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertproducto` BEFORE INSERT ON `producto` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;modelo=',ifnull(new.modelo,''),
    ' ;chasis=',ifnull(new.chasis,''),
    ' ;motor=',ifnull(new.motor,''),
    ' ;descripcion=',ifnull(new.descripcion,''),
    ' ;id_fabrica=',ifnull(new.id_fabrica,''),
    ' ;costo=',ifnull(new.costo,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"producto");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateproducto` BEFORE UPDATE ON `producto` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;modelo=',ifnull(old.modelo,''),
    ' ;chasis=',ifnull(old.chasis,''),
    ' ;motor=',ifnull(old.motor,''),
    ' ;descripcion=',ifnull(old.descripcion,''),
    ' ;id_fabrica=',ifnull(old.id_fabrica,''),
    ' ;costo=',ifnull(old.costo,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;modelo=',ifnull(new.modelo,''),
    ' ;chasis=',ifnull(new.chasis,''),
    ' ;motor=',ifnull(new.motor,''),
    ' ;descripcion=',ifnull(new.descripcion,''),
    ' ;id_fabrica=',ifnull(new.id_fabrica,''),
    ' ;costo=',ifnull(new.costo,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' ;Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.modelo <> new.modelo THEN set @res = CONCAT (@res, ' ;Cambió el valor de modelo: ',ifnull(OLD.modelo,''), ' a: ',ifnull(new.modelo,'')); END IF;
	IF OLD.chasis <> new.chasis THEN set @res = CONCAT (@res, ' ;Cambió el valor de chasis: ',ifnull(OLD.chasis,''), ' a: ',ifnull(new.chasis,'')); END IF;
	IF OLD.motor <> new.motor THEN set @res = CONCAT (@res, ' ;Cambió el valor de motor: ',ifnull(OLD.motor,''), ' a: ',ifnull(new.motor,'')); END IF;
	IF OLD.descripcion <> new.descripcion THEN set @res = CONCAT (@res, ' ;Cambió el valor de descripcion: ',ifnull(OLD.descripcion,''), ' a: ',ifnull(new.descripcion,'')); END IF;
	IF OLD.id_fabrica <> new.id_fabrica THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_fabrica: ',ifnull(OLD.id_fabrica,''), ' a: ',ifnull(new.id_fabrica,'')); END IF;
	IF OLD.costo <> new.costo THEN set @res = CONCAT (@res, ' ;Cambió el valor de costo: ',ifnull(OLD.costo,''), ' a: ',ifnull(new.costo,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"producto");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"producto",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteproducto` AFTER DELETE ON `producto` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;modelo=',ifnull(old.modelo,''),
    ' ;chasis=',ifnull(old.chasis,''),
    ' ;motor=',ifnull(old.motor,''),
    ' ;descripcion=',ifnull(old.descripcion,''),
    ' ;id_fabrica=',ifnull(old.id_fabrica,''),
    ' ;costo=',ifnull(old.costo,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"producto");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table proveedor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `sitio_web` varchar(200) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `rl_nombre` varchar(50) NOT NULL,
  `rl_apellidos` varchar(40) NOT NULL,
  `rl_corrreo` varchar(40) NOT NULL,
  `rl_telefono` varchar(20) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;

INSERT INTO `proveedor` (`id`, `nombre`, `sitio_web`, `telefono`, `rl_nombre`, `rl_apellidos`, `rl_corrreo`, `rl_telefono`, `direccion`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(3,'aaaaaaaaaa','www.motor.com.co','1321355','sssss','sss','sss','5555','ssssss',1,0,'2020-08-17 04:16:12','2020-08-17 02:32:31'),
	(2,'ewq','ewq','ewq','ewqqe','ewq','eqwe','qweq','e',1,0,'2019-09-28 20:41:42','2019-09-28 20:41:42'),
	(1,'rew','rewrew','rewrew','rew','rewr','ewrew','rew','rw',NULL,0,'2019-09-27 01:27:37','2019-09-27 01:27:37');

/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertproveedor` BEFORE INSERT ON `proveedor` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;sitio_web=',ifnull(new.sitio_web,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;rl_nombre=',ifnull(new.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(new.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(new.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(new.rl_telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"proveedor");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateproveedor` BEFORE UPDATE ON `proveedor` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;sitio_web=',ifnull(old.sitio_web,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;rl_nombre=',ifnull(old.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(old.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(old.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(old.rl_telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;sitio_web=',ifnull(new.sitio_web,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;rl_nombre=',ifnull(new.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(new.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(new.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(new.rl_telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.sitio_web <> new.sitio_web THEN set @res = CONCAT (@res, ' ;Cambió el valor de sitio_web: ',ifnull(OLD.sitio_web,''), ' a: ',ifnull(new.sitio_web,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.rl_nombre <> new.rl_nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_nombre: ',ifnull(OLD.rl_nombre,''), ' a: ',ifnull(new.rl_nombre,'')); END IF;
	IF OLD.rl_apellidos <> new.rl_apellidos THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_apellidos: ',ifnull(OLD.rl_apellidos,''), ' a: ',ifnull(new.rl_apellidos,'')); END IF;
	IF OLD.rl_corrreo <> new.rl_corrreo THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_corrreo: ',ifnull(OLD.rl_corrreo,''), ' a: ',ifnull(new.rl_corrreo,'')); END IF;
	IF OLD.rl_telefono <> new.rl_telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_telefono: ',ifnull(OLD.rl_telefono,''), ' a: ',ifnull(new.rl_telefono,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"proveedor");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"proveedor",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteproveedor` AFTER DELETE ON `proveedor` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;sitio_web=',ifnull(old.sitio_web,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;rl_nombre=',ifnull(old.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(old.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(old.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(old.rl_telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"proveedor");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `descripcion` varchar(190) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nombre`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;

INSERT INTO `rol` (`id`, `nombre`, `descripcion`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(1,'Administrador','Enagado global sistema',1,0,'2020-08-28 21:48:22','2019-09-27 23:42:34'),
	(4,'Almacenero','encargado del almacen',1,0,'2020-08-28 21:47:59','2020-08-26 02:00:11'),
	(2,'Cliente','cliente de la empresa',1,0,'2020-08-28 21:48:46','2019-09-27 23:42:34'),
	(3,'Reportes','solo reporte',1,0,'2019-09-28 15:37:00','2019-09-28 15:37:00'),
	(8,'Vededor2','encargado de la venta',1,0,'2020-08-29 02:23:36','2020-08-29 02:23:36'),
	(5,'Vendedor','Es el encargado de las ventas',1,0,'2020-08-28 21:47:40','2020-08-28 21:46:44');

/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) NOT NULL,
  `email` varchar(78) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_rol` int(11) DEFAULT '2',
  `id_persona` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL DEFAULT 'wicoder.png',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usuario`),
  KEY `id` (`id`),
  KEY `fk_usuario_rol` (`id_rol`),
  KEY `fk_usuario_persona` (`id_persona`),
  CONSTRAINT `fk_usuario_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`),
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `usuario`, `email`, `password`, `id_rol`, `id_persona`, `logo`, `estado`, `eliminado`, `updated_at`, `created_at`)
VALUES
	(11,'ana','ana@yahoo.es','$2y$10$9k9cCO6I3OU0/kxNMk4S1.TrjOnViKGdgH1mIPATgqnbyfUFS8luu',1,4,'wicoder.png',1,0,'2020-08-26 00:56:11','2020-08-26 00:56:11'),
	(1,'Esther','admin@gmail.com','$2y$10$b21pGDO99uWFwxs83j2TT.aPBRxNfW2guYC/u4fqldngmiT4Ghmo.',1,1,'wicoder.png',1,0,'2020-08-21 03:06:55','2019-09-27 23:42:05'),
	(17,'Gabiel','gabriel@gmail.com','$2y$10$pgkQbM4TdLwIh2hdoU3JoOpIHqn9.OtUWvrjloZMpYWMTLQk7icQ.',4,11,'wicoder.png',1,0,'2020-08-29 02:22:04','2020-08-29 02:21:38'),
	(9,'genara','genara@gmail.com','$2y$10$.swdb/0VqDnuva8ypXdOvu7PqDW907Z45YHmr66/T786..O9EQgKm',2,6,'wicoder.png',1,0,'2020-08-26 00:41:18','2020-08-26 00:41:18'),
	(13,'Guillermo','guillermo@gmail.com','$2y$10$oy9D8KtVKLIZAcL5/OIUXOsbMvx1eFGRTJau4VKhD0jRYheZKkPF2',4,10,'wicoder.png',1,0,'2020-08-28 22:27:28','2020-08-28 22:27:28'),
	(4,'jaimito','jaimito@gmail.com','$2y$10$FYJOLT7QzqIJDFntcJcFvOfgXQF8x3klCTLoyzFaFy3OhG76Dg8fG',2,2,'wicoder.png',1,0,'2020-08-17 21:03:16','2019-10-06 12:50:40'),
	(16,'Laliest','lali@gmail.com','$2y$10$LoiBHe3e8OxZb7rr/TvEBODEKIJPhTa1v3UJ53EAY8Ngf4HnCnQN2',1,1,'wicoder.png',1,0,'2020-08-28 22:49:20','2020-08-28 22:37:54'),
	(10,'lolis','lol@hotmail.com','$2y$10$0MgfnSnBUJtOrZk8EhHA/O4BBOwV17JWs0w2TcFMpF9wGhLASBebW',3,5,'wicoder.png',1,0,'2020-08-26 00:54:16','2020-08-26 00:54:16'),
	(12,'pepe','pepe@gmail.com','$2y$10$aTLtNFKpi1ImvDAbghumgunIQbzs0qt82kVWz6AC0dn8Vhnsi4j3a',4,7,'wicoder.png',1,0,'2020-08-26 02:02:39','2020-08-26 02:02:39'),
	(3,'pruebas','pruebas@gmail.com','$2y$10$Tj9KEk0O3UzMJ2hXEdtA5OEAVOuDvnJ/FsRZXAkbQ7KUla0/f9Ly2',1,3,'wicoder.png',1,0,'2019-10-06 02:46:48','2019-10-06 02:13:22'),
	(8,'pruebass','pruebas@gmail.com','$2y$10$fI06Mu08O3AuNXpZI3B2dOsNziWZiZD1Q.EE67fTbirdzHSg3gWnK',1,2,'wicoder.png',1,0,'2019-10-26 12:51:52','2019-10-26 12:51:52'),
	(2,'will','will@gmail.com','$2y$10$8FX.Pmtt5ZJYWg2iBD0kJubnFgIXKfaMXTIzR5Mgw5TMdNYE8TMeO',3,3,'wicoder.png',1,0,'2019-09-29 00:56:53','2019-09-29 00:56:53');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertusers` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;usuario=',ifnull(new.usuario,''),
    ' ;email=',ifnull(new.email,''),
    ' ;password=',ifnull(new.password,''),
    ' ;id_rol=',ifnull(new.id_rol,''),
    ' ;id_persona=',ifnull(new.id_persona,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"users");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateusers` BEFORE UPDATE ON `users` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;usuario=',ifnull(old.usuario,''),
    ' ;email=',ifnull(old.email,''),
    ' ;password=',ifnull(old.password,''),
    ' ;id_rol=',ifnull(old.id_rol,''),
    ' ;id_persona=',ifnull(old.id_persona,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;usuario=',ifnull(new.usuario,''),
    ' ;email=',ifnull(new.email,''),
    ' ;password=',ifnull(new.password,''),
    ' ;id_rol=',ifnull(new.id_rol,''),
    ' ;id_persona=',ifnull(new.id_persona,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.usuario <> new.usuario THEN set @res = CONCAT (@res, ' ;Cambió el valor de usuario: ',ifnull(OLD.usuario,''), ' a: ',ifnull(new.usuario,'')); END IF;
	IF OLD.email <> new.email THEN set @res = CONCAT (@res, ' ;Cambió el valor de email: ',ifnull(OLD.email,''), ' a: ',ifnull(new.email,'')); END IF;
	IF OLD.password <> new.password THEN set @res = CONCAT (@res, ' ;Cambió el valor de password: ',ifnull(OLD.password,''), ' a: ',ifnull(new.password,'')); END IF;
	IF OLD.id_rol <> new.id_rol THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_rol: ',ifnull(OLD.id_rol,''), ' a: ',ifnull(new.id_rol,'')); END IF;
	IF OLD.id_persona <> new.id_persona THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_persona: ',ifnull(OLD.id_persona,''), ' a: ',ifnull(new.id_persona,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

	IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"users");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"users",ifnull(@res,'No cambio nada'));
    END IF;

END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteusers` AFTER DELETE ON `users` FOR EACH ROW BEGIN
  SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;usuario=',ifnull(old.usuario,''),
    ' ;email=',ifnull(old.email,''),
    ' ;password=',ifnull(old.password,''),
    ' ;id_rol=',ifnull(old.id_rol,''),
    ' ;id_persona=',ifnull(old.id_persona,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"users");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table venta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `venta`;

CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `total` varchar(10) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avance` varchar(20) NOT NULL DEFAULT 'pendiente',
  `tipo_pago` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `id` (`id`),
  KEY `fk_cotizacion_cliente` (`id_cliente`),
  KEY `fk_cotizacion_users` (`id_users`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;

INSERT INTO `venta` (`id`, `codigo`, `fecha`, `id_cliente`, `id_users`, `total`, `estado`, `eliminado`, `updated_at`, `created_at`, `avance`, `tipo_pago`)
VALUES
	(10,'456','2020-08-28',1,1,'17,350.00',NULL,0,'2020-08-29 03:54:17','2020-08-29 03:54:17','pendiente','efectivo'),
	(2,'123','2020-08-17',1,1,'14,800.00',NULL,0,'2020-08-19 22:57:38','2020-08-18 07:17:42','pendiente','cheque'),
	(3,'148874','2020-08-17',1,1,'29,480.00',NULL,0,'2020-08-20 01:35:22','2020-08-18 07:21:30','cerrado','efectivo'),
	(4,'457845','2020-08-17',1,1,'14,800.00',NULL,0,'2020-08-20 01:35:09','2020-08-18 07:25:02','pendiente','cheque'),
	(5,'12','2020-08-19',1,1,'34,950.00',NULL,0,'2020-08-20 02:43:54','2020-08-19 08:15:41','pendiente','efectivo'),
	(11,'145','2020-08-28',2,1,'17,400.00',NULL,0,'2020-08-29 00:02:14','2020-08-29 04:01:31','cerrado','efectivo'),
	(7,'52152','2020-08-19',1,1,'14,667.00',NULL,0,'2020-08-30 03:47:17','2020-08-20 00:44:14','cerrado','cheque'),
	(8,'45','2020-08-25',3,1,'14,677.00',NULL,0,'2020-08-26 06:11:37','2020-08-26 06:11:37','pendiente','cheque'),
	(9,'147','2020-08-26',8,1,'17,500.00',NULL,0,'2020-08-26 23:58:53','2020-08-27 03:58:27','cerrado','efectivo'),
	(12,'78','2020-08-28',10,1,'17,350.00',NULL,0,'2020-08-29 02:30:04','2020-08-29 06:29:32','cerrado','efectivo'),
	(13,'159','2020-08-29',1,1,'78,360.00',NULL,0,'2020-08-30 03:50:20','2020-08-30 07:44:40','cerrado','efectivo'),
	(14,'4567','2020-08-29',7,1,'78,560.00',NULL,0,'2020-08-30 03:46:48','2020-08-30 07:46:03','cerrado','cheque'),
	(15,'753','2020-08-29',6,1,'157,120.00',NULL,0,'2020-08-30 03:54:21','2020-08-30 07:51:56','cerrado','efectivo');

/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertventa` BEFORE INSERT ON `venta` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;fecha=',ifnull(new.fecha,''),
    ' ;id_cliente=',ifnull(new.id_cliente,''),
    ' ;id_users=',ifnull(new.id_users,''),
    ' ;total=',ifnull(new.total,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;avance=',ifnull(new.avance,''),
    ' ;tipo_pago=',ifnull(new.tipo_pago,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"venta");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateventa` BEFORE UPDATE ON `venta` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;fecha=',ifnull(old.fecha,''),
    ' ;id_cliente=',ifnull(old.id_cliente,''),
    ' ;id_users=',ifnull(old.id_users,''),
    ' ;total=',ifnull(old.total,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;avance=',ifnull(old.avance,''),
    ' ;tipo_pago=',ifnull(old.tipo_pago,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;fecha=',ifnull(new.fecha,''),
    ' ;id_cliente=',ifnull(new.id_cliente,''),
    ' ;id_users=',ifnull(new.id_users,''),
    ' ;total=',ifnull(new.total,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;avance=',ifnull(new.avance,''),
    ' ;tipo_pago=',ifnull(new.tipo_pago,'')
  );

  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' ;Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.fecha <> new.fecha THEN set @res = CONCAT (@res, ' ;Cambió el valor de fecha: ',ifnull(OLD.fecha,''), ' a: ',ifnull(new.fecha,'')); END IF;
	IF OLD.id_cliente <> new.id_cliente THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_cliente: ',ifnull(OLD.id_cliente,''), ' a: ',ifnull(new.id_cliente,'')); END IF;
	IF OLD.id_users <> new.id_users THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_users: ',ifnull(OLD.id_users,''), ' a: ',ifnull(new.id_users,'')); END IF;
	IF OLD.total <> new.total THEN set @res = CONCAT (@res, ' ;Cambió el valor de total: ',ifnull(OLD.total,''), ' a: ',ifnull(new.total,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;
	IF OLD.avance <> new.avance THEN set @res = CONCAT (@res, ' ;Cambió el valor de avance: ',ifnull(OLD.avance,''), ' a: ',ifnull(new.avance,'')); END IF;
	IF OLD.tipo_pago <> new.tipo_pago THEN set @res = CONCAT (@res, ' ;Cambió el valor de tipo_pago: ',ifnull(OLD.tipo_pago,''), ' a: ',ifnull(new.tipo_pago,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"venta");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"venta",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteventa` AFTER DELETE ON `venta` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;fecha=',ifnull(old.fecha,''),
    ' ;id_cliente=',ifnull(old.id_cliente,''),
    ' ;id_users=',ifnull(old.id_users,''),
    ' ;total=',ifnull(old.total,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;avance=',ifnull(old.avance,''),
    ' ;tipo_pago=',ifnull(old.tipo_pago,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"venta");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table venta_detalle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `venta_detalle`;

CREATE TABLE `venta_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_venta` int(11) DEFAULT NULL,
  `id_producto` varchar(50) NOT NULL,
  `cantidad` decimal(10,0) NOT NULL,
  `precio_uni` decimal(10,2) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `descuento` float DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `fk_vendetall_venta` (`id_venta`),
  KEY `fk_vendetall_producto` (`id_producto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `venta_detalle` WRITE;
/*!40000 ALTER TABLE `venta_detalle` DISABLE KEYS */;

INSERT INTO `venta_detalle` (`id`, `id_venta`, `id_producto`, `cantidad`, `precio_uni`, `eliminado`, `updated_at`, `created_at`, `descuento`)
VALUES
	(1,2,'1',1,14800.00,0,'2020-08-18 07:27:28','2020-08-18 03:17:42',0),
	(2,3,'1',2,14800.00,0,'2020-08-18 03:21:30','2020-08-18 03:21:30',0),
	(3,4,'1',1,14800.00,0,'2020-08-20 02:38:27','2020-08-18 03:25:02',0),
	(4,5,'2',2,17500.00,0,'2020-08-20 02:43:54','2020-08-19 04:15:41',50),
	(6,7,'1',1,14800.00,0,'2020-08-19 20:44:14','2020-08-19 20:44:14',0),
	(7,6,'1',1,14800.00,0,'2020-08-20 02:50:58','2020-08-19 21:34:02',50),
	(8,8,'1',1,14800.00,0,'2020-08-26 02:11:37','2020-08-26 02:11:37',0),
	(9,9,'2',1,17500.00,0,'2020-08-26 23:58:27','2020-08-26 23:58:27',0),
	(10,10,'2',1,17500.00,0,'2020-08-28 23:54:17','2020-08-28 23:54:17',0),
	(11,11,'2',1,17500.00,0,'2020-08-29 00:01:31','2020-08-29 00:01:31',0),
	(12,12,'2',1,17500.00,0,'2020-08-29 02:29:33','2020-08-29 02:29:33',0),
	(13,13,'6',1,78560.00,0,'2020-08-30 03:44:40','2020-08-30 03:44:40',0),
	(14,14,'6',1,78560.00,0,'2020-08-30 03:46:03','2020-08-30 03:46:03',0),
	(15,15,'6',1,78560.00,0,'2020-08-30 07:53:51','2020-08-30 03:51:56',0);

/*!40000 ALTER TABLE `venta_detalle` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `insertventa_detalle` BEFORE INSERT ON `venta_detalle` FOR EACH ROW BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_venta=',ifnull(new.id_venta,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;cantidad=',ifnull(new.cantidad,''),
    ' ;precio_uni=',ifnull(new.precio_uni,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;descuento=',ifnull(new.descuento,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"venta_detalle");
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `updateventa_detalle` BEFORE UPDATE ON `venta_detalle` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_venta=',ifnull(old.id_venta,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;cantidad=',ifnull(old.cantidad,''),
    ' ;precio_uni=',ifnull(old.precio_uni,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;descuento=',ifnull(old.descuento,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_venta=',ifnull(new.id_venta,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;cantidad=',ifnull(new.cantidad,''),
    ' ;precio_uni=',ifnull(new.precio_uni,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;descuento=',ifnull(new.descuento,'')
  );

  set @res = '';
	IF OLD.id_venta <> new.id_venta THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_venta: ',ifnull(OLD.id_venta,''), ' a: ',ifnull(new.id_venta,'')); END IF;
	IF OLD.id_producto <> new.id_producto THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_producto: ',ifnull(OLD.id_producto,''), ' a: ',ifnull(new.id_producto,'')); END IF;
	IF OLD.cantidad <> new.cantidad THEN set @res = CONCAT (@res, ' ;Cambió el valor de cantidad: ',ifnull(OLD.cantidad,''), ' a: ',ifnull(new.cantidad,'')); END IF;
	IF OLD.precio_uni <> new.precio_uni THEN set @res = CONCAT (@res, ' ;Cambió el valor de precio_uni: ',ifnull(OLD.precio_uni,''), ' a: ',ifnull(new.precio_uni,'')); END IF;
	IF OLD.descuento <> new.descuento THEN set @res = CONCAT (@res, ' ;Cambió el valor de descuento: ',ifnull(OLD.descuento,''), ' a: ',ifnull(new.descuento,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"venta_detalle");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"venta_detalle",ifnull(@res,'No cambio nada'));
    END IF;
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `deleteventa_detalle` AFTER DELETE ON `venta_detalle` FOR EACH ROW BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_venta=',ifnull(old.id_venta,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;cantidad=',ifnull(old.cantidad,''),
    ' ;precio_uni=',ifnull(old.precio_uni,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;descuento=',ifnull(old.descuento,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"venta_detalle");
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
