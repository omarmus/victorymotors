var datos = new Array;
var datoinivtario = '';
(function (wn) {
    'use strict';
    wn.agregarProducto = function (event) {
        //alert($("#CargarProducto .StockVal").text(item.data('stock')));
        var html = '';
        if ($("#CargarProducto [name='cantidad']").val() != '') {

            if (datos.map((datoinivtario) => datoinivtario.id_producto).includes(datoinivtario.id_producto)) {
                alert('Ya existe el mismo producto en la cotizacion');
            } else {
                var ccc = $("#CargarProducto [name='cantidad']").val()
                var descripcion = $("#CargarProducto [name='productoBuscar']").val()
                let sbt = ((parseInt(ccc) * parseFloat(datoinivtario.precio)));
                //alert(sbt);
                W.push(datos, {
                    id_producto: datoinivtario.id_producto,
                    id: datoinivtario.id,
                    producto: datoinivtario.producto,
                    codigo: datoinivtario.codigo,
                    cantidad: ccc,
                    precio: datoinivtario.precio,
                    subtotal: sbt});
                //consoleLog(datos);
                $.each(datos, function (e, tr) {
                    html = '<tr val="' + tr.id_producto + '" key="' + tr.id + '">\n\
                            <td id="index"><button class="btn btn-danger btn-xs" type="button"><i class="fa fa-times" aria-hidden="true"></i> </td>\n\
                            <td>' + W.input({type: 'hidden', name: 'producto[]', value: tr.id_producto})
                            + tr.codigo + '</td>\n\
                            <td>' + W.input({type: 'number', name: 'cantidad[]', value: tr.cantidad}) + '</td>\n\
                            <td>' + tr.producto + '</td>\n\
                            <td>' + W.input({type: 'number', name: 'descuento[]', value: tr.descuento}) + '</td>\n\
                            <td>' + W.input({type: 'text', name: 'precio_uni[]', value: tr.precio})
                            + W.input({type: 'hidden', name: 'subtotal[]', value: tr.subtotal}) + '</td>\n\
                            <td>' + sbt + '</td>\n\
                </tr>';
                });

                $('#cargarDetalle').append(html);
                wn.sumaTotal();
                wn.delete();
                $('#modal-producto').attr('aria-modal', "false").addClass('hide').css('z-index', '9991').hide();
                $('#CargarProducto').trigger("reset");
            }
        }
        $('#CargarProducto').trigger("reset");
        return false;
    };
    wn.getProducto = function (e) {
        //$('#productoBuscar').value = '';
        document.getElementById("productoBuscar").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("precio").value = "";
        $(".StockVal").text ('00');
        $($(e).data('id')).attr('aria-modal', "true").addClass('show').css('z-index', '9992').show();
    };
    wn.close = function (e) {
        $($(e).data('id')).attr('aria-modal', "true").addClass('show').css('z-index', '0').hide();
    };


    wn.sumaTotal = function (cuotas) {
        var sumac = 0, sumab = 0, sumDes = 0, tt = W.$i('getProductoCard'), trbd, x, s = 3;

        trbd = [].slice.call(W.$qsa(tt, 'tbody tr'));

        trbd.forEach(function (td, j) {
            for (x = 4; x <= 4; x++) {
                if (td.cells[x].querySelector('input[name="descuento[]"]')) {
                    td.cells[x].querySelector('input').addEventListener('keyup', function (ev, h) {
                        datos.forEach((element, index) => {
                            if (element.id_producto == parseInt(td.getAttribute('val'))) {
                                element.descuento = $(this).val();
                                if ($(this).val() != "") {
                                    element.subtotal = (parseInt(element.cantidad) * (parseFloat(element.precio))) - parseInt($(this).val());
                                    td.cells[6].innerHTML = ($(this).val() == '' ? 0 : element.subtotal);
                                    W.push(datos, element);
                                    sumar();
                                }else{
                                    element.subtotal = (parseInt(element.cantidad) * (parseFloat(element.precio)));
                                    td.cells[6].innerHTML =  element.subtotal;
                                    W.push(datos, element);
                                    sumar();
                                }
                            }
                        });
                    });
                }
            }
        });

        trbd.forEach(function (td, j) {
            for (x = 5; x <= 5; x++) {
                if (td.cells[x].querySelector('input[name="precio_uni[]"]')) {
                    td.cells[x].querySelector('input').addEventListener('keyup', function (ev, h) {
                        datos.forEach((element, index) => {
                            if (element.id_producto == parseInt(td.getAttribute('val'))) {
                                element.precio = $(this).val();
                                element.subtotal = parseInt(element.cantidad) * (parseFloat(element.precio));
                                td.cells[6].innerHTML = ($(this).val() == '' ? 0 : element.subtotal);
                                W.push(datos, element);
                                sumar();
                            }
                        });
                    });
                }
            }
        });

        trbd.forEach(function (td, j) {
            for (x = 2; x <= 2; x++) {
                if (td.cells[x].querySelector('input[name="cantidad[]"]')) {
                    td.cells[x].querySelector('input').addEventListener('keyup', function (ev, h) {
                        datos.forEach((element, index) => {
                            if (element.id_producto == td.getAttribute('val')) {
                                element.cantidad = $(this).val();
                                element.subtotal = parseInt(element.cantidad) * (parseFloat(element.precio));
                                td.cells[6].innerHTML = ($(this).val() == '' ? 0 : element.subtotal);
                                W.push(datos, element);
                                sumar();
                            }
                        });
                    });
                }
            }
        });

        var sumar = function () {
            var sb = 0, desc = 0;
            $.each(datos, function (e, tr) {
                sb = parseInt(sb) + parseInt(tr.subtotal);
                desc = parseInt(desc) + parseInt(tr.descuento);
            });

            var htmlFoot = '<tr>\n\
                          <th colspan="6" style="text-align: right;background: #fff;border-bottom:none">\n\
                            <div class="col-md-12" style="display: inline-grid;">\n\
                                \n\<input type="hidden" name="total" value="' + W.format(sb) + '">\n\
                                <span style="font-size: 23px;font-weight: bold;font-family: monospace;" id="total-productos" data-total="' + sb + '">Total:<strong> ' + W.format(sb) + ' Bs</strong></span>\n\
                            </div>\n\
                          </th>\n\
                    </tr>';
            W.$i('costoTotal').innerHTML = htmlFoot;
            renderCuotas(cuotas);
        }

        sumar();
    };

    wn.delete = function () {
        var tt = W.$i('getProductoCard'), trbd;
        trbd = [].slice.call(W.$qsa(tt, 'tbody tr'));
        trbd.forEach(function (td, i) {
            td.querySelector('button').addEventListener('click', function (ev) {

                td.remove();
                if (td.getAttribute('key') != '0') {
                    var ids = td.getAttribute('key');
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: url_delete,
                        data: {id: ids, "_token": csrf_token},
                        success: function (data) {
                            data.estado ? toastr.success(data.mensaje) : toastr.error(data.mensaje);
                            $('#ajax_form').trigger("reset");

                        }
                    });
                }
                wn.sumaTotal();

            });
        });
    };
    wn.new = function () {
        datos = new Array;
        $("[name='id']").val('');
        $('#ajax_form').trigger("reset");
        $('#cargarDetalle').empty();
        $('#modal-venta').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
        $('.buttonProducto').show();
        $('.buttonGuardar').show();
        $('#costoTotal').show();
        $('.titulo-modal-venta').text('Nueva Venta');
        $("#modal-venta #chansecliente option[value=0]").attr("selected", true).trigger("chosen:updated");
        $("#modal-venta #avance option[value=0]").attr("selected", true).trigger("chosen:updated");
        $("#modal-venta #tipo_pago option[value=0]").attr("selected", true).trigger("chosen:updated");
        wn.delete();

    };
    wn.editar = function (event) {

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: $(event).data('route'),
            data: {id: $(event).data('id'), "_token": csrf_token},
            success: function (data) {
                $('#cargarDetalle').empty();
                datos = new Array;
                var html = '';
                let v = data.data.venta;
                let dell = data.data.detalle;
                $.each(dell, function (e, tr) {
                    let sbt = parseInt(tr.cantidad) * parseFloat(tr.precio_uni);

                    W.push(datos, {
                        producto: tr.producto,
                        codigo: tr.codigo,
                        id: tr.id,
                        id_producto: tr.id_producto,
                        cantidad: tr.cantidad,
                        descuento: tr.descuento,
                        precio: tr.precio_uni,
                        subtotal: sbt});
                });

                $.each(datos, function (e, tr) {
                    html += '<tr val="' + tr.id_producto + '"  key="' + tr.id + '">\n\
                            <td id="index">' + W.input({type: 'hidden', name: 'dell_id[]', value: tr.id}) + '\
<button class="badge badge-danger" type="button"><i class="fa fa-times" aria-hidden="true"></i></button> </td>\n\
                            <td>' + W.input({type: 'hidden', name: 'producto[]', value: tr.id_producto})
                            + tr.codigo + '</td>\n\
                            <td>' + W.input({type: 'number', name: 'cantidad[]', value: tr.cantidad}) + '</td>\n\
                            <td>' + tr.producto + '</td>\n\
                            <td>' + W.input({type: 'number', name: 'descuento[]', value: tr.descuento}) + '</td>\n\
                            <td>' + W.input({type: 'text', name: 'precio_uni[]', value: tr.precio})
                            + W.input({type: 'hidden', name: 'subtotal[]', value: tr.subtotal}) + '</td>\n\
                            <td>' + tr.subtotal + '</td>\n\
                         </tr>';
                });
                $('.titulo-modal-venta').text('Editar Venta');
                if (v.avance == 'cerrado') {
                    $('.buttonProducto').hide();
                    $('.buttonGuardar').hide();
                    $('#costoTotal').show();
                } else {
                    $('.buttonProducto').show();
                    $('.buttonGuardar').show();
                    $('#costoTotal').show();
                }

                $('#modal-venta').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                $('#cargarDetalle').append(html);

                $("#modal-venta #ajax_form [name='codigo']").val(v.codigo);
                $("#modal-venta #ajax_form [name='id']").val(v.id);
                $("#modal-venta #ajax_form [name='fecha']").val(v.fecha);

                $("#modal-venta #chansecliente option[value=" + v.id_cliente + "]").attr("selected", true).trigger("chosen:updated");
                $("#modal-venta #avance option[value=" + v.avance + "]").attr("selected", true).trigger("chosen:updated");
                $("#modal-venta #tipo_pago option[value=" + v.tipo_pago + "]").attr("selected", true).trigger("chosen:updated");
                $("#modal-venta #nro_cuotas").val(v.nro_cuotas);
                // $("#modal-venta #cuotas").val(v.cuotas);

                $('#nro_cuotas_container').css('display', v.tipo_pago === 'credito' ? 'block' : 'none');
                var $table = $('#container_tables');
                console.log('table', $table);
                $table.next()
                      .css('display', v.tipo_pago === 'credito' ? 'block' : 'none')
                      .removeClass('col-sm-4')
                      .addClass(v.tipo_pago === 'credito' ? 'col-sm-4' : '');
                $table.removeClass('col-sm-8')
                      .removeClass('col-sm-12')
                      .addClass(v.tipo_pago === 'credito' ? 'col-sm-8' : 'col-sm-12');
                // console.log('CUOTAS', JSON.parse(v.cuotas));
                var cuotas = JSON.parse(v.cuotas);
                if (typeof cuotas === 'string') {
                    cuotas = JSON.parse(cuotas);
                }
                wn.sumaTotal(cuotas);
                wn.delete();
            }
        });

    };
    wn.informacion = function (event) {

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: $(event).data('route'),
            data: {id: $(event).data('id'), "_token": csrf_token},
            success: function (data) {
                $('#cargarDetalle').empty();
                datos = [];
                var html = '';
                let v = data.data.venta;
                let dell = data.data.detalle;
                $.each(dell, function (e, tr) {
                    let sbt = parseInt(tr.cantidad) * parseFloat(tr.precio_uni);

                    W.push(datos, {
                        producto: tr.producto,
                        codigo: tr.codigo,
                        id: tr.id,
                        id_producto: tr.id_producto,
                        cantidad: tr.cantidad,
                        precio: tr.precio_uni,
                        subtotal: sbt});
                });

                $.each(datos, function (e, tr) {
                    html += '<tr val="' + tr.id_producto + '"  key="' + tr.id + '">\n\
                    <td>' + tr.codigo + '</td>\n\
                    <td>' + tr.cantidad + '</td>\n\
                    <td>' + tr.producto + '</td>\n\
                    <td>' + tr.precio + '</td>\n\
                    <td>' + tr.subtotal + '</td>\n\
                    </tr>';
                });
                console.log(v);
                if (v.avance == 'cerrado') {
                    $('.buttonProducto').hide();
                    $('.buttonGuardar').hide();
                    $('#costoTotal').show();
                } else {
                    $('.buttonProducto').show();
                    $('.buttonGuardar').show();
                    $('#costoTotal').show();
                }

                $('#modal-info').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                $('#modal-info #cargarDetalle-info').html(html);

                $("#modal-info #ajax_form [name='codigo']").val(v.codigo);
                $("#modal-info #ajax_form [name='id']").val(v.id);
                $("#modal-info #ajax_form [name='fecha']").val(v.fecha);

                $("#modal-info #chansecliente option[value=" + v.id_cliente + "]").attr("selected", true).trigger("chosen:updated");
                $("#modal-info #avance option[value=" + v.avance + "]").attr("selected", true).trigger("chosen:updated");
                $("#modal-info #tipo_pago2 option[value=" + v.tipo_pago + "]").attr("selected", true).trigger("chosen:updated");
                $('#nro_cuotas2').val(v.nro_cuotas);


                $('#container_nro_cuotas2').css('display', v.tipo_pago === 'credito' ? 'block' : 'none');
                $('#col-cuotas').css('display', v.tipo_pago === 'credito' ? 'block' : 'none');
                if (v.tipo_pago === 'credito') {
                    $('#col-productos').addClass('col-sm-8');
                    $('#col-cuotas').addClass('col-sm-4');
                    var html = [];
                    var cuotas = JSON.parse(v.cuotas);
                    if (typeof cuotas === 'string') {
                        cuotas = JSON.parse(cuotas);
                    }
                    for (let i in cuotas) {
                        html.push('<tr>' + '<td>' + cuotas[i].id + '</td><td>' + cuotas[i].total + '</td><td class="text-center"><input disabled type="checkbox" ' + (cuotas[i].pagado ? 'checked' : '') + '></td></tr>');
                    }
                    $('#cuotasRegistros2').html(html.join(''));
                }

                var sumar = function () {
                    var sb = 0;
                    $.each(datos, function (e, tr) {
                        sb = parseInt(sb) + parseInt(tr.subtotal);
                    });
                    var htmlFoot = '<tr>\n\
                          <th colspan="6" style="text-align: right;background: #fff;border-bottom:none">\n\
                            <div class="col-md-12" style="display: inline-grid;">\n\
                                \n\<input type="hidden" name="total" value="' + W.format(sb) + '">\n\
                                <span style="font-size: 23px;font-weight: bold;font-family: monospace;">Total:<strong> ' + W.format(sb) + ' Bs</strong></span>\n\
                            </div>\n\
                          </th>\n\
                    </tr>';
                    $('#modal-info #costoTotal-info').html(htmlFoot);
                }
                sumar();
            }
        });

    };
    wn.print = function (event) {
        Wicoder.confirm({title: 'Esta Seguro de factura/nota?', content: 'En caso aceptar ya no podra modificar mas...',
            okay: {text: 'Aceptar',
                action: function () {
                    var srcurl = base_url + 'Venta/NotaVenta' + '?id=' + event;
                    let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                    $('#htmpprintRerp').html(h);
                    W.Modals({id: 'PdfProforma', width: '40'});
                }
            },
            cancel: {text: 'Cancelado', action: function () {
                    console.log('Clicked cancel button');
                }}
        });

    };
    wn.reporte = function (event) {

        var row = gridCotizador.getSelected();
        if (row != undefined) {

            var srcurl = $(event).attr('data-url') + '?id=' + row.id;
            console.log('reporte', srcurl);
            let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
            $('#htmpprintRerp').html(h);
            W.Modals({id: 'PdfProforma', width: '40'});

        } else {
            Wicoder.alert('Selecione un fila porfavor');
        }
    };

    window.venta = typeof define === 'function' && define.amd ? define(wn) : wn;
})(Wicoder.val);

$('#editarCliente').hide();
disabled({pared: 'dataCotizar', name: ['telefono', 'correo'], status: true});

$('#productoBuscar').autoComplete({
    minChars: 1,
    delay: 100,
    source: function (name, response) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url_produto,
            data: {value: name, "_token": csrf_token},
            success: function (data) {
                response(data.data);
            }
        });
    },
    renderItem: function (item, search) {
        search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
        return '<div class="autocomplete-suggestion" data-fabrica="' + item.id_fabrica + '"   data-stock="' + item.stock + '" \n\
           data-codigo="' + item.codigo + '"  data-precio="' + item.precio + '" \n\
            data-idproducto="' + item.id_producto + '" data-id="' + item.id + '"\n\
             data-producto="' + item.nombre + '">' + item.nombre.replace(re, "<b>$1</b>") + ' (' + item.stock + ')</div>';
    },
    onSelect: function (e, term, item) {

        datoinivtario = {
            id: item.data('id'),
            id_producto: item.data('idproducto'),
            producto: item.data('producto'),
            codigo: item.data('codigo'),
            precio: item.data('precio'),
            descuento: 0,
            fabrica: item.data('fabrica'),
            stock: item.data('stock')
        };

        $("#CargarProducto [name='id']").val(item.data('idproducto'));
        $("#CargarProducto [name='productoBuscar']").val(item.data('producto'));
        $("#CargarProducto .codigo").html("<strong>Producto:</strong> " + item.data('producto') + "  <br> " + "<strong>Codigo:</strong> " + item.data('codigo'));
        $("#CargarProducto [name='precio']").val(item.data('precio'));
        $("#CargarProducto [name='fabrica']").val(item.data('fabrica'));
        $("#CargarProducto .StockVal").text(item.data('stock'));

        if (item.data('stock') == 0) {
            alert('no puede pedir este producto');
            $('#agregarid').addClass('disabled');
        }else{
            $('#agregarid').removeClass('disabled');
        }

//        $("#CargarProducto #agregarid").data.id(item.data('id'));


    }
});


