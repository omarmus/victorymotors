alter table persona drop foreign key fk_persona_expedido;
alter table users drop foreign key fk_usuario_persona;
alter table users drop foreign key fk_usuario_rol;
alter table permiso drop foreign key fk_permiso_grupo;
alter table permiso drop foreign key fk_permiso_rol;
alter table permiso drop foreign key fk_permiso_menu;
alter table venta drop foreign key fk_cotizacion_cliente;
alter table venta drop foreign key fk_cotizacion_users;

alter table venta_detalle drop foreign key fk_vendetall_venta;
alter table venta_detalle drop foreign key fk_vendetall_producto;

alter table producto drop foreign key fk_producto_fabrica;

alter table inventario drop foreign key fk_inventario_producto;
alter table almacen drop foreign key fk_almacen_producto;
alter table compras drop foreign key fk_compras_producto;




ALTER TABLE persona
  ADD CONSTRAINT fk_persona_expedido FOREIGN KEY (id_expedido) REFERENCES expedido (id);

ALTER TABLE users
  ADD CONSTRAINT fk_usuario_persona FOREIGN KEY (id_persona) REFERENCES persona(id),
  ADD CONSTRAINT fk_usuario_rol FOREIGN KEY (id_rol) REFERENCES rol(id);


 alter table permiso
  add constraint fk_permiso_grupo foreign key (id_grupo) references grupo(id),
  add constraint fk_permiso_rol foreign key (id_rol) references rol(id),
  add constraint fk_permiso_menu foreign key (id_menu) references menu(id);

alter table venta 
    add constraint fk_cotizacion_cliente foreign key(id_cliente) references cliente(id),
    add constraint fk_cotizacion_users foreign key(id_users) references users(id);
 

alter table venta_detalle
    add constraint fk_vendetall_venta foreign key(id_venta) references venta(id),
    add constraint fk_vendetall_producto foreign key(id_producto) references producto(id);

alter table producto
    add constraint fk_producto_fabrica foreign key(id_fabrica) references fabrica(id);

alter table inventario
    add constraint fk_inventario_producto foreign key(id_producto) references producto(id);

alter table almacen
    add constraint fk_almacen_producto foreign key(id_producto) references producto(id);

-- alter table compras
--     add constraint fk_compras_pro foreign key(id_producto) references producto(id);

alter table compras
    add constraint fk_compras_proveedor foreign key(id_proveedor) references proveedor(id);











