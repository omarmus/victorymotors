
-- -----tables------------------------


DROP TABLE IF EXISTS proveedor;
CREATE TABLE IF NOT EXISTS proveedor (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  sitio_web varchar(200) NOT NULL,
  telefono varchar(20) NOT NULL,
  rl_nombre  varchar(50) NOT NULL,
  rl_apellidos  varchar(40) NOT NULL,
  rl_corrreo  varchar(40) NOT NULL,
  rl_telefono  varchar(20) NOT NULL,
  direccion  varchar(200) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS compras;
CREATE TABLE IF NOT EXISTS compras (
  id int NOT NULL   AUTO_INCREMENT,
  id_producto int NOT NULL,
  id_proveedor int NOT NULL,
  fecha date DEFAULT NULL,
  stock decimal(10,0) DEFAULT NULL,
  id_users int(11) NOT NULL,
  precio decimal(10,2) NOT NULL,
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id,id_producto),
  KEY  id(id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS fabrica;
CREATE TABLE IF NOT EXISTS fabrica (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS producto;
CREATE TABLE IF NOT EXISTS producto (
  id int(11) NOT NULL AUTO_INCREMENT,
  codigo varchar(15) NOT NULL,
  nombre varchar(80) NOT NULL,
  modelo varchar(50) NOT NULL,
  descripcion varchar(50) NOT NULL,
  id_fabrica int(11) NOT NULL,
  costo decimal(10,2) NOT NULL,
  logo varchar(80) DEFAULT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id(id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS inventario;
CREATE TABLE IF NOT EXISTS inventario (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_producto int NOT NULL,  
  precio decimal(10,2) NOT NULL,
  cantidad decimal(10,0) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_producto),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS almacen;
CREATE TABLE IF NOT EXISTS almacen ( 
  id int(11) NOT NULL AUTO_INCREMENT,
  id_producto varchar(50) NOT NULL,
  fecha date NOT NULL DEFAULT '2019-01-01',
  stock decimal(10,0) NOT NULL,
  id_users int(11) NOT NULL,
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_producto),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS cliente;
CREATE TABLE IF NOT EXISTS cliente (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) NOT NULL,
  telefono varchar(18) NOT NULL,
  correo varchar(40) NOT NULL,
  direccion varchar(18) NOT NULL,
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS venta;
CREATE TABLE IF NOT EXISTS venta (
  id int(11) NOT NULL AUTO_INCREMENT,
  codigo varchar(20) NOT NULL,
  placa varchar(30) NOT NULL,
  fecha date DEFAULT NULL,
  id_cliente int(11) NOT NULL,
  id_users int(11) NOT NULL,  
  total varchar(10) NOT NULL,
  avance varchar(10) NOT NULL,
  tipo_pago varchar(10) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY codigo (codigo),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS venta_detalle;
CREATE TABLE IF NOT EXISTS venta_detalle (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_venta int(11) DEFAULT NULL,
  id_producto varchar(50) NOT NULL,
  cantidad decimal(10,0) NOT NULL,
  precio_uni decimal(10,2) NOT NULL,
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY id (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;





-- -----vistas------------------------

-- 
-- DROP VIEW IF EXISTS view_fabrica;
-- CREATE VIEW view_fabrica  AS  select id,nombre,estado from fabrica where  eliminado = 0; 
-- 
-- DROP VIEW IF EXISTS view_cliente;
-- CREATE VIEW view_cliente  AS  select id,nombre,telefono,correo from cliente where  eliminado = 0; 
-- 
-- 
-- DROP VIEW IF EXISTS view_proveedor;
-- CREATE VIEW view_proveedor  AS  select id,nombre,razon_social,representante_legal,cuenta from proveedor where  eliminado = 0; 
-- 
-- 
-- 
-- 
-- DROP VIEW IF EXISTS view_inventario;
-- CREATE  VIEW view_inventario  AS 
--  select i.id AS id,i.id_producto AS id_producto,p.nombre AS producto,
-- p.id_marca AS id_marca,p.id_categoria AS id_categoria,m.nombre AS marca,c.nombre AS categoria,i.precio AS precio,i.stock AS stock,i.id_sucursal,
-- (select sucursal.nombre from sucursal where sucursal.id = i.id_sucursal) AS sucursal
--  from producto p 
--  join inventario i on p.id = i.id_producto
--  join marca m on  p.id_marca = m.id
--  join categoria c on p.id_categoria = c.id ;
-- 
-- 
-- DROP VIEW IF EXISTS view_cotizacion;
-- CREATE VIEW view_cotizacion  AS
--  select c.id,c.codigo,c.fecha,cl.correo,
-- cl.nombre as cliente,cl.telefono,c.valido_entrega,c.valido_cotizacion,c.tipo_pago,c.avance,
-- c.total AS total,vd.persona AS vendedor,
-- vd.id AS id_vendedor,c.actualizado AS actualizado
-- from cotizacion c
-- join cliente cl on c.cliente = cl.id
-- join view_usuario vd on c.vendedor = vd.id 
-- where c.eliminado = 0 ;
-- 
-- 
-- DROP VIEW IF EXISTS view_compras;   
-- CREATE VIEW view_compras  AS
-- select c.id,c.fecha,p.nombre AS producto,u.persona AS usuario,
-- pv.nombre AS proveedor,c.precio, c.stock,c.estado
-- from compras c 
-- join producto p on p.id = c.id_producto
-- join view_usuario u on  c.id_usuario=u.id
-- join proveedor pv on c.id_proveedor = pv.id 
-- where p.eliminado = 0 and p.estado = 1 ;
-- 
-- 
-- -- -------  *****
-- 
-- DROP VIEW IF EXISTS view_almacen;
-- CREATE VIEW view_almacen  AS 
-- select a.id,a.id_producto,p.nombre as producto,p.id_marca,
-- p.id_categoria,m.nombre as marca,a.precio,a.stock
-- from producto p
-- join almacen a on p.id=a.id_producto
-- join fabrica m on p.id_fabrica=m.id; 
-- 
-- 
-- 
-- 
-- DROP VIEW IF EXISTS view_producto;
-- CREATE VIEW view_producto  AS 
-- select p.id AS id,p.nombre,m.nombre AS marca,c.nombre AS categoria,p.logo,p.estado  
-- from producto p 
-- join marca m on p.id_marca = m.id
-- join categoria c on p.id_categoria = c.id 
-- where p.eliminado = 0 and m.eliminado = 0 and p.estado = 1 ;
-- 
-- 
-- 
-- 
-- INSERT INTO cliente(id, nombre, telefono, correo, eliminado, actualizado, creado)
--  VALUES (NULL, 'jaimito', '66565', 'jamito@gmail.com', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP), 
-- (NULL, 'pepito', '46666', 'pepito@gmail.com', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
-- 
-- INSERT INTO marca (id, nombre, estado, eliminado, actualizado, creado) VALUES
-- (4, '111', 1, 0, '2018-11-17 02:21:03', '2018-11-17 02:21:03'),
-- (2, 'carton', 1, 0, '2018-10-29 18:58:38', '2018-10-29 18:58:38'),
-- (3, 'cartondisc', 1, 0, '2018-10-29 19:28:16', '2018-10-29 19:27:38'),
-- (1, 'sony', 1, 0, '2018-10-29 18:58:38', '2018-10-29 18:58:38');
-- 
-- 
-- INSERT INTO categoria (id, nombre, estado, eliminado, actualizado, creado) VALUES
-- (12, 'CTP prensa e impresion', 1, 0, '2018-10-27 17:17:47', '2018-10-27 17:17:29'),
-- (11, 'Gigantografia (bastidores)', 1, 0, '2018-10-27 17:16:11', '2018-10-27 17:15:51'),
-- (10, 'Gigantografia (letreros luminosos)', 1, 0, '2018-10-27 17:16:08', '2018-10-27 17:15:37'),
-- (9, 'Gigantografia (lona normal)', 1, 0, '2018-10-27 17:16:07', '2018-10-27 17:15:05'),
-- (4, 'Impresion offset  Calendarios', 1, 0, '2018-10-27 17:15:59', '2018-10-27 17:13:24'),
-- (5, 'Impresion offset  Catalogos', 1, 0, '2018-10-27 17:16:01', '2018-10-27 17:13:32'),
-- (2, 'Impresion offset  Libros', 1, 0, '2018-10-27 17:13:07', '2018-10-27 15:39:05'),
-- (1, 'Impresion offset  Placas', 1, 0, '2018-10-27 17:13:12', '2018-10-27 15:39:01'),
-- (6, 'Impresion offset bipticos', 1, 0, '2018-10-27 17:16:03', '2018-10-27 17:14:10'),
-- (8, 'Impresion offset marcapaginas', 1, 0, '2018-10-27 17:16:06', '2018-10-27 17:14:25'),
-- (3, 'Impresion offset Tarejatas', 1, 0, '2018-10-27 17:13:00', '2018-10-27 17:10:17'),
-- (7, 'Impresion offset tripticos', 1, 0, '2018-10-27 17:16:04', '2018-10-27 17:14:16');
-- 
-- 
-- INSERT INTO producto (id, nombre, id_marca, id_categoria, logo, estado, eliminado, actualizado, creado) 
-- VALUES (NULL, 'hojas bon', '1', '1', 'default.png', 1, '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP), 
-- (NULL, 'carton liso', '1', '2', 'default.png', 1, '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);



-- INSERT INTO `cotizacion` (`fecha`, `codigo`, `cliente`, `vendedor`, `tipo_pago`,
--  `valido_cotizacion`, `valido_entrega`, `socursal`, `avance`, `total`) VALUES ('2019-08-29', 'COTIW-00001-2019', '2', '1', '1', '1', '1', 1, 1, '144.00');
-- 
-- 
-- INSERT INTO inventario (id, id_producto, id_sucursal, precio, stock, estado, eliminado, actualizado, creado) VALUES (NULL, '1', '1', '12', '1231', '1', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP), (NULL, '2', '1', '32', '321321', '1', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- select * from view_marca