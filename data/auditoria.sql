DROP TRIGGER IF EXISTS `insertusers`;
DROP TRIGGER IF EXISTS `deleteusers`;
DROP TRIGGER IF EXISTS `updateusers`;
DROP TRIGGER IF EXISTS `insertcliente`;
DROP TRIGGER IF EXISTS `deletecliente`;
DROP TRIGGER IF EXISTS `updatecliente`;
DROP TRIGGER IF EXISTS `insertempresa`;
DROP TRIGGER IF EXISTS `deleteempresa`;
DROP TRIGGER IF EXISTS `updateempresa`;
DROP TRIGGER IF EXISTS `insertinventario`;
DROP TRIGGER IF EXISTS `deleteinventario`;
DROP TRIGGER IF EXISTS `updateinventario`;
DROP TRIGGER IF EXISTS `insertpersona`;
DROP TRIGGER IF EXISTS `deletepersona`;
DROP TRIGGER IF EXISTS `updatepersona`;
DROP TRIGGER IF EXISTS `insertproducto`;
DROP TRIGGER IF EXISTS `deleteproducto`;
DROP TRIGGER IF EXISTS `updateproducto`;
DROP TRIGGER IF EXISTS `insertproveedor`;
DROP TRIGGER IF EXISTS `deleteproveedor`;
DROP TRIGGER IF EXISTS `updateproveedor`;
DROP TRIGGER IF EXISTS `insertventa`;
DROP TRIGGER IF EXISTS `deleteventa`;
DROP TRIGGER IF EXISTS `updateventa`;
DROP TRIGGER IF EXISTS `insertventa_detalle`;
DROP TRIGGER IF EXISTS `deleteventa_detalle`;
DROP TRIGGER IF EXISTS `updateventa_detalle`;

DROP TABLE IF EXISTS `inventario`.`log`;
CREATE TABLE `inventario`.`log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `old` text,
  `new` text,
  `valor_alterado` text,
  `usuario` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `fecha` timestamp(6) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  `eliminado` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1379 DEFAULT CHARSET=utf8;

# TRIGGERS USERS

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertusers`
BEFORE INSERT ON `users` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;usuario=',ifnull(new.usuario,''),
    ' ;email=',ifnull(new.email,''),
    ' ;password=',ifnull(new.password,''),
    ' ;id_rol=',ifnull(new.id_rol,''),
    ' ;id_persona=',ifnull(new.id_persona,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"users");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteusers`
AFTER DELETE ON `users` FOR EACH ROW
BEGIN
  SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;usuario=',ifnull(old.usuario,''),
    ' ;email=',ifnull(old.email,''),
    ' ;password=',ifnull(old.password,''),
    ' ;id_rol=',ifnull(old.id_rol,''),
    ' ;id_persona=',ifnull(old.id_persona,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"users");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateusers`
BEFORE UPDATE ON `users` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;usuario=',ifnull(old.usuario,''),
    ' ;email=',ifnull(old.email,''),
    ' ;password=',ifnull(old.password,''),
    ' ;id_rol=',ifnull(old.id_rol,''),
    ' ;id_persona=',ifnull(old.id_persona,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;usuario=',ifnull(new.usuario,''),
    ' ;email=',ifnull(new.email,''),
    ' ;password=',ifnull(new.password,''),
    ' ;id_rol=',ifnull(new.id_rol,''),
    ' ;id_persona=',ifnull(new.id_persona,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.usuario <> new.usuario THEN set @res = CONCAT (@res, ' ;Cambió el valor de usuario: ',ifnull(OLD.usuario,''), ' a: ',ifnull(new.usuario,'')); END IF;
	IF OLD.email <> new.email THEN set @res = CONCAT (@res, ' ;Cambió el valor de email: ',ifnull(OLD.email,''), ' a: ',ifnull(new.email,'')); END IF;
	IF OLD.password <> new.password THEN set @res = CONCAT (@res, ' ;Cambió el valor de password: ',ifnull(OLD.password,''), ' a: ',ifnull(new.password,'')); END IF;
	IF OLD.id_rol <> new.id_rol THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_rol: ',ifnull(OLD.id_rol,''), ' a: ',ifnull(new.id_rol,'')); END IF;
	IF OLD.id_persona <> new.id_persona THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_persona: ',ifnull(OLD.id_persona,''), ' a: ',ifnull(new.id_persona,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

	IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"users");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"users",ifnull(@res,'No cambio nada'));
    END IF;

END$$

# TRIGGERS CLIENTE

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertcliente`
BEFORE INSERT ON `cliente` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"cliente");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deletecliente`
AFTER DELETE ON `cliente` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"cliente");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updatecliente`
BEFORE UPDATE ON `cliente` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' ;Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"cliente");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"cliente",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS EMPRESA

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertempresa`
BEFORE INSERT ON `empresa` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;red_social=',ifnull(new.red_social,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"empresa");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteempresa`
AFTER DELETE ON `empresa` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;red_social=',ifnull(old.red_social,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"empresa");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateempresa`
BEFORE UPDATE ON `empresa` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;red_social=',ifnull(old.red_social,''),
    ' ;correo=',ifnull(old.correo,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;red_social=',ifnull(new.red_social,''),
    ' ;correo=',ifnull(new.correo,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.red_social <> new.red_social THEN set @res = CONCAT (@res, ' ;Cambió el valor de red_social: ',ifnull(OLD.red_social,''), ' a: ',ifnull(new.red_social,'')); END IF;
	IF OLD.correo <> new.correo THEN set @res = CONCAT (@res, ' ;Cambió el valor de correo: ',ifnull(OLD.correo,''), ' a: ',ifnull(new.correo,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"empresa");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"empresa",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS INVENTARIO

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertinventario`
BEFORE INSERT ON `inventario` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;precio=',ifnull(new.precio,''),
    ' ;stock=',ifnull(new.stock,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"inventario");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteinventario`
AFTER DELETE ON `inventario` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;precio=',ifnull(old.precio,''),
    ' ;stock=',ifnull(old.stock,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"inventario");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateinventario`
BEFORE UPDATE ON `inventario` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;precio=',ifnull(old.precio,''),
    ' ;stock=',ifnull(old.stock,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;precio=',ifnull(new.precio,''),
    ' ;stock=',ifnull(new.stock,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.id_producto <> new.id_producto THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_producto: ',ifnull(OLD.id_producto,''), ' a: ',ifnull(new.id_producto,'')); END IF;
	IF OLD.precio <> new.precio THEN set @res = CONCAT (@res, ' ;Cambió el valor de precio: ',ifnull(OLD.precio,''), ' a: ',ifnull(new.precio,'')); END IF;
	IF OLD.stock <> new.stock THEN set @res = CONCAT (@res, ' ;Cambió el valor de stock: ',ifnull(OLD.stock,''), ' a: ',ifnull(new.stock,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"inventario");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"inventario",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS PERSONA

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertpersona`
BEFORE INSERT ON `persona` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;paterno=',ifnull(new.paterno,''),
    ' ;materno=',ifnull(new.materno,''),
    ' ;ci=',ifnull(new.ci,''),
    ' ;id_expedido=',ifnull(new.id_expedido,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"persona");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deletepersona`
AFTER DELETE ON `persona` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;paterno=',ifnull(old.paterno,''),
    ' ;materno=',ifnull(old.materno,''),
    ' ;ci=',ifnull(old.ci,''),
    ' ;id_expedido=',ifnull(old.id_expedido,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"persona");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updatepersona`
BEFORE UPDATE ON `persona` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;paterno=',ifnull(old.paterno,''),
    ' ;materno=',ifnull(old.materno,''),
    ' ;ci=',ifnull(old.ci,''),
    ' ;id_expedido=',ifnull(old.id_expedido,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;paterno=',ifnull(new.paterno,''),
    ' ;materno=',ifnull(new.materno,''),
    ' ;ci=',ifnull(new.ci,''),
    ' ;id_expedido=',ifnull(new.id_expedido,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.paterno <> new.paterno THEN set @res = CONCAT (@res, ' ;Cambió el valor de paterno: ',ifnull(OLD.paterno,''), ' a: ',ifnull(new.paterno,'')); END IF;
	IF OLD.materno <> new.materno THEN set @res = CONCAT (@res, ' ;Cambió el valor de materno: ',ifnull(OLD.materno,''), ' a: ',ifnull(new.materno,'')); END IF;
	IF OLD.ci <> new.ci THEN set @res = CONCAT (@res, ' ;Cambió el valor de ci: ',ifnull(OLD.ci,''), ' a: ',ifnull(new.ci,'')); END IF;
	IF OLD.id_expedido <> new.id_expedido THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_expedido: ',ifnull(OLD.id_expedido,''), ' a: ',ifnull(new.id_expedido,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"persona");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"persona",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS PRODUCTO

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertproducto`
BEFORE INSERT ON `producto` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;modelo=',ifnull(new.modelo,''),
    ' ;chasis=',ifnull(new.chasis,''),
    ' ;motor=',ifnull(new.motor,''),
    ' ;descripcion=',ifnull(new.descripcion,''),
    ' ;id_fabrica=',ifnull(new.id_fabrica,''),
    ' ;costo=',ifnull(new.costo,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"producto");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteproducto`
AFTER DELETE ON `producto` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;modelo=',ifnull(old.modelo,''),
    ' ;chasis=',ifnull(old.chasis,''),
    ' ;motor=',ifnull(old.motor,''),
    ' ;descripcion=',ifnull(old.descripcion,''),
    ' ;id_fabrica=',ifnull(old.id_fabrica,''),
    ' ;costo=',ifnull(old.costo,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"producto");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateproducto`
BEFORE UPDATE ON `producto` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;modelo=',ifnull(old.modelo,''),
    ' ;chasis=',ifnull(old.chasis,''),
    ' ;motor=',ifnull(old.motor,''),
    ' ;descripcion=',ifnull(old.descripcion,''),
    ' ;id_fabrica=',ifnull(old.id_fabrica,''),
    ' ;costo=',ifnull(old.costo,''),
    ' ;logo=',ifnull(old.logo,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;modelo=',ifnull(new.modelo,''),
    ' ;chasis=',ifnull(new.chasis,''),
    ' ;motor=',ifnull(new.motor,''),
    ' ;descripcion=',ifnull(new.descripcion,''),
    ' ;id_fabrica=',ifnull(new.id_fabrica,''),
    ' ;costo=',ifnull(new.costo,''),
    ' ;logo=',ifnull(new.logo,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' ;Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.modelo <> new.modelo THEN set @res = CONCAT (@res, ' ;Cambió el valor de modelo: ',ifnull(OLD.modelo,''), ' a: ',ifnull(new.modelo,'')); END IF;
	IF OLD.chasis <> new.chasis THEN set @res = CONCAT (@res, ' ;Cambió el valor de chasis: ',ifnull(OLD.chasis,''), ' a: ',ifnull(new.chasis,'')); END IF;
	IF OLD.motor <> new.motor THEN set @res = CONCAT (@res, ' ;Cambió el valor de motor: ',ifnull(OLD.motor,''), ' a: ',ifnull(new.motor,'')); END IF;
	IF OLD.descripcion <> new.descripcion THEN set @res = CONCAT (@res, ' ;Cambió el valor de descripcion: ',ifnull(OLD.descripcion,''), ' a: ',ifnull(new.descripcion,'')); END IF;
	IF OLD.id_fabrica <> new.id_fabrica THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_fabrica: ',ifnull(OLD.id_fabrica,''), ' a: ',ifnull(new.id_fabrica,'')); END IF;
	IF OLD.costo <> new.costo THEN set @res = CONCAT (@res, ' ;Cambió el valor de costo: ',ifnull(OLD.costo,''), ' a: ',ifnull(new.costo,'')); END IF;
	IF OLD.logo <> new.logo THEN set @res = CONCAT (@res, ' ;Cambió el valor de logo: ',ifnull(OLD.logo,''), ' a: ',ifnull(new.logo,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"producto");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"producto",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS PROVEEDOR

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertproveedor`
BEFORE INSERT ON `proveedor` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;sitio_web=',ifnull(new.sitio_web,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;rl_nombre=',ifnull(new.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(new.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(new.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(new.rl_telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"proveedor");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteproveedor`
AFTER DELETE ON `proveedor` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;sitio_web=',ifnull(old.sitio_web,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;rl_nombre=',ifnull(old.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(old.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(old.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(old.rl_telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"proveedor");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateproveedor`
BEFORE UPDATE ON `proveedor` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;nombre=',ifnull(old.nombre,''),
    ' ;sitio_web=',ifnull(old.sitio_web,''),
    ' ;telefono=',ifnull(old.telefono,''),
    ' ;rl_nombre=',ifnull(old.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(old.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(old.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(old.rl_telefono,''),
    ' ;direccion=',ifnull(old.direccion,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;nombre=',ifnull(new.nombre,''),
    ' ;sitio_web=',ifnull(new.sitio_web,''),
    ' ;telefono=',ifnull(new.telefono,''),
    ' ;rl_nombre=',ifnull(new.rl_nombre,''),
    ' ;rl_apellidos=',ifnull(new.rl_apellidos,''),
    ' ;rl_corrreo=',ifnull(new.rl_corrreo,''),
    ' ;rl_telefono=',ifnull(new.rl_telefono,''),
    ' ;direccion=',ifnull(new.direccion,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,'')
  );

  set @res = '';
	IF OLD.nombre <> new.nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de nombre: ',ifnull(OLD.nombre,''), ' a: ',ifnull(new.nombre,'')); END IF;
	IF OLD.sitio_web <> new.sitio_web THEN set @res = CONCAT (@res, ' ;Cambió el valor de sitio_web: ',ifnull(OLD.sitio_web,''), ' a: ',ifnull(new.sitio_web,'')); END IF;
	IF OLD.telefono <> new.telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de telefono: ',ifnull(OLD.telefono,''), ' a: ',ifnull(new.telefono,'')); END IF;
	IF OLD.rl_nombre <> new.rl_nombre THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_nombre: ',ifnull(OLD.rl_nombre,''), ' a: ',ifnull(new.rl_nombre,'')); END IF;
	IF OLD.rl_apellidos <> new.rl_apellidos THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_apellidos: ',ifnull(OLD.rl_apellidos,''), ' a: ',ifnull(new.rl_apellidos,'')); END IF;
	IF OLD.rl_corrreo <> new.rl_corrreo THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_corrreo: ',ifnull(OLD.rl_corrreo,''), ' a: ',ifnull(new.rl_corrreo,'')); END IF;
	IF OLD.rl_telefono <> new.rl_telefono THEN set @res = CONCAT (@res, ' ;Cambió el valor de rl_telefono: ',ifnull(OLD.rl_telefono,''), ' a: ',ifnull(new.rl_telefono,'')); END IF;
	IF OLD.direccion <> new.direccion THEN set @res = CONCAT (@res, ' ;Cambió el valor de direccion: ',ifnull(OLD.direccion,''), ' a: ',ifnull(new.direccion,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"proveedor");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"proveedor",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS VENTA

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertventa`
BEFORE INSERT ON `venta` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;fecha=',ifnull(new.fecha,''),
    ' ;id_cliente=',ifnull(new.id_cliente,''),
    ' ;id_users=',ifnull(new.id_users,''),
    ' ;total=',ifnull(new.total,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;avance=',ifnull(new.avance,''),
    ' ;tipo_pago=',ifnull(new.tipo_pago,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"venta");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteventa`
AFTER DELETE ON `venta` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;fecha=',ifnull(old.fecha,''),
    ' ;id_cliente=',ifnull(old.id_cliente,''),
    ' ;id_users=',ifnull(old.id_users,''),
    ' ;total=',ifnull(old.total,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;avance=',ifnull(old.avance,''),
    ' ;tipo_pago=',ifnull(old.tipo_pago,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"venta");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateventa`
BEFORE UPDATE ON `venta` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;codigo=',ifnull(old.codigo,''),
    ' ;fecha=',ifnull(old.fecha,''),
    ' ;id_cliente=',ifnull(old.id_cliente,''),
    ' ;id_users=',ifnull(old.id_users,''),
    ' ;total=',ifnull(old.total,''),
    ' ;estado=',ifnull(old.estado,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;avance=',ifnull(old.avance,''),
    ' ;tipo_pago=',ifnull(old.tipo_pago,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;codigo=',ifnull(new.codigo,''),
    ' ;fecha=',ifnull(new.fecha,''),
    ' ;id_cliente=',ifnull(new.id_cliente,''),
    ' ;id_users=',ifnull(new.id_users,''),
    ' ;total=',ifnull(new.total,''),
    ' ;estado=',ifnull(new.estado,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;avance=',ifnull(new.avance,''),
    ' ;tipo_pago=',ifnull(new.tipo_pago,'')
  );

  set @res = '';
	IF OLD.codigo <> new.codigo THEN set @res = CONCAT (@res, ' ;Cambió el valor de codigo: ',ifnull(OLD.codigo,''), ' a: ',ifnull(new.codigo,'')); END IF;
	IF OLD.fecha <> new.fecha THEN set @res = CONCAT (@res, ' ;Cambió el valor de fecha: ',ifnull(OLD.fecha,''), ' a: ',ifnull(new.fecha,'')); END IF;
	IF OLD.id_cliente <> new.id_cliente THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_cliente: ',ifnull(OLD.id_cliente,''), ' a: ',ifnull(new.id_cliente,'')); END IF;
	IF OLD.id_users <> new.id_users THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_users: ',ifnull(OLD.id_users,''), ' a: ',ifnull(new.id_users,'')); END IF;
	IF OLD.total <> new.total THEN set @res = CONCAT (@res, ' ;Cambió el valor de total: ',ifnull(OLD.total,''), ' a: ',ifnull(new.total,'')); END IF;
	IF OLD.estado <> new.estado THEN set @res = CONCAT (@res, ' ;Cambió el valor de estado: ',ifnull(OLD.estado,''), ' a: ',ifnull(new.estado,'')); END IF;
	IF OLD.avance <> new.avance THEN set @res = CONCAT (@res, ' ;Cambió el valor de avance: ',ifnull(OLD.avance,''), ' a: ',ifnull(new.avance,'')); END IF;
	IF OLD.tipo_pago <> new.tipo_pago THEN set @res = CONCAT (@res, ' ;Cambió el valor de tipo_pago: ',ifnull(OLD.tipo_pago,''), ' a: ',ifnull(new.tipo_pago,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"venta");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"venta",ifnull(@res,'No cambio nada'));
    END IF;
END$$

# TRIGGERS VENTA_DETALLE

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `insertventa_detalle`
BEFORE INSERT ON `venta_detalle` FOR EACH ROW
BEGIN
  SET @oldq = '';
  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_venta=',ifnull(new.id_venta,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;cantidad=',ifnull(new.cantidad,''),
    ' ;precio_uni=',ifnull(new.precio_uni,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;descuento=',ifnull(new.descuento,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq,@newq,CURRENT_USER,"INSERT",NOW(),"venta_detalle");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `deleteventa_detalle`
AFTER DELETE ON `venta_detalle` FOR EACH ROW
BEGIN
	SET @newq = '';
  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_venta=',ifnull(old.id_venta,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;cantidad=',ifnull(old.cantidad,''),
    ' ;precio_uni=',ifnull(old.precio_uni,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;descuento=',ifnull(old.descuento,'')
  );
	INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
    VALUES (@oldq ,@newq,CURRENT_USER,"DELETE",NOW(),"venta_detalle");
END$$

DELIMITER $$
USE `inventario`$$
CREATE TRIGGER `updateventa_detalle`
BEFORE UPDATE ON `venta_detalle` FOR EACH ROW #aqui puedes poner antes o despues del update
BEGIN

  SET @oldq = CONCAT (
    ' ;id=',ifnull(old.id,''),
    ' ;id_venta=',ifnull(old.id_venta,''),
    ' ;id_producto=',ifnull(old.id_producto,''),
    ' ;cantidad=',ifnull(old.cantidad,''),
    ' ;precio_uni=',ifnull(old.precio_uni,''),
    ' ;eliminado=',ifnull(old.eliminado,''),
    ' ;descuento=',ifnull(old.descuento,'')
  );

  SET @newq = CONCAT (
    ' ;id=',ifnull(new.id,''),
    ' ;id_venta=',ifnull(new.id_venta,''),
    ' ;id_producto=',ifnull(new.id_producto,''),
    ' ;cantidad=',ifnull(new.cantidad,''),
    ' ;precio_uni=',ifnull(new.precio_uni,''),
    ' ;eliminado=',ifnull(new.eliminado,''),
    ' ;descuento=',ifnull(new.descuento,'')
  );

  set @res = '';
	IF OLD.id_venta <> new.id_venta THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_venta: ',ifnull(OLD.id_venta,''), ' a: ',ifnull(new.id_venta,'')); END IF;
	IF OLD.id_producto <> new.id_producto THEN set @res = CONCAT (@res, ' ;Cambió el valor de id_producto: ',ifnull(OLD.id_producto,''), ' a: ',ifnull(new.id_producto,'')); END IF;
	IF OLD.cantidad <> new.cantidad THEN set @res = CONCAT (@res, ' ;Cambió el valor de cantidad: ',ifnull(OLD.cantidad,''), ' a: ',ifnull(new.cantidad,'')); END IF;
	IF OLD.precio_uni <> new.precio_uni THEN set @res = CONCAT (@res, ' ;Cambió el valor de precio_uni: ',ifnull(OLD.precio_uni,''), ' a: ',ifnull(new.precio_uni,'')); END IF;
	IF OLD.descuento <> new.descuento THEN set @res = CONCAT (@res, ' ;Cambió el valor de descuento: ',ifnull(OLD.descuento,''), ' a: ',ifnull(new.descuento,'')); END IF;

    IF OLD.eliminado <> new.eliminado THEN
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla)
        VALUES (@oldq,'',CURRENT_USER,"DELETE",NOW(),"venta_detalle");
    ELSE
        INSERT INTO log (old,new,usuario,tipo,fecha,tabla,valor_alterado)
        VALUES (@oldq,@newq,CURRENT_USER,"UPDATE",NOW(),"venta_detalle",ifnull(@res,'No cambio nada'));
    END IF;
END$$
