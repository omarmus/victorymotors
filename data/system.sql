-- select * from usuario
/**
 * Author:  wicoder
 * Created: 08-may-2019
 */
-- CREATE DATABASE mysqlbase;



DROP TABLE IF EXISTS  empresa;
DROP TABLE IF EXISTS  expedido;
DROP TABLE IF EXISTS  persona;
DROP TABLE IF EXISTS  rol;
DROP TABLE IF EXISTS  users;
DROP TABLE IF EXISTS  grupo;
DROP TABLE IF EXISTS  menu;
DROP TABLE IF EXISTS  permiso;
DROP TABLE IF EXISTS  perfil;




CREATE TABLE IF NOT EXISTS  empresa (
  id int(30) NOT NULL AUTO_INCREMENT,
  nombre varchar(60) NOT NULL,
  red_social varchar(50) NOT NULL,
  correo varchar(30) NOT NULL,
  telefono int(30) DEFAULT NULL,
  direccion varchar(150) NOT NULL,
  logo varchar(36) DEFAULT 'default.png',
  tema enum('oscuro','naranja','azul') DEFAULT NULL,
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS perfil (
  id int(11) NOT NULL AUTO_INCREMENT,
  header varchar(100) NOT NULL,
  vertical  varchar(100) NOT NULL,
  fondo  varchar(100) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (header),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  expedido (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
 updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS  persona (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(40) NOT NULL,
  paterno varchar(30) DEFAULT '',
  materno varchar(30) DEFAULT '',
  ci varchar(15) NOT NULL,
  id_expedido int(11) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
 updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ci),
  KEY id (id),
  KEY fk_persona_expedido (id_expedido)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS  rol (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) NOT NULL,
  descripcion varchar(190) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- $2y$10$b21pGDO99uWFwxs83j2TT.aPBRxNfW2guYC/u4fqldngmiT4Ghmo.

CREATE TABLE IF NOT EXISTS  users (
  id int(11) NOT NULL AUTO_INCREMENT,
  usuario varchar(30) NOT NULL,
  email varchar(78) NOT NULL,
  password varchar(100) NOT NULL,
  id_rol int(11) DEFAULT '2',
  id_persona int(11) NOT NULL,
  id_perfil int(11) NOT NULL DEFAULT 1,
  logo varchar(100) NOT NULL DEFAULT 'wicoder.png',
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (usuario),
  KEY id (id),
  KEY fk_usuario_rol (id_rol),
  KEY fk_usuario_persona (id_persona)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS  grupo (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(30) NOT NULL,
  icos varchar(25) NOT NULL default 'fa-table',
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS  menu (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(25) NOT NULL,
  titulo varchar(50) NOT NULL,
  url varchar(40) NOT NULL,
  icos varchar(25) NOT NULL,
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (nombre,url),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  permiso (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_grupo int(11) NOT NULL,
  id_menu int(11) NOT NULL,
  id_rol int(11) NOT NULL,
  isdelete tinyint(1) NOT NULL DEFAULT '1',
  isupdate tinyint(1) NOT NULL DEFAULT '1',
  isnew tinyint(1) NOT NULL DEFAULT '1',
  isreporte tinyint(1) NOT NULL DEFAULT '0',
  isviewall tinyint(1) NOT NULL DEFAULT '0',
  isview tinyint(1) NOT NULL DEFAULT '0',
  estado tinyint(1) DEFAULT '1',
  eliminado tinyint(1) DEFAULT '0',
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_grupo,id_menu,id_rol),
  KEY id (id)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


/***SCRIPT**/
INSERT INTO  empresa (id, nombre, red_social, correo, telefono, direccion, logo, tema, eliminado, actualizado, creado) VALUES
(1, 'Empesa nnnnnnnn', 'faceboo.com/emm', 'aaaa@gmail.com', 444, 'calle los pinos', 'default.png', 'azul', 0, '2019-03-10 01:58:57', '2018-10-27 19:26:30');

INSERT INTO sucursal (id, nombre, descripion, telefono, estado, eliminado, actualizado, creado)
 VALUES (NULL, 'coplejo', 'fotos con como', 'rttretre', '1', '0', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

INSERT INTO  rol (id, nombre, descripcion, estado, eliminado, actualizado, creado) VALUES
(1, 'Administrador', 'gerente de la empresa', 1, 0, '2019-02-15 20:24:10', '2018-11-17 06:15:52'),
(2, 'Vendedor', 'personal de la empresa ...', 1, 0, '2018-11-17 06:15:22', '2018-10-17 01:05:16');

INSERT INTO  expedido (id, nombre, estado, eliminado, actualizado, creado) VALUES
(2, 'CBBA', 1, 0, '2018-08-12 02:36:45', '2018-08-12 02:36:45'),
(7, 'CH', 1, 0, '2018-08-22 02:16:08', '2018-08-22 02:15:53'),
(1, 'LP', 1, 0, '2018-08-12 02:36:45', '2018-08-12 02:36:45'),
(3, 'SRC', 1, 0, '2018-08-12 02:36:45', '2018-08-12 02:36:45');

INSERT INTO  persona (id, nombre, paterno, materno, ci, id_expedido, estado, eliminado, actualizado, creado) VALUES
(2, 'tests', 'tests', 'vee', '321312', 1, 1, 0, '2018-11-17 06:17:37', '2018-11-17 06:17:37'),
(1, 'Wilmer', 'Quispe', 'Assss', '8522452', 1, 1, 0, '2018-11-20 23:35:37', '2018-10-17 01:00:11');

INSERT INTO  usuario (id, usuario,id_sucursal, clave, id_rol, id_persona,logo) VALUES
(1, 'admin', 1,'21232f297a57a5a743894a0e4a801fc3', 1, 1, 'wicoder.png'),
(5, 'vendedor1',1, '21232f297a57a5a743894a0e4a801fc3', 2, 2, 'wicoder.png');


INSERT INTO  grupo(id, nombre, estado, eliminado, actualizado, creado) VALUES
(1, 'Administrador', 1, 0, '2018-10-17 05:01:13', '2018-10-17 05:01:13'),
(5, 'Asignacion Diaria', 1, 0, '2019-05-13 01:56:47', '2018-10-17 05:01:13'),
(2, 'Configuracion', 1, 0, '2018-10-17 05:01:13', '2018-10-17 05:01:13'),
(3, 'Productos', 1, 0, '2018-10-30 03:00:23', '2018-10-30 03:00:23'),
(6, 'Reportes', 1, 0, '2019-05-15 00:20:31', '2019-05-15 00:20:31');

INSERT INTO  menu(id, nombre,titulo, url, icos, estado, eliminado, actualizado, creado) VALUES
(2, 'Acceso','Acceso', 'Acceso/desing', 'wif-file', 1, 0, '2018-10-23 22:11:19', '2018-10-17 01:02:42'),
(23, 'almacen','almacen', 'Almacen/inicio', 'wif-file', 1, 0, '2019-02-16 13:01:15', '2019-02-16 13:01:15'),
(9, 'categoria','categoria', 'main/categoria', 'wif-file', 1, 0, '2018-12-10 19:12:05', '2018-10-27 19:37:51'),
(18, 'cliente','cliente', 'main/cliente', 'wif-human', 1, 0, '2018-12-10 19:12:13', '2018-11-13 19:03:28'),
(20, 'compras','compras', 'Compras/inicio', 'wif-file', 1, 0, '2018-12-16 01:11:33', '2018-12-15 23:35:19'),
(16, 'cotizacion','cotizacion', 'Cotizacion/inicio', 'wif-file', 1, 0, '2018-11-15 06:58:13', '2018-10-30 17:35:04'),
(8, 'empresa','empresa', 'Admin/Empresa/inicio', 'wif-file', 1, 0, '2018-10-27 03:58:09', '2018-10-23 22:32:58'),
(7, 'expedido','expedido', 'main/expedido', 'wif-file', 1, 0, '2018-12-10 19:12:17', '2018-10-22 19:05:06'),
(13, 'grupo_menu','grupo_menu', 'main/grupo_menu', 'wif-file', 1, 0, '2018-12-10 19:12:24', '2018-10-29 22:59:35'),
(21, 'inventario','inventario', 'Inventario/inicio', 'wif-file', 1, 0, '2019-02-16 13:01:30', '2018-12-16 02:19:20'),
(15, 'marca','marca', 'main/marca', 'wif-file', 1, 0, '2018-12-10 19:12:26', '2018-10-29 23:26:09'),
(1, 'menu','menu', 'main/menu', 'wif-file', 1, 0, '2018-12-10 19:12:29', '2018-10-17 01:02:42'),
-- (25, 'parametros','parametros', 'Data/inicio', 'wif-file', 1, 0, '2019-08-25 01:55:35', '2019-08-25 01:55:35'),
(26, 'slider','Slider', 'Slider/inicio', 'wif-file', 1, 0, '2019-08-25 01:55:35', '2019-08-25 01:55:35'),
(3, 'persona','persona', 'main/persona', 'wif-file', 1, 0, '2018-12-10 19:12:32', '2018-10-17 01:02:54'),
(14, 'producto','producto', 'Producto/inicio', 'wif-file', 1, 0, '2019-01-21 21:31:16', '2018-10-29 23:00:43'),
(19, 'proveedor','proveedor', 'main/proveedor', 'wif-file', 1, 0, '2018-12-15 10:53:39', '2018-12-15 10:53:39'),
(5, 'rol','rol', 'main/rol', 'wif-file', 1, 0, '2018-12-10 19:12:38', '2018-10-22 17:58:14'),
(24, 'sucursal','sucursal', 'main/sucursal', 'wif-file', 1, 0, '2019-08-25 01:17:35', '2019-08-25 01:04:33'),
(6, 'usuario','usuario', 'main/usuario', 'wif-file', 1, 0, '2018-12-10 19:12:41', '2018-10-22 17:58:35');


INSERT INTO permiso (id, id_grupo, id_menu, id_rol, isdelete, isupdate, isnew, eliminado, isreporte, estado, actualizado, creado, isviewall, isview) VALUES
(13, 1, 3, 1, 1, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:57', 1, 0),
(14, 1, 5, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:59', 1, 0),
(15, 1, 6, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:40:02', 1, 0),
(9, 1, 7, 1, 0, 1, 0, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:49', 1, 0),
(16, 2, 1, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:48:07', '2018-12-16 08:40:04', 1, 0),
(1, 2, 2, 1, 1, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:38:37', 1, 0),
(8, 2, 8, 1, 0, 1, 0, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:48', 1, 0),
(11, 2, 13, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:52', 1, 0),
(23, 2, 25, 1, 1, 1, 1, 0, 0, 1, '2019-08-25 09:55:55', '2019-08-25 09:55:55', 0, 0),
(22, 2, 26, 1, 1, 1, 1, 0, 0, 1, '2019-09-01 07:36:45', '2019-08-25 09:55:55', 1, 0),
(2, 3, 9, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:13', 1, 0),
(4, 3, 14, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:23', 1, 0),
(28, 3, 15, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2019-08-29 07:18:31', 1, 0),
(27, 3, 18, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2019-08-29 07:18:27', 1, 0),
(25, 3, 19, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2019-08-29 07:18:21', 1, 0),
(3, 3, 20, 1, 0, 0, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:17', 1, 0),
(26, 3, 21, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2019-08-29 07:18:25', 1, 0),
(20, 3, 23, 1, 0, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2019-02-16 21:04:16', 1, 0),
(24, 3, 24, 1, 0, 1, 1, 0, 0, 1, '2019-09-02 01:54:53', '2019-08-29 07:18:18', 1, 0),
(6, 5, 16, 1, 1, 1, 1, 0, 0, 1, '2019-09-01 18:47:41', '2018-12-16 08:39:40', 1, 0);


-- -------------------------------------------------------------------------------------
-- 
-- DROP VIEW IF EXISTS view_sucursal;
-- CREATE VIEW view_sucursal  AS  select id,nombre,descripion,telefono,estado from sucursal where  eliminado = 0; 
-- 
-- 
-- DROP VIEW IF EXISTS view_rol;
-- CREATE VIEW view_rol  AS 
-- select rol.id AS id,rol.nombre AS nombre,rol.estado AS estado from rol where  eliminado = 0; 
-- 
-- 
-- DROP VIEW IF EXISTS view_usuario;
-- CREATE VIEW view_usuario  AS 
-- select u.id AS id,concat(p.nombre,' ',p.paterno,' ',p.materno) AS persona,u.clave AS clave,u.usuario,r.nombre AS rol,u.estado,s.nombre as sucursal
-- from usuario u join rol r on u.id_rol = r.id 
-- join persona p on u.id_persona = p.id 
-- join sucursal s on u.id_sucursal = s.id where u.eliminado = 0 ;
-- 
-- 
-- DROP VIEW IF EXISTS view_expedido;
-- CREATE VIEW view_expedido  AS
-- select expedido.id AS id,expedido.nombre AS nombre,expedido.estado AS estado  from expedido  where  eliminado = 0; 
-- 
-- DROP VIEW IF EXISTS view_grupo_menu;
-- CREATE VIEW view_grupo_menu  AS
-- select grupo_menu.id AS id,grupo_menu.nombre AS nombre from grupo_menu   where  eliminado = 0; 
-- 
-- 
-- DROP VIEW IF EXISTS view_persona;
-- CREATE VIEW view_persona  AS 
-- select p.id AS id,p.nombre AS nombre,p.paterno AS paterno,p.materno AS materno, p.ci AS ci,e.nombre AS expedido,p.estado AS estado 
-- from persona p join expedido e on p.id_expedido = e.id   where  p.eliminado = 0; ;
-- 
-- 
-- DROP VIEW IF EXISTS view_empresa;
-- CREATE VIEW view_empresa  AS 
-- select id AS id,nombre,red_social AS RedSocial,correo,telefono,direccion,logo,tema
-- from empresa  where eliminado = 0 ;
-- 
-- 
-- DROP VIEW IF EXISTS view_menu;
-- CREATE VIEW view_menu  AS
-- select id AS id,nombre,titulo,url,icos,estado  from menu   where  eliminado = 0; 
-- 
-- DROP VIEW IF EXISTS view_slider;
-- CREATE VIEW view_slider  AS
-- select id AS id,file  from slider  where  eliminado = 0; 
-- 
-- 
-- INSERT INTO `slider` (`id`, `file`, `eliminado`, `actualizado`, `creado`) VALUES
-- (1, 'UUNzRlE2K2pxVXpZUnIxN0UzMktXUT09', 0, '2019-09-05 03:38:19', '2019-09-05 03:38:19'),
-- (2, 'K0dTR1oxZ3FBUmFKNmdTT3k1MWEydz09', 0, '2019-09-05 03:38:24', '2019-09-05 03:38:24'),
-- (3, 'ZG55cDN2eXBNYmtranBEZCtYS0RWdz09', 0, '2019-09-05 03:38:29', '2019-09-05 03:38:29'),
-- (4, 'c0d1bXlhWnFIc1dmMEtTQmx5TkFXcTdMZ1laMUFJUGQzbXhsQUhqVGJNZz0_', 0, '2019-09-05 03:38:34', '2019-09-05 03:38:34'),
-- (5, 'QjNWK3B2WXNoYmVyRURtWHd0SDlrb1ExSmVWdUVDaXR1WC84dTBhdU82QT0_', 0, '2019-09-05 03:38:38', '2019-09-05 03:38:38'),
-- (6, 'cUdMa3VRQ0YrRkQ3TmlBVjhHeE03bHBjcmZvZHVrejZyK1NqYmRwT3BIWT0_', 0, '2019-09-05 03:38:42', '2019-09-05 03:38:42'),
-- (7, 'ZkJkVW4vdE11T2RsVTlhU1c2a3lIUT09', 0, '2019-09-05 03:38:53', '2019-09-05 03:38:53');
-- 
-- ---------------------------------------------------------------------------
-- 
-- 
-- 
-- DELIMITER $$
-- --
-- -- Procedimientos
-- --
-- DROP PROCEDURE IF EXISTS opciones$$
-- CREATE  PROCEDURE opciones (IN accion TEXT)  begin
--     case accion
--         when 'expedido' then
--             select id,nombre  from expedido where eliminado=0;
--         when 'rol' then
--             select id, nombre  from rol  where eliminado=0;
--         when 'persona' then
--             select id,concat(nombre,' ',paterno,' ',materno) as nombre  from persona  where eliminado=0;
--         when 'grupo_menu' then
--             select id,nombre  from grupo_menu where eliminado=0;  
--         when 'menu' then
--             select id,nombre  from menu where eliminado=0; 
--         when 'usuario' then
--             select u.id,concat(p.nombre,' ',p.paterno,' ',p.materno) as nombre  from usuario u inner join persona p on p.id=u.id_persona where u.eliminado=0;
--         when 'categoria' then
--             select id,nombre  from categoria where eliminado=0; 
--         when 'empresa' then
--             select id,nombre  from empresa where eliminado=0; 
--         when 'gestion' then
--             select id,gestion as gestion  from gestion where eliminado=0; 
--         when 'marca' then
--             select id,nombre from marca where eliminado=0; 
--         when 'vendedor' then
--             select u.id,concat(p.nombre,' ',p.paterno,' ',p.materno) as nombre  from usuario u inner join persona p on p.id=u.id_persona where u.eliminado=0;
--         when 'cliente' then
--             select id,nombre from cliente where eliminado=0; 
--         when 'proveedor' then
--             select id,nombre  from proveedor where eliminado=0 and estado=1; 
--         when 'producto' then
--             select id,nombre from producto where eliminado=0 and estado=1; 
--         when 'sucursal' then
--             select id,nombre from sucursal where eliminado=0 and estado=1; 
--         else
--              select 'salida' as salida;
--     end case;
--   end$$
-- 
-- DELIMITER ;