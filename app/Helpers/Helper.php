<?php

use App\Util;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

function createFolder($file) {
    if (!is_dir("./" . $file)) {
        mkdir("./" . $file, 0777);
    }
}

if (!function_exists('onlineUser')) {

    function onlineUser() {
        $is_format = [];
        if (!empty(Auth::user()->id_persona)) {
            session(['menus' => allMenu()]);
            session(['usuario' => DB::table('persona')->where('id', Auth::user()->id_persona)->first()]);
            session(['rol' => DB::table('rol')->where('id', Auth::user()->id_rol)->first()]);
            session(['empresa' => DB::table('empresa')->first()]);
            $permiso = DB::table('permiso')->selectRaw('permiso.id, menu.nombre, permiso.id_menu, permiso.id_rol, permiso.isdelete, permiso.isupdate, '
                                    . 'permiso.isnew, permiso.isreporte, permiso.isviewall, permiso.isview, permiso.estado')
                            ->join('menu', 'menu.id', '=', 'permiso.id_menu')
                            ->where('permiso.id_rol', Auth::user()->id_rol)->get();
            foreach ($permiso as $data) {
                $is_format[$data->nombre] = $data;
            }
            session(['permiso' => $is_format]);
        }
    }

}

function randomString($length, $type = false) {
    if ($type == false) {
        $type = 'default';
    }
    switch ($type) {
        case 'num':
            $salt = '1234567890';
            break;
        case 'lower':
            $salt = 'abcdefghijklmnopqrstuvwxyz';
            break;
        case 'upper':
            $salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 'default':
            $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            break;
    }
    return substr(str_shuffle($salt), 0, $length);
}

if (!function_exists('allMenu')) {

    function allMenu() {

        $grupo = DB::table('permiso')->selectRaw(" distinct grupo.id, grupo.nombre ")
                        ->join('grupo', 'grupo.id', '=', 'permiso.id_grupo')
                        ->join('menu', 'menu.id', '=', 'permiso.id_menu')
                        ->join('rol', 'rol.id', '=', 'permiso.id_rol')
                        ->where('rol.id', '=', Auth::user()->id_rol)
                        ->Where('permiso.estado', '1')->get();

        $menus = DB::table('permiso')->selectRaw(" permiso.id,menu.nombre as menu,rol.nombre as rol,menu.url,menu.icos,menu.titulo
                ,rol.id , grupo.id as id_grupo, grupo.nombre as grupo,permiso.estado")
                        ->join('grupo', 'grupo.id', '=', 'permiso.id_grupo')
                        ->join('menu', 'menu.id', '=', 'permiso.id_menu')
                        ->join('rol', 'rol.id', '=', 'permiso.id_rol')
                        ->where('rol.id', '=', Auth::user()->id_rol)
                        ->Where('permiso.estado', '1')->get();
        $menu = '';
        foreach ($grupo as $value) {
            $menu .= '<li class="treeview">';
            $menu .= '<a href="javascript:void(0)" class="app-menu__item" data-toggle="treeview">';
            $menu .= '<i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">' . ucwords($value->nombre) . '</span><i class="treeview-indicator fa fa-angle-right"></i>';
            $menu .= ' </a><ul class="treeview-menu">';
            foreach ($menus as $val) {
                if ($val->id_grupo == $value->id) {
                    $menu .= ' <li><a href="' . route($val->url) . '" class="treeview-item"  rel="' . strtolower($val->menu) . '"'
                            . ' data-table="' . $val->url . '" title="Seleccionar"> <i class="icon fa ' . $val->icos . '"></i>  '
                            . ucwords($val->titulo) . '</a></li>';
                }
            }
            $menu .= '</ul>';
            $menu .= ' </li>';
        }
        return $menu;
    }

}


if (!function_exists('button')) {

    function button($table, $boton) {
        $html = "";
        $formato = '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo $d"  $b > Nuevo $s</button>';
//      sprintf($formato, $num, $ubicación);
        foreach ($boton as $key => $value) {
            
        }
    }

}


if (!function_exists('parserInput')) {

    function parserInput($form) {
        foreach ($form as $key => $value) {
            if (empty($value)) {
                $form[$key] = '';
            }
        }
        return $form;
    }

}
if (!function_exists('forankeys')) {

    function forankeys($table, $id) {
        return (new Util())->forakeys($table, $id);
    }

}
if (!function_exists('options')) {

    function options($table) {

        return (new Util())->selected($table);
    }

}
if (!function_exists('_options')) {

    function _options() {

        return (new Util())->querys();
    }

}

if (!function_exists('field')) {

    function field($table) {
        return (new Util())->field($table);
    }

}
if (!function_exists('column')) {

    function column($table) {
        return (new Util())->column($table);
    }

}
if (!function_exists('concatRaw')) {

    function concatRaw($table, $true = false) {

        $is_select = '';
        foreach (field($table) as $value) {
            if (preg_match('/^id_/i', $value->name)) {
                $tl = preg_replace('/^\id_*/', '', $value->name);
                $subSel = '';
                $c = count(column($tl));
                $i = 1;
                foreach (column($tl) as $row) {
                    $subSel .= $tl . '.' . $row;
                    if ($c > 1 && $c != $i) {
                        $subSel .= '," ",';
                        $i++;
                    }
                }
                $is_select .= " concat($subSel)" . ($true ? " as text_" . $tl : '') . " ,";
            } else {
                $is_select .= $table . '.' . $value->name . ',';
            }
        }
        return trim($is_select, ',');
    }

}


if (!function_exists('paginator')) {

    function paginator($table, $input, $hidden = []) {

        $base = new \stdClass();
        $model = DB::table($table)->where($table . '.eliminado', '0');

        $limit = $input->input('length');
        $start = $input->input('start');

        foreach (field($table) as $value) {
            if (preg_match('/^id_/i', $value->name)) {
                $tl = preg_replace('/^\id_*/', '', $value->name);
                $model->join($tl, "$tl.id", '=', "$table.$value->name");
            }
        }

        $model->selectRaw(concatRaw($table, true));

        $totalFiltered = $model->count();

        $orderby = $input->input('columns')[$input->input('order.0.column')]['data'];

        $dir = $input->input('order.0.dir');

        if (empty($input->input('search.value'))) {
            $data = $model->offset($start)->limit($limit)->orderBy($orderby, $dir)->get();
        } else {
            $search = $input->input('search.value');
            $filtermulti = " CONCAT_WS(" . concatRaw($table) . ")";
            $data = $model->whereRaw($filtermulti . ' LIKE ?', ["%{$search}%"])->offset($start)->limit($limit)->orderBy($orderby, $dir)->get();
            $totalFiltered = $model->whereRaw($filtermulti . ' LIKE ?', ["%{$search}%"])->count();
        }
        $base->data = $data;
        $base->total = $model->count();
        $base->totalFilter = $totalFiltered;
        return $base;
    }

}