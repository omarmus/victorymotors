<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaDetalle extends Model
{
    protected $table = 'venta_detalle';
    public $columns;
    protected $fillable = ['codigo', 'nombre', 'modelo', 'descripcion', 'id_fabrica', 'costo', 'unitaria', 'precio_venta', 'stock_inicial', 'logo', 'estado'];

    public function __construct() {
        parent::__construct();
        $this->columns = $this->fillable;
    }
}
