<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabrica extends Model
{
   protected $table = 'fabrica';
    public $columns;
    protected $fillable = ['nombre', 'estado'];

    public function __construct() {
        parent::__construct();
        $this->columns = $this->fillable;
    }
}
