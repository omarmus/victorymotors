<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
     protected $table = 'compras';
    public $columns;
    protected $fillable = ['id_producto', 'id_proveedor', 'fecha', 'stock', 'id_usuario', 'precio',  'estado'];

    public function __construct() {
        parent::__construct();
        $this->columns = $this->fillable;
    }
}

