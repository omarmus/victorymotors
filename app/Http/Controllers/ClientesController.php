<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ClientesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.cliente');
    }

    public function create(Request $request, Cliente $model) {
        $reglas=[
            'nombre' => 'required|alpha'];
            $mensajes=['nombre.required.string' => 'es obligatorio'
            ];
            $validator = Validator::make( $request->all(),$reglas,$mensajes );
        $model->insert($request->input());
    }

    public function show() {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }

}
