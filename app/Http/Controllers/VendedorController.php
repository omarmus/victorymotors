<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VendedorController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('vendedor');
    }


}
