<?php

namespace App\Http\Controllers;

use App\Venta;
use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class VentaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        $data['cliente'] = Cliente::get();
        return view('module.venta', $data);
    }

    function lists(Request $request) {
        $table = 'venta';
        $format = paginator($table, $request, ['estado']);

        $permisos = session('permiso')[$table];
        $html = '';
        if (!empty($format->data)) {
            foreach ($format->data as $key => $row) {

                $row->tipo_pago = '<button type="button" class="badge badge-primary btn-xs">' . $row->tipo_pago . '</button>';

                $html .= '<div class="dropdown"><button id="editar-600" style="" class="badge badge-info"
                    type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i><span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="editar-600">';

                if ($permisos->isupdate == 1 && ($row->avance != 'cerrado')) {
                    $html .= '<li class="abc" onclick="venta.editar(this)" data-id="' . $row->id . '" data-route="' . route('venta.show') . '"><a href="#">Editar</a></li>';
                } else {
                    $html .= '<li class="abc" onclick="venta.informacion(this)" data-id="' . $row->id . '" data-route="' . route('venta.show') . '"><a href="#">Informacion</a></li>';
                }

                $html .= '<li class = "abc"  data-title="Reporte Nota de Venta"  onclick = "' . $table . '.imprimir(this)" data-id = "' . $row->id . '" data-route = "show/' . $table . '"><a href = "#">Nota de venta</a></li >';
                if ($row->avance != 'cerrado') {
                    $html .= '<li class = "abc"  data-title="Aprobar la venta"  onclick = "' . $table . '.aprobar(this)" data-id = "' . $row->id . '" data-route = "show/' . $table . '"><a href = "#">Aprobar la venta</a></li >';
                } else {

                }
                $html .= '</ul></div>';
                $row->action = $html;
                $row->avance = '<button type="button" class="badge badge-' . ($row->avance == 'cerrado' ? 'success' : 'info') . ' btn-xs">' . $row->avance . '</button>';

                $html = '';
            }
        }

        return response()->json(["draw" => intval($request->input('draw')), "recordsTotal" => intval($format->total),
                    "recordsFiltered" => intval($format->totalFilter), "data" => $format->data]);
    }

    public function getProducto(Request $request) {
        $model = DB::table('inventario')->selectRaw("inventario.id,inventario.precio,inventario.stock,producto.id as id_producto,producto.nombre,"
                                . "producto.codigo,producto.id_fabrica")
                        ->join('producto', 'producto.id', '=', 'inventario.id_producto')
                        ->where('producto.nombre', 'like', '%' . $request->value . '%')
                        ->orWhere('producto.codigo', 'like', '%' . $request->value . '%')->get();

        return response()->json(["estado" => true, "data" => $model]);
    }

    public function create(Request $request) {

        if (empty($request->id)) {
            $venta = new Venta;
            $venta->fecha = $request['fecha'];
            $venta->codigo = $request['codigo'];
            $venta->id_cliente = $request['cliente'];
            $venta->avance = $request['avance'];
            $venta->tipo_pago = $request['tipo_pago'];
            $venta->nro_cuotas = $request['nro_cuotas'];
            $venta->cuotas = $request['cuotas'];
            $venta->id_users = Auth::user()->id;
            $venta->total = !empty($request['total']) ? $request['total'] : '0';
            $venta->save();
            $LastInsertId = $venta->id;
            if ($LastInsertId) {
                if (!empty($request['producto'])) {
                    for ($i = 0; $i < count($request['producto']); $i++) {
                        $gData = [
                            'id_producto' => $request['producto'][$i],
                            'id_venta' => $LastInsertId,
                            'cantidad' => $request['cantidad'][$i],
                            'precio_uni' => $request['precio_uni'][$i],
                        ];
                        \App\VentaDetalle::insert($gData);
                    }
                }
                return response()->json(["estado" => true, "data" => "Se Registro satisfactoriamente...!"]);
            } else {
                return response()->json(["estado" => false, "data" => "error"]);
            }
        } else {

            $venta = new Venta;
            $venta::where('id', $request['id'])->update([
                'fecha' => $request['fecha'],
                'id_cliente' => $request['cliente'],
                'avance' => $request['avance'],
                'tipo_pago' => $request['tipo_pago'],
                'total' => $request['total'],
                'nro_cuotas' => $request['nro_cuotas'],
                'cuotas' => $request['cuotas'],
            ]);
            if (!empty($request['producto'])) {
                for ($i = 0; $i < count($request['producto']); $i++) {
                    if (!empty($request['dell_id'][$i])) {
                        $gData = [
                            'id_producto' => $request['producto'][$i],
                            'cantidad' => $request['cantidad'][$i],
                            'precio_uni' => $request['precio_uni'][$i],
                            'descuento' => $request['descuento'][$i],
                        ];

                        \App\VentaDetalle::where('id', $request['dell_id'][$i])->update($gData);
                    } elseif (empty($request['dell_id'][$i])) {
                        $gData = [
                            'id_producto' => $request['producto'][$i],
                            'id_venta' => $request->id,
                            'cantidad' => $request['cantidad'][$i],
                            'precio_uni' => $request['precio_uni'][$i],
                            'descuento' => $request['descuento'][$i],
                        ];
                        \App\VentaDetalle::insert($gData);
                    }
                }
                return response()->json(["estado" => true, "data" => "Se Registro satisfactoriamente...!"]);
            } else {
                return response()->json(["estado" => false, "data" => "error"]);
            }
        }
    }

    public function show(Request $request) {

        $data = new \stdClass();
        $data->venta = DB::table('venta')
                        ->select("venta.id", "venta.codigo", "venta.fecha", "venta.total", "cliente.nombre", "venta.id_cliente", 'venta.avance', 'venta.tipo_pago', 'venta.nro_cuotas', 'venta.cuotas')
                        ->join('cliente', 'cliente.id', '=', 'venta.id_cliente')
                        ->where('venta.id', $request->id)->first();

        $detalle = DB::table('venta')
                        ->select("venta_detalle.id", "producto.nombre as producto", "venta_detalle.descuento", "producto.id as id_producto", "producto.codigo", "venta_detalle.cantidad", "venta_detalle.precio_uni")
                        ->join('venta_detalle', 'venta_detalle.id_venta', '=', 'venta.id')
                        ->join('producto', 'producto.id', '=', 'venta_detalle.id_producto')
                        ->where('venta.id', $request->id)->get();

        $data->detalle = $detalle;
        return response()->json(["estado" => true, "data" => $data]);
    }

    public function aprobar(Request $request) {

        $venta = DB::table('venta')->where('id', $request->id)->first();
        if ($venta->avance != 'cerrado') {
            $dell = DB::table('venta_detalle')->where('id_venta', $request->id)->get();
            foreach ($dell as $value) {
                $inv = DB::table('inventario')->where('id_producto', $value->id_producto)->first();
                DB::table('inventario')->where('id_producto', $value->id_producto)->update(['stock' => ( $inv->stock - $value->cantidad)]);
            }
            DB::table('venta')->where('id', $request->id)->update(['avance' => 'cerrado']);
        }
        return response()->json(["estado" => true, "mensaje" => "Se Aprobo satisfactoriamente...!"]);
    }

    public function destroy(Request $request) {
        return DB::table('venta_detalle')->where('id', $request->id)->delete();
    }

}
