<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use File;

class DatasourceController extends Controller {

    protected $hidden = ['eliminado', 'updated_at', 'created_at'];
    protected $tipos = [
        'UPDATE' => 'ACTUALIZACIÓN DE REGISTRO',
        'INSERT' => 'NUEVO REGISTRO',
        'DELETE' => 'ELIMINACIÓN DEL REGISTRO'
    ];
    protected $tiposB = [
        'UPDATE' =>'ACTUALIZADO',
        'INSERT' =>'REGISTRADO',
        'DELETE' =>'ELIMINADO'
    ];

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.cliente');
    }

    /**
     *
     * @param Request $request
     * paginato(model,request,hidden)
     * @return type
     */
    function table(Request $request, $table) {

        $format = paginator($table, $request, ['estado']);



        $permisos = session('permiso')[$table];
        $html = '';
        if (!empty($format->data)) {
            foreach ($format->data as $key => $row) {

                if (!empty($row->estado)) {
                    $row->estado = '<button type="button" class="badge badge-' . ($row->estado == '0' ? 'danger' : 'success') . ' btn-xs">' . ($row->estado == '0' ? 'Desactivo' : 'Activo') . '</button>';
                }
                if (!empty($row->logo)) {
                    $row->logo = '<img style="width: 38px;" id="imagenes" src="'.asset('files/'.$row->logo).'">';
                }

                $html .= '<div class="dropdown"><button id="editar-600" style="" class="badge badge-info"
                    type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i><span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="editar-600">';
                if ($permisos->isupdate == 1) {
                    $html .= '<li class="abc" onclick="' . $table . '.editar(this)" data-title="Editar ' . $table . '"  data-id="' . $row->id . '" data-route="show/' . $table . '"><a href="#">Editar</a></li>';
                }
                if ($permisos->isdelete == 1) {
                    $html .= '<li class = "abc" onclick = "' . $table . '.destroy(this)" data-id = "' . $row->id . '" data-route = "destroy/' . $table . '"><a href = "#">Eliminar</a></li >';
                }
                if ($table == 'users') {
                    $html .= '<li class = "abc"  data-title="Cambiar contraseña"  onclick = "' . $table . '.reset(this)" data-id = "' . $row->id . '" data-route = "show/' . $table . '"><a href = "#">Cambiar Contraseña</a></li >';
                }

                $html .= '</ul></div>';
                $row->action = $html;

                if ($table == 'log') {
                    $cambios = '';
                    if ($row->tipo == 'UPDATE') {
                        $cambios = 'El registro con <strong>' . $this->getID($row->old) . '</strong> fue ' . $this->tiposB[$row->tipo] . '. <br> <em>Valores modificados:</em>' . ($row->valor_alterado ? $row->valor_alterado : 'NINGUNO');
                    }
                    if ($row->tipo == 'DELETE') {
                        $cambios = 'El registro con <strong>' . $this->getID($row->old) . '</strong> fue ' . $this->tiposB[$row->tipo] . '.';
                    }
                    if ($row->tipo == 'INSERT') {
                        $cambios = 'Se agregó un nuevo registro con los siguientes datos: <br>' . str_replace(';id=0', '', $row->new);
                    }
                    $row->cambios = $cambios;
                    $row->tipo = $this->tipos[$row->tipo];
                    $row->fecha = date('d/m/Y H:i', strtotime($row->fecha));
                    $row->tabla = 'En la sección <strong>' . $row->tabla . '</strong>';
                }

                $html = '';
            }
        }

        return response()->json(["draw" => intval($request->input('draw')), "recordsTotal" => intval($format->total),
                    "recordsFiltered" => intval($format->totalFilter), "data" => $format->data]);
    }

    private function getID ($data) {
        if (count(explode(';', $data)) > 1) {
            $data = explode(';', $data)[1];
            if (strpos($data, 'id=') !== false) {
              return strtoupper($data);
            } else {
              return '';
            }
        }
        return '';
    }

    public function create(Request $request, $table) {
        $param = $request->input();
        if (count($request->file()) > 0) {
            $param['logo'] = $this->SubirArchivo();
        }

        unset($param['id']);
        if (empty($request->id)) {
            $true = DB::table($table)->insert(parserInput($param));
        } else {
            DB::table($table)->where('id', $request->id)->update(parserInput($param));
            $true = true;
        }
        return response()->json(["estado" => ($true ? true : false),
                    "mensaje" => ($true ? (empty($request->id) ? 'Almaceno' : 'Actualizo') . '  Corrrectamente...' : 'Ocurrio un error interna')]);
    }

    public function show(Request $request, $table) {

        $data = DB::table($table)->where('id', $request->id)->first();
        return response()->json(["data" => $data, "estado" => (!empty($data) ? true : false), "mensaje" => (!empty($data) ? 'Consulta Satisfactoria' : 'Ocurrio un error interna')]);
    }

    public function destroy(Request $request, $table) {
        $true = DB::table($table)->where('id', $request->id)->update(['eliminado' => '1']);
        return response()->json(["estado" => ($true == 1 ? true : false), "mensaje" => ($true == 1 ? 'Se Elimino Corrrectamente...' : 'Ocurrio un error interna')]);
    }

    public function SubirArchivo() {
        $file_name = public_path() . '/files/';
        if (!empty($_FILES)) {
            $file = $_FILES['files0']['name'];
            if (!file_exists($file_name))
                File::makeDirectory($file_name, 0777, true);
            $type = explode('/', $_FILES['files0']['type']);
            $extesion = ['png', 'mp4', 'jpg', 'jpeg'];
            foreach ($extesion as $value) {
                if ($value == $type[1]) {
                    $namfile = time() . $file;
                    if ($namfile && move_uploaded_file($_FILES['files0']['tmp_name'], $file_name . $namfile)) {
                        sleep(2); //retrasamos la petición 3 segundos
                        return $namfile;
//                        response(['url' => base_url('files/' . $namfile), 'type' => $type[0]]);
                    }
                }
            }
        }
    }

}
