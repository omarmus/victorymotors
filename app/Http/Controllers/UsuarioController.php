<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Hash;

class UsuarioController extends Controller {

     public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.usuario');
    }

    public function create(Request $request) {
        $param = $request->input();
        unset($param['id']);
        if (empty($request->id)) {
            $param['password'] = Hash::make($param['password']);
            $true = DB::table('users')->insert(parserInput($param));
        } else {
            DB::table('users')->where('id', $request->id)->update(parserInput($param));
            $true = true;
        }
        return response()->json(["estado" => ($true ? true : false),
                    "mensaje" => ($true ? (empty($request->id) ? 'Almaceno' : 'Actualizo') . '  Corrrectamente...' : 'Ocurrio un error interna')]);
    }

    public function pass(Request $request) {
        $param = $request->input();
        $param['password'] = Hash::make($param['reset_password']);
        unset($param['id']);
        unset($param['reset_password']);
        DB::table('users')->where('id', $request->id)->update(parserInput($param));
        return response()->json(["estado" => true, "mensaje" => 'Almaceno  Corrrectamente..']);
    }

    public function update(Request $request, $id) {
        //
    }

}
