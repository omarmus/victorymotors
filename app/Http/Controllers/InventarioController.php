<?php

namespace App\Http\Controllers;

use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventarioController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.stock');
    }

    public function create(Request $request) {
        $true = false;
        $param = $request->input();
        unset($param['id']);
        $data = DB::table('almacen')->where('id_producto', $request->id_producto)->first();
        if ($data->stock <= $request->stock) {
            return response()->json(["estado" => FALSE, "mensaje" => 'Error Cantidad no debe superar al de almcen!']);
        }
        if (empty($request->id) && empty(DB::table('inventario')->where('id_producto', $request->id_producto)->first())) {
            $true = DB::table('inventario')->insert(parserInput($param));
            $mes = 'Se almaceno satisfactoriamente';
        } else if (!empty($request->id) && !empty(DB::table('inventario')->where('id_producto', $request->id_producto)->first())) {
            if (!empty($request->aumentar)) {
                $true = true;
                $param['stock'] = intval($param['stock'] + $param['aumentar']);
                unset($param['aumentar']);
                DB::table('inventario')->where('id_producto', $request->id_producto)->update(parserInput($param));
                $mes = 'Se Aumento Satifactoriamente';
            } else {
                $mes = 'No se aumento el stock';
                $true = false;
            }
        }
        if ($true) {
            DB::table('almacen')->where('id_producto', $request->id_producto)->update(['stock' => intval($data->stock - $request->cantidad)]);
        }

        return response()->json(["estado" => ($true ? true : false), "mensaje" => $mes]);
    }

  

}
