<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Closure;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $data['vendida'] = DB::table('venta')->selectRaw('sum(venta.total) as total,count(venta.id) as cantidad')->where('venta.avance', 'cerrado')->first();

        $data['pendiente'] = DB::table('venta')->selectRaw('sum(venta.total) as total,count(venta.id) as cantidad')->where('venta.avance', 'pendiente')->first();

        $data['stock'] = DB::table('inventario')->selectRaw('sum(inventario.stock) as total,count(inventario.id) as cantidad')->first();

        $data['cliente'] = DB::table('cliente')->selectRaw('count(id) as cantidad')->first();


        $data['line'] = DB::table('venta')
                        ->select(DB::raw('SUM(venta.total) as total,MONTH(venta.fecha) AS mes'))
                        ->where('venta.avance', 'cerrado')->groupBy('mes')
                        ->orderBy('mes', 'asc')->get();

        $data['historial'] = DB::table('venta')
                        ->select("cliente.nombre","venta.total", "venta.fecha", "venta_detalle.id", "producto.nombre as producto", "producto.id as id_producto", "producto.codigo", "venta_detalle.cantidad", "venta_detalle.precio_uni")
                        ->join('venta_detalle', 'venta_detalle.id_venta', '=', 'venta.id')
                        ->join('cliente', 'cliente.id', '=', 'venta.id_cliente')
                        ->join('producto', 'producto.id', '=', 'venta_detalle.id_producto')->where('venta.avance', 'cerrado')->limit(5)->orderBy('venta.fecha', 'desc')->get();
//dump($data);
//exit();
        return view('module.entrada', $data);
    }

    public function login() {
        return redirect('/home');
    }

}
