<?php

namespace App\Http\Controllers;

use App\Grupo;
use Illuminate\Http\Request;

class GrupoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.grupo');
    }

}
