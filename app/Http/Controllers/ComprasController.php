<?php

namespace App\Http\Controllers;

use App\Compras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ComprasController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.compras');
    }

    function lists(Request $request) {

        $format = paginator('compras', $request, ['estado']);

        if (!empty($format->data)) {
            foreach ($format->data as $key => $row) {
                $row->action = '<div class="dropdown"><button id="editar-600" style="" class="btn btn btn-primary btn-xs"  
                    type="button"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-sun"></i><span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="editar-600">
                    <li style="padding: 1px 11px; border-bottom: 1px solid #989898;"><a href="#">Editar</a></li>
                    <li style="padding: 1px 11px;"><a href="#">Eliminar</a></li>
                </ul></div>';
            }
        }

        return response()->json(["draw" => intval($request->input('draw')), "recordsTotal" => intval($format->total),
                    "recordsFiltered" => intval($format->totalFilter), "data" => $format->data]);
    }

    public function create(Request $request) {
        $true = false;
        $param = $request->input();
        unset($param['id']);

        $data = DB::table('almacen')->where('id_producto', $request->id_producto)->first();

        if (empty($request->id)) {
            DB::table('compras')->insert(parserInput($param));
            $mes = 'Se almaceno satisfactoriamente...!';
            $true = true;
        }
        if ($true) {
            if (empty($data->stock)) {
                DB::table('almacen')->insert(['stock' => $request->stock, 'id_producto' => $request->id_producto, 'fecha' => date("Y-m-d"), 'id_users' => Auth::user()->id]);
            } else {
                DB::table('almacen')->where('id_producto', $request->id_producto)->update(['stock' => intval($data->stock + $request->stock)]);
            }
        }

        return response()->json(["estado" => ($true ? true : false), "mensaje" => $mes]);
    }

}
