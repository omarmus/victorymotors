<?php

namespace App\Http\Controllers;

use App\Fabrica;
use Illuminate\Http\Request;

class FabricaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.fabrica');
    }

    function lists(Request $request) {

        $format = paginator(new Fabrica(), $request, ['estado']);

        if (!empty($format->data)) {
            foreach ($format->data as $key => $row) {
                $row['id'] = $key + 1;
                $row->estado = '<button type="button" class="btn btn-block bg-gradient-' . ($row->estado == '0' ? 'danger' : 'success') . ' btn-xs">' . ($row->estado == '0' ? 'Desactivo' : 'Activo') . '</button>';
                $row['action'] = '<div class="dropdown"><button id="editar-600" style="" class="btn btn btn-primary btn-xs"  
                    type="button"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-sun"></i><span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="editar-600">
                    <li style="padding: 1px 11px; border-bottom: 1px solid #989898;"><a href="#">Editar</a></li>
                    <li style="padding: 1px 11px;"><a href="#">Eliminar</a></li>
                </ul></div>';
            }
        }

        return response()->json(["draw" => intval($request->input('draw')), "recordsTotal" => intval($format->total),
                    "recordsFiltered" => intval($format->totalFilter), "data" => $format->data]);
    }

    public function create(Request $request) {
        //
    }

    public function show($id) {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }

}
