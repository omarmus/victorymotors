<?php

namespace App\Http\Controllers;

use App\Permiso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermisoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
        return view('module.permiso');
    }

    public function setMenu(Request $request) {

        $id = $request->roles;
        $inArray = Array();
        $in = DB::table('permiso')->selectRaw('menu.nombre as menu')
                        ->join('grupo', 'grupo.id', '=', 'permiso.id_grupo')
                        ->join('menu', 'menu.id', '=', 'permiso.id_menu')
                        ->join('rol', 'rol.id', '=', 'permiso.id_rol')
                        ->where('rol.id', '=', $id)->where('permiso.estado', '=', '1')->get()->toArray();
        foreach ($in as $value) {
            array_push($inArray, $value->menu);
        }
        $data = DB::table('menu')->whereNotIn('nombre', $inArray)->get();

        $html = '';
        foreach ($data as $val) {
            $html .= "<tr>";
            $html .= "<td style='width: 15mm'><input type='radio' name='menu' id='menu' value='$val->id' ></td>";
            $html .= "<td style=width: 50mm'>$val->nombre</td>";
            $html .= "</tr>";
        }
        echo $html;
//         return response()->json(["estado" => true, "data" => $model]);
    }

    public function removerPermiso(Request $request) {
        $id = $request->permiso;
        DB::table('permiso')->delete(['id' => $id]);
        session(['menus' => allMenu()]);
        echo 'eliminado';
    }

    public function habil(Request $request) {
        $id = $request->roles;

        $data = DB::table('permiso')->selectRaw('permiso.id,menu.nombre as menu,rol.nombre as rol, grupo.id as id_grupo, grupo.nombre as grupo,permiso.estado,permiso.id_menu,permiso.isnew,permiso.isupdate,permiso.isdelete,permiso.isreporte
                ,permiso.isview,permiso.isviewall')
                        ->join('grupo', 'grupo.id', '=', 'permiso.id_grupo')
                        ->join('menu', 'menu.id', '=', 'permiso.id_menu')
                        ->join('rol', 'rol.id', '=', 'permiso.id_rol')
                        ->where('rol.id', '=', $id)->where('permiso.estado', '=', '1')->get();

        $grupo = options("grupo");
        $menu = options("menu");

        $html = '';
        $i = 1;
        $color = ['btn-danger', 'btn-success'];
        foreach ($data as $val) {
            $html .= "<tr>";
            $html .= "<td><input type='radio' name='permiso' id='permiso' value='$val->id' >$i</td>";
            $html .= "<td><input type='hidden' name='id[]' id='id' value='$val->id' >"
                    . "<select  name='grupo[]' class='form-control'><option>Selecione grupo..</option>";
            foreach ($grupo as $value) {
                $html .= '<option value="' . $value->id . '"  ' . ($val->id_grupo == $value->id ? "selected" : "") . '>'
                        . $value->text_grupo . '</option>';
            }
            $html .= "</select></td>";
            $html .= "<td >$val->menu </td>";
            $html .= '<td>' . $this->estadoHtml('new[]', $val->isnew) . '</td>';
            $html .= '<td>' . $this->estadoHtml('update[]', $val->isupdate) . '</td>';
//            $html .= '<td>' . $this->estadoHtml('view[]', $val->isview) . '</td>';
//            $html .= '<td>' . $this->estadoHtml('viewall[]', $val->isviewall) . '</td>';
            $html .= '<td>' . $this->estadoHtml('delete[]', $val->isdelete) . '</td>';
            $html .= '<td>' . $this->estadoHtml('estado[]', $val->estado) . '</td>';
//            $html .= '<td>' . $this->estadoHtml('reporte[]', $val->isreporte) . '</td>';

            $html .= "</tr>";
            $i++;
        }
        echo $html;
    }

    public function estadoHtml($name, $status) {
        $id = randomString(10);
        return '<span class="checkbox">
                        <input  data-id="' . $id . '" onclick="permisos(this)"    type="checkbox" ' . ($status == 1 ? 'checked="checked"' : '') . ' >
                        <label  data-on="Si" data-off="No"></label>
                        <input type="hidden" value="' . $status . '" id="' . $id . '"  name="' . $name . '" >
                    </span>';
    }

    public function Asigna(Request $request) {
        $id = $request->roles;
        $grupo = $request->grupo;
        $menu = $request->menu;

        $data = DB::table('permiso')->insert(array(
            'id_rol' => $id,
            'id_grupo' => $grupo,
            'id_menu' => $menu
        ));

        if ($data) {
            session(['menus' => allMenu()]);
            return response()->json(["estado" => true, "mensaje" => 'Se asgino satisfactoriamente...!']);
        } else {
            return response()->json(["estado" => true, "mensaje" => 'Error de asignacion...']);
        }
    }

    public function cambio(Request $request) {
//        dump($request->'grupo'));
        if (!empty($request->id)) {
            for ($i = 0; $i < count($request->id); $i++) {
                $id = intval($request->id[$i]);
                $grupo = intval($request->grupo[$i]);
//                $reporte = !empty($request->reporte[$i]) ? ($request->reporte[$i]) : ('0');
                $new = !empty($request->new[$i]) ? ($request->new[$i]) : ('0');
                $update = !empty($request->update[$i]) ? ($request->update[$i]) : ('0');
//                $view = !empty($request->view[$i]) ? ($request->view[$i]) : ('0');
//                $viewall = !empty($request->viewall[$i]) ? ($request->viewall[$i]) : ('0');
                $delete = !empty($request->delete[$i]) ? ($request->delete[$i]) : ('0');
                $estado = !empty($request->estado[$i]) ? ($request->estado[$i]) : ('0');

                DB::table('permiso')->where('id', $id)->update([
                    'id_grupo' => $grupo,
                    'isupdate' => $update,
                    'estado' => $estado,
                    'isdelete' => $delete,
                    'isnew' => $new,
                ]);
            }
//            $menu['permisos'] = $this->util->isPermiso($this->session->userdata('id_rol'));
            session(['menus' => allMenu()]);
            return response()->json(["estado" => true, "mensaje" => 'Se asgino satisfactoriamente...!']);
        } else {
            return response()->json(["estado" => true, "mensaje" => 'Error de asignacion...']);
        }
    }

}
