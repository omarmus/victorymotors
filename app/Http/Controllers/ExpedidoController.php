<?php

namespace App\Http\Controllers;

use App\Expedido;
use Illuminate\Http\Request;

class ExpedidoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

}
