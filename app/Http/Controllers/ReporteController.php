<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\DB;

class ReporteController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index() {
//         $data['title'] = "Reporte generales";
//          return view('reporte.reporte',$data);
        return view('module.reporte');
    }

    public function all(Request $request) {
        $data = $request->input();

        if ($data['radioName'] == 'stock') {
            return $this->inventario();
        }
        if ($data['radioName'] == 'compras') {

            return $this->Compras();
        }
        if ($data['radioName'] == 'almacen') {
            return $this->Almacen();
        }
        if ($data['radioName'] == 'venta') {
            return $this->notaAll($request);
        }
    }

    public function notaAll(Request $request) {

        $venta = DB::table('venta')
                        ->select("venta.id", "venta.codigo", "venta.fecha", "venta.total", "cliente.nombre", "venta.id_cliente", 'venta.avance', 'venta.tipo_pago')
                        ->join('cliente', 'cliente.id', '=', 'venta.id_cliente')
                        ->whereBetween('venta.fecha', [$request->desde, $request->hasta])->get();

        foreach ($venta as $key => $value) {
            $venta[$key]->detalle = DB::table('venta')
                            ->select("venta_detalle.id","cliente.nombre", "producto.nombre as producto", "producto.id as id_producto", "producto.codigo", "venta_detalle.cantidad", "venta_detalle.precio_uni")
                            ->join('venta_detalle', 'venta_detalle.id_venta', '=', 'venta.id')
                            ->join('producto', 'producto.id', '=', 'venta_detalle.id_producto')
                            ->join('cliente', 'cliente.id', '=', 'venta.id_cliente')
                            ->where('venta.id', $value->id)->whereBetween('venta.fecha', [$request->desde, $request->hasta])->get();
        }

        $pdf = PDF::loadView('reporte.ventaAlll', ['venta' => $venta]);
        return $pdf->stream('notaVenta.pdf');
    }

    public function nota(Request $request, $id) {

        $venta = DB::table('venta')
                        ->select("venta.id", "venta.codigo", "venta.fecha", "venta.total", "cliente.nombre", "venta.id_cliente", 'venta.avance', 'venta.tipo_pago', 'venta.nro_cuotas', 'venta.cuotas')
                        ->join('cliente', 'cliente.id', '=', 'venta.id_cliente')
                        ->where('venta.id', $id)->first();

        $detalle = DB::table('venta')
                        ->select("venta_detalle.id", "producto.nombre as producto", "producto.id as id_producto", "producto.codigo", "venta_detalle.cantidad", "venta_detalle.precio_uni")
                        ->join('venta_detalle', 'venta_detalle.id_venta', '=', 'venta.id')
                        ->join('producto', 'producto.id', '=', 'venta_detalle.id_producto')
                        ->where('venta.id', $id)->get();

        $pdf = PDF::loadView('reporte.notaVenta', ['venta' => $venta, 'detalle' => $detalle]);
        return $pdf->stream('notaVenta.pdf');
//        return $pdf->download('itsolutionstuff.pdf');
    }

    public function inventario() {
        $model = DB::table('inventario')->selectRaw("inventario.id,inventario.precio,inventario.stock,producto.id as id_producto,producto.nombre as producto,"
                                . "producto.codigo,producto.id_fabrica")
                        ->join('producto', 'producto.id', '=', 'inventario.id_producto')->get();

        $pdf = PDF::loadView('reporte.printInventario', ['data' => $model]);
        return $pdf->stream('printInvetario.pdf');
    }
    public function Compras() {
        $model = DB::table('compras')->selectRaw("compras.id,compras.precio,compras.fecha,compras.stock,producto.id as id_producto,producto.nombre as producto,"
                                . "producto.codigo,producto.id_fabrica")
                        ->join('producto', 'producto.id', '=', 'compras.id_producto')->get();

        $pdf = PDF::loadView('reporte.printCompras', ['data' => $model]);
        return $pdf->stream('printCompras.pdf');
    }
    public function Almacen() {
        $model = DB::table('almacen')->selectRaw("almacen.id,almacen.fecha,almacen.stock,producto.id as id_producto,producto.nombre as producto,"
                                . "producto.codigo,producto.id_fabrica")
                        ->join('producto', 'producto.id', '=', 'almacen.id_producto')->get();

        $pdf = PDF::loadView('reporte.printAlmacen', ['data' => $model]);
        return $pdf->stream('printAlmacen.pdf');
    }
      public function Producto() {
        $model = DB::table('producto')->selectRaw("producto.id,producto.codigo,producto.nombre,producto.modelo,producto.chasis,producto.motor,producto.descripcion,producto.id_fabrica,producto.costo,producto.logo,producto.estado")
        ->get();

        $pdf = PDF::loadView('reporte.printProductos', ['data' => $model]);
        return $pdf->stream('printProductos.pdf');
    }
      public function Cliente() {
        $model = DB::table('cliente')->selectRaw("cliente.id,cliente.nombre,cliente.telefono,cliente.correo,cliente.direccion")
        ->get();

        $pdf = PDF::loadView('reporte.printClientes', ['data' => $model]);
        return $pdf->stream('printClientes.pdf');
    }


}
