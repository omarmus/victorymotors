<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isVendedor {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $requestUserId = $request->route()->parameter('id');
        if (Auth::user()->id_rol === '2' || Auth::user()->id == $requestUserId) {
            return $next($request);
        } else {
            return response()->json(['error' => 'no tiene permiso / no es vendedor']);
        }
    }

}
