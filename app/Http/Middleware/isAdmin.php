<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class isAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // return $next($request);
//        $requestUserId = $request->route()->parameter('id');
        onlineUser();
//        if (Auth::user()->id_rol === '1' || Auth::user()->id == $requestUserId) {
        return $next($request);
//        } else {
//            return response()->json(['error' => 'no tiene permiso / no es admin']);
//        }
    }

}
