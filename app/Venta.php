<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = 'venta';
    public $columns;
    protected $fillable = ['nombre', 'estado'];
    protected $casts = [
        'cuotas' => 'array'
    ];

    public function __construct() {
        parent::__construct();
        $this->columns = $this->fillable;
    }
}
