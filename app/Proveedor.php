<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
     protected $table = 'proveedor';
    public $columns;
    protected $fillable = ['nombre', 'sitio_web', 'rl_nombre', 'rl_apellidos',  'rl_corrreo',  'rl_telefono',  'direccion',  'estado'];

    public function __construct() {
        parent::__construct();
        $this->columns = $this->fillable;
    }
}
