<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Util extends Model {

    protected $select = [
        'users' => ['usuario'],
        'proveedor' => ['nombre', 'rl_nombre'],
        'persona' => ['nombre'],
        'producto' => ['nombre'],
        'fabrica' => ['nombre'],
        'venta_detalle' => ['id'],
        'cliente' => ['nombre'],
        'grupo' => ['nombre'],
        'expedido' => ['nombre'],
        'menu' => ['nombre'],
        'rol' => ['nombre'],
    ];

    public function selected($table) {

        $is_select = '';
        foreach ($this->select[$table] as $value) {
            $is_select .= $value . " as text_" . $table . " ,";
        }

        return DB::table($table)->selectRaw('id,' . trim($is_select, ','))->get();
    }

    public function forakeys($table, $id) {
        return DB::table($table)->select('id', DB::raw('concat(' . $this->select[$table] . ') as ' . $table))->where('id', $id)->first();
    }

    public function column($table) {
        if (empty($this->select[$table])) {
            return false;
        } else {
            return $this->select[$table];
        }
    }

    public function querys() {
        $data = new \stdClass();

        $data->almacen = DB::table('almacen')->selectRaw('producto.id,producto.nombre as text_producto')->join('producto', 'producto.id', '=', 'almacen.id_producto')->get();
        $data->producto = DB::table('producto')->selectRaw('nombre as text_producto,id')->get();

        return $data;
    }

    public function field($table) {
        return DB::select(DB::raw("SELECT COLUMN_NAME as name,DATA_TYPE as type,IS_NULLABLE as isnull,CHARACTER_MAXIMUM_LENGTH as max,COLUMN_TYPE as col_type FROM information_schema.columns
                            WHERE table_schema = DATABASE()
                            and TABLE_NAME ='$table'
                            ORDER BY table_name, ordinal_position;"));
    }

}
