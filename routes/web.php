<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
  Route::get('/', function () {
    return view('index');
});

// Route::get('/', function () {
//     return view('auth.login');
// //Route::get('/', 'HomeController@login');
// });
// Auth::routes();
// //Route::get('/index', function () {
// //    return view('layouts.login');
// Route::get('/index', 'HomeController@login');
// //});

// Route::get('/main', function () {
//     return view('main');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/home', 'HomeController@index');
Route::get('/vendedor', 'VendedorController@index')->name('home');


Route::post('/datatable/{table}', 'DatasourceController@table');
Route::post('/create/{table}', 'DatasourceController@create');
Route::post('/show/{table}', 'DatasourceController@show');
Route::post('/destroy/{table}', 'DatasourceController@destroy');


Route::get('/reporte/{id}', 'ReporteController@nota');
Route::get('/printInventario', 'ReporteController@inventario');
Route::get('/printCompras', 'ReporteController@Compras');
Route::get('/printAlmacen', 'ReporteController@Almacen');
Route::get('/printProductos', 'ReporteController@Producto');
Route::get('/printClientes', 'ReporteController@Cliente');
Route::get('/printAll', 'ReporteController@all');

Route::group(['prefix' => 'reporte'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ReporteController@index',
            'as' => 'reporte'
        ]);

        Route::match(['get', 'post'], '/all', [
            'uses' => 'ReporteController@all',
            'as' => 'reporte.all'
        ]);
    });
});

Route::group(['prefix' => 'menu'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'MenuController@index',
            'as' => 'menu'
        ]);
    });
});
Route::group(['prefix' => 'users'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'UsuarioController@index',
            'as' => 'users'
        ]);
        Route::match(['get', 'post'], '/create', [
            'uses' => 'UsuarioController@create',
            'as' => 'users.create'
        ]);
        Route::match(['get', 'post'], '/update', [
            'uses' => 'UsuarioController@update',
            'as' => 'users.update'
        ]);
        Route::match(['get', 'post'], '/pass', [
            'uses' => 'UsuarioController@pass',
            'as' => 'users.pass'
        ]);
        Route::match(['get', 'post'], '/destroy', [
            'uses' => 'UsuarioController@pass',
            'as' => 'users.destroy'
        ]);
    });
});

Route::group(['prefix' => 'grupo'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'GrupoController@index',
            'as' => 'grupo'
        ]);
    });
});
Route::group(['prefix' => 'perfil'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'PerfilController@index',
            'as' => 'perfil'
        ]);
    });
});
Route::group(['prefix' => 'persona'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'PersonaController@index',
            'as' => 'persona'
        ]);
    });
});
Route::group(['prefix' => 'permiso'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'PermisoController@index',
            'as' => 'permiso'
        ]);
        Route::match(['get', 'post'], '/habilitado', [
            'uses' => 'PermisoController@habil',
            'as' => 'habilitado'
        ]);
        Route::match(['get', 'post'], '/setmenu', [
            'uses' => 'PermisoController@setMenu',
            'as' => 'setmenu'
        ]);
        Route::match(['get', 'post'], '/cambio', [
            'uses' => 'PermisoController@cambio',
            'as' => 'cambio'
        ]);
        Route::match(['get', 'post'], '/asignar', [
            'uses' => 'PermisoController@Asigna',
            'as' => 'asignar'
        ]);
        Route::match(['get', 'post'], '/remover', [
            'uses' => 'PermisoController@removerPermiso',
            'as' => 'remover'
        ]);
    });
});


Route::group(['prefix' => 'rol'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'RolController@index',
            'as' => 'rol'
        ]);
    });
});
Route::group(['prefix' => 'expedido'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ExpedidoController@index',
            'as' => 'expedido'
        ]);
    });
});
Route::group(['prefix' => 'empresa'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'EmpresaController@index',
            'as' => 'empresa'
        ]);
    });
});

Route::group(['prefix' => 'log'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get'], '/', [
            'uses' => 'LogController@index',
            'as' => 'log'
        ]);
    });
});






Route::group(['prefix' => 'producto'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ProductoController@index',
            'as' => 'producto'
        ]);
    });
});

Route::group(['prefix' => 'cliente'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ClientesController@index',
            'as' => 'cliente'
        ]);
    });
});
Route::group(['prefix' => 'proveedor'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ProveedorController@index',
            'as' => 'proveedor'
        ]);
    });
});
Route::group(['prefix' => 'compras'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'ComprasController@index',
            'as' => 'compras'
        ]);

        Route::match(['get', 'post'], '/create', [
            'uses' => 'ComprasController@create',
            'as' => 'compras.create'
        ]);
    });
});
Route::group(['prefix' => 'fabrica'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'FabricaController@index',
            'as' => 'fabrica'
        ]);
    });
});
Route::group(['prefix' => 'almacen'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'AlmacenController@index',
            'as' => 'almacen'
        ]);
    });
});
Route::group(['prefix' => 'inventario'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'InventarioController@index',
            'as' => 'inventario'
        ]);
        Route::match(['get', 'post'], '/create', [
            'uses' => 'InventarioController@create',
            'as' => 'inventario.create'
        ]);
        Route::match(['get', 'post'], '/prints', [
            'uses' => 'InventarioController@print',
            'as' => 'inventario.prints'
        ]);
    });
});
Route::group(['prefix' => 'venta'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/', [
            'uses' => 'VentaController@index',
            'as' => 'venta'
        ]);
        Route::match(['get', 'post'], '/list', [
            'uses' => 'VentaController@lists',
            'as' => 'venta.list'
        ]);
        Route::match(['get', 'post'], '/crear', [
            'uses' => 'VentaController@create',
            'as' => 'venta.crear'
        ]);

        Route::match(['get', 'post'], '/show', [
            'uses' => 'VentaController@show',
            'as' => 'venta.show'
        ]);
        Route::match(['get', 'post'], '/destroy', [
            'uses' => 'VentaController@destroy',
            'as' => 'venta.destroy'
        ]);
        Route::match(['get', 'post'], '/producto', [
            'uses' => 'VentaController@getProducto',
            'as' => 'venta.producto'
        ]);
        Route::match(['get', 'post'], '/aprobar', [
            'uses' => 'VentaController@aprobar',
            'as' => 'venta.aprobar'
        ]);
    });
});
