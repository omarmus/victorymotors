-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-08-2020 a las 22:51:42
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `id` int(11) NOT NULL,
  `id_producto` varchar(50) NOT NULL,
  `fecha` date NOT NULL DEFAULT '2019-01-01',
  `stock` decimal(10,0) NOT NULL,
  `id_users` int(11) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`id`, `id_producto`, `fecha`, `stock`, `id_users`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, '1', '2020-08-16', '20', 1, 0, '2020-08-17 02:21:43', '2020-08-17 02:21:43'),
(2, '2', '2020-08-19', '10', 1, 0, '2020-08-19 04:10:27', '2020-08-19 04:10:27'),
(3, '3', '2020-08-19', '5', 1, 0, '2020-08-19 20:56:36', '2020-08-19 20:56:36'),
(4, '5', '2020-08-28', '6', 13, 0, '2020-08-29 00:14:25', '2020-08-29 00:14:25'),
(5, '6', '2020-08-28', '5', 1, 0, '2020-08-29 02:26:25', '2020-08-29 02:26:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `telefono` varchar(18) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `direccion` text NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `telefono`, `correo`, `direccion`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, 'juan lopez yana', '7894661', 'juan@gmail.com', 'C.los tajivos N°12', 0, '2020-08-13 04:01:56', '2020-08-13 04:01:56'),
(2, 'diego cruz gutierrez', '7856123', 'diego@gmail.com', 'C. Grau entre Laos N°78', 0, '2020-08-19 22:46:45', '2020-08-19 22:46:04'),
(3, 'juan perez alanoca', '78946133', 'juan@gmail.com', 'C. Uruguay entre los Palmos N°4', 0, '2020-08-25 22:34:28', '2020-08-25 22:29:17'),
(4, 'Ducelia', '78946513', 'dulce@hotmail.com', 'Av. Puerto Escobar N°56', 0, '2020-08-25 22:35:56', '2020-08-25 22:35:44'),
(5, 'juan alberto gutierres', '79846512', 'alberto@gmail.com', 'C. Uruguay entre los Palmos N°4', 0, '2020-08-27 04:24:40', '2020-08-25 22:50:28'),
(6, 'favian', '7946513', 'favi@hotmail.com', 'C. atalamos N°99', 0, '2020-08-27 04:27:06', '2020-08-25 23:06:52'),
(7, 'ailyn condori ticona', '78945612', 'condori@hotmail.com', 'C. cruz papal entre el Tam N°59', 0, '2020-08-27 04:26:05', '2020-08-25 23:12:00'),
(8, 'juan lopez hidalgo', '78945612', 'juan@hotmail.com', 'C. Los tajivos entre pacajes N°12', 0, '2020-08-26 02:05:00', '2020-08-26 02:05:00'),
(9, 'Luis Callisaya Kuno', '78945645', 'luis@gmailcom', 'C.Ayacucho N°45', 0, '2020-08-27 04:08:03', '2020-08-27 04:08:03'),
(10, 'Carlos Gutierrez Alanca', '78945233', 'carlos@gmail.com', 'C. Tiquina N°78', 0, '2020-08-29 02:27:45', '2020-08-29 02:27:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `stock` decimal(10,0) DEFAULT NULL,
  `id_users` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(30) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `red_social` varchar(50) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `telefono` int(30) DEFAULT NULL,
  `direccion` varchar(150) NOT NULL,
  `logo` varchar(36) DEFAULT 'default.png',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `red_social`, `correo`, `telefono`, `direccion`, `logo`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, 'VictoryM Bolivia', 'victory motors', 'victorymotors012@gmail.com', 24456128, 'Av. Juan Pablo II frente al TAM', '1572117248victorymotors.png', 1, '2020-08-29 02:20:04', '2019-09-28 19:13:03'),
(2, 'victoryMotors', 'victorymotor', 'victory@gmail.com', 78945122, 'Av, juan palo II', '159841064702-KEYTON-CRISCAR-1.png', 1, '2020-08-27 06:17:48', '2020-08-26 01:58:01'),
(3, 'victoryMotors', 'victorymotor', 'victory@gmail.com', 78945122, 'Av, juan palo II', '159840708004-KING-LONG-CRISCAR.png', 1, '2020-08-26 02:57:11', '2020-08-26 01:58:02'),
(4, 'Vcitory', 'victory', 'victory@gmail.com', 78945612, 'En la ruz papal frente al Tam', '159865347002-KEYTON-CRISCAR-1.png', 1, '2020-08-28 22:25:13', '2020-08-28 22:24:32'),
(5, 'vicrtory', 'victory', 'victory@gmail.com', 78945122, 'En la ruz papal frente al Tam', '159866758304-KING-LONG-CRISCAR.png', 0, '2020-08-29 02:19:45', '2020-08-29 02:19:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expedido`
--

CREATE TABLE `expedido` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `expedido`
--

INSERT INTO `expedido` (`id`, `nombre`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(5, 'BN', 1, 0, '2019-10-22 01:36:16', '2019-10-22 01:36:16'),
(2, 'CBBA', 1, 0, '2019-10-22 01:35:54', '2019-10-22 01:35:54'),
(1, 'LP', 1, 0, '2019-10-22 01:36:04', '2019-09-27 23:43:25'),
(3, 'OR', 1, 0, '2019-10-22 01:35:54', '2019-10-22 01:35:54'),
(4, 'PD', 1, 0, '2019-10-22 01:36:16', '2019-10-22 01:36:16'),
(9, 'PT', 1, 0, '2019-10-22 01:36:55', '2019-10-22 01:36:55'),
(7, 'SC', 1, 0, '2019-10-22 01:36:31', '2019-10-22 01:36:31'),
(6, 'STC', 1, 0, '2019-10-22 01:36:31', '2019-10-22 01:36:31'),
(8, 'TJ', 1, 0, '2019-10-22 01:36:55', '2019-10-22 01:36:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabrica`
--

CREATE TABLE `fabrica` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fabrica`
--

INSERT INTO `fabrica` (`id`, `nombre`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(2, 'asddsa', 1, 0, '2019-09-28 15:43:35', '2019-09-27 00:43:49'),
(6, 'Camioneta', 1, 0, '2020-08-29 02:28:23', '2020-08-29 02:28:23'),
(4, 'Kinglong', 1, 0, '2020-08-19 03:49:58', '2020-08-19 03:49:58'),
(3, 'Minibus', 1, 0, '2020-08-19 04:02:36', '2019-09-28 20:04:39'),
(5, 'TKing', 1, 0, '2020-08-27 05:11:45', '2020-08-27 05:11:45'),
(1, 'Vagoneta', 1, 0, '2020-08-19 04:02:58', '2019-09-27 00:43:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `icos` varchar(25) NOT NULL DEFAULT 'ico- file',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id`, `nombre`, `icos`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(2, 'Adminstrador', 'fa-cog', 1, 0, '2020-08-14 02:20:00', '2019-09-28 11:57:08'),
(6, 'Alamcen', 'fa-list', 1, 0, '2020-08-28 20:30:53', '2020-08-28 20:30:53'),
(10, 'Empresa', 'fa-table', 1, 0, '2020-08-28 22:40:23', '2020-08-28 22:40:23'),
(1, 'Grupo Menu', 'wi-file', 1, 0, '2020-08-19 03:39:23', '2019-09-28 11:56:24'),
(5, 'Productos', 'fa-table', 1, 0, '2019-09-28 15:39:12', '2019-09-28 15:39:12'),
(4, 'Reportes', 'fa-table', 1, 0, '2019-09-28 15:38:55', '2019-09-28 15:38:55'),
(7, 'Stock', 'fa-edit', 1, 0, '2020-08-28 20:31:23', '2020-08-28 20:31:23'),
(9, 'Usuarios', 'fa-table', 1, 0, '2020-08-28 22:39:32', '2020-08-28 22:39:32'),
(8, 'Ventas', 'fa-list', 1, 0, '2020-08-28 20:31:52', '2020-08-28 20:31:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `id_producto` varchar(50) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `stock` decimal(10,0) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `id_producto`, `precio`, `stock`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, '1', '14800.00', '6', 1, 0, '2020-08-30 03:47:17', '2020-08-17 04:19:09'),
(2, '2', '17500.00', '6', 1, 0, '2020-08-29 02:30:04', '2020-08-19 04:11:08'),
(3, '3', '59800.00', '4', 1, 0, '2020-08-19 21:04:00', '2020-08-19 21:04:00'),
(4, '5', '79800.00', '5', 1, 0, '2020-08-29 00:15:42', '2020-08-29 00:15:42'),
(5, '6', '78560.00', '0', 1, 0, '2020-08-30 03:54:21', '2020-08-29 02:30:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `url` varchar(40) NOT NULL,
  `icos` varchar(25) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `titulo`, `url`, `icos`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(13, 'almacen', 'Almacen', 'almacen', 'fa-table', 1, 0, '2019-09-28 16:39:48', '2019-09-28 16:39:48'),
(4, 'cliente', 'Lista Cliente', 'cliente', 'fa-table', 1, 0, '2019-09-28 14:20:48', '2019-09-28 14:20:48'),
(7, 'compras', 'Compras', 'compras', 'fa-table', 1, 0, '2019-09-28 14:21:50', '2019-09-28 14:21:50'),
(15, 'empresa', 'Empresa', 'empresa', 'fa-table', 1, 0, '2019-09-28 16:40:22', '2019-09-28 16:40:22'),
(8, 'fabrica', 'Listar Categoria', 'fabrica', 'fa-table', 1, 0, '2020-08-19 04:01:20', '2019-09-28 14:22:13'),
(5, 'grupo', 'Grupo Menu', 'grupo', 'fa-table', 1, 0, '2019-09-28 14:21:10', '2019-09-28 14:21:10'),
(14, 'inventario', 'Stock', 'inventario', 'fa-table', 1, 0, '2019-10-06 20:23:00', '2019-09-28 16:40:07'),
(3, 'menu', 'Menu', 'menu', 'fa-table', 1, 0, '2019-09-28 12:07:21', '2019-09-28 12:07:21'),
(12, 'perfil', 'perfil Usuario', 'perfil', 'fa-table', 1, 1, '2019-09-28 20:11:20', '2019-09-28 16:39:22'),
(10, 'permiso', 'Permisos', 'permiso', 'fa-table', 1, 0, '2019-09-28 16:35:56', '2019-09-28 14:22:51'),
(1, 'persona', 'Listar Persona', 'persona', 'fa-table', 1, 0, '2019-09-28 12:03:49', '2019-09-28 12:03:30'),
(6, 'producto', 'Lista Producto', 'producto', 'fa-table', 1, 0, '2019-09-28 14:21:33', '2019-09-28 14:21:33'),
(2, 'proveedor', 'Proveedor', 'proveedor', 'fa-table', 1, 0, '2019-09-28 13:36:30', '2019-09-28 12:06:17'),
(17, 'reporte', 'Reportes', 'reporte', 'fa-file', 1, 0, '2020-08-13 21:32:41', '2020-08-13 21:32:41'),
(11, 'rol', 'Rol/Categoria', 'rol', 'fa-table', 1, 0, '2020-08-28 21:50:20', '2019-09-28 15:28:32'),
(16, 'users', 'Usuario', 'users', 'fa-table', 1, 0, '2019-10-06 01:34:08', '2019-10-06 01:34:08'),
(9, 'venta', 'Lista Ventas', 'venta', 'fa-table', 1, 0, '2019-09-28 14:22:30', '2019-09-28 14:22:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL,
  `header` varchar(100) NOT NULL,
  `vertical` varchar(100) NOT NULL,
  `fondo` varchar(100) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`id`, `header`, `vertical`, `fondo`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, '2', '2', '2', 1, 0, '2019-09-27 23:42:41', '2019-09-27 23:42:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `isdelete` tinyint(1) NOT NULL DEFAULT '1',
  `isupdate` tinyint(1) NOT NULL DEFAULT '1',
  `isnew` tinyint(1) NOT NULL DEFAULT '1',
  `isreporte` tinyint(1) NOT NULL DEFAULT '0',
  `isviewall` tinyint(1) NOT NULL DEFAULT '0',
  `isview` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `id_grupo`, `id_menu`, `id_rol`, `isdelete`, `isupdate`, `isnew`, `isreporte`, `isviewall`, `isview`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(32, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:48', '2020-08-21 02:29:01'),
(13, 1, 5, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2019-09-28 15:38:00', '2019-09-28 15:38:00'),
(23, 1, 13, 2, 1, 1, 1, 0, 0, 0, 1, 0, '2019-09-28 20:14:17', '2019-09-28 20:14:17'),
(5, 2, 3, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:47', '2019-09-28 14:15:47'),
(28, 2, 4, 2, 1, 1, 1, 0, 0, 0, 1, 0, '2019-11-10 19:56:17', '2019-11-10 19:56:17'),
(27, 2, 9, 2, 1, 1, 1, 0, 0, 0, 1, 0, '2019-11-10 19:56:12', '2019-11-10 19:56:12'),
(7, 2, 10, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-17 03:21:42', '2019-09-28 14:24:07'),
(12, 2, 11, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-17 03:21:42', '2019-09-28 15:28:45'),
(19, 2, 13, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:47', '2019-09-28 16:40:37'),
(25, 2, 13, 3, 1, 1, 1, 0, 0, 0, 1, 0, '2019-09-28 20:16:41', '2019-09-28 20:16:41'),
(20, 2, 15, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:48', '2019-09-28 16:40:40'),
(26, 2, 16, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:48', '2019-10-06 01:34:23'),
(30, 4, 8, 2, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-13 00:19:46', '2020-08-13 00:19:46'),
(31, 4, 17, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-13 22:02:00', '2020-08-13 22:02:00'),
(35, 4, 17, 4, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:34:57', '2020-08-28 22:28:47'),
(41, 4, 17, 5, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:51:35', '2020-08-28 22:51:35'),
(14, 5, 4, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:47', '2019-09-28 15:39:28'),
(17, 5, 6, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2019-09-28 15:39:39', '2019-09-28 15:39:39'),
(38, 5, 6, 4, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:35:07', '2020-08-28 22:35:07'),
(16, 5, 8, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-17 03:21:42', '2019-09-28 15:39:36'),
(39, 5, 8, 5, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:50:44', '2020-08-28 22:50:44'),
(18, 5, 9, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:47', '2019-09-28 15:39:41'),
(22, 5, 14, 1, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 23:50:47', '2019-09-28 16:40:48'),
(33, 6, 13, 4, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:34:57', '2020-08-26 02:00:52'),
(37, 7, 14, 4, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:34:09', '2020-08-28 22:34:09'),
(40, 8, 4, 5, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:51:02', '2020-08-28 22:51:02'),
(42, 8, 6, 5, 1, 1, 1, 0, 0, 0, 1, 0, '2020-08-28 22:52:05', '2020-08-28 22:52:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `paterno` varchar(30) DEFAULT '',
  `materno` varchar(30) DEFAULT '',
  `ci` varchar(15) NOT NULL,
  `id_expedido` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `nombre`, `paterno`, `materno`, `ci`, `id_expedido`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(6, 'genara', 'luna', 'chura', '12344646', 2, 1, 0, '2020-08-26 00:34:45', '2020-08-26 00:34:45'),
(2, 'vallll', 'dsjaj', 'jdfh', '56656565', 1, 1, 1, '2019-09-28 15:27:24', '2019-09-28 15:26:41'),
(3, 'cliente', '5555', '555', '66564464', 1, 1, 1, '2020-08-27 05:15:22', '2019-09-28 15:27:14'),
(1, 'Esther', 'Calle', 'Limachi', '66644678', 1, 1, 0, '2020-08-21 03:06:26', '2019-09-27 23:43:15'),
(8, 'Luis', 'Figueroa Roque', 'Roque', '7098545', 1, 1, 0, '2020-08-28 20:34:52', '2020-08-28 20:34:52'),
(9, 'Angy', 'Arizaga', 'Benz', '7412589', 2, 1, 0, '2020-08-28 20:35:25', '2020-08-28 20:35:25'),
(4, 'aaa', 'sss', 'dddd', '7894512', 7, 1, 1, '2020-08-27 05:15:28', '2020-08-17 02:58:54'),
(11, 'Gabriel', 'Luna', 'Condori', '7894523', 1, 1, 0, '2020-08-29 02:20:46', '2020-08-29 02:20:46'),
(7, 'pepe', 'hialri', 'poma', '78945612', 1, 1, 0, '2020-08-26 02:01:56', '2020-08-26 02:01:56'),
(5, 'lola', 'figueroa', 'calsina', '7894652', 2, 1, 0, '2020-08-19 03:24:57', '2020-08-19 03:24:57'),
(10, 'Guillermo', 'Lopez', 'Perez', '79845612', 3, 1, 0, '2020-08-28 22:26:05', '2020-08-28 22:26:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `chasis` varchar(50) NOT NULL,
  `motor` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `id_fabrica` int(11) NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `logo` varchar(80) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `nombre`, `modelo`, `chasis`, `motor`, `descripcion`, `id_fabrica`, `costo`, `logo`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(6, '78914522GHL', 'Jinkey', '2019', 'GHK-745', '78451266GH78', 'motor de cc2335', 3, '78543.00', '1598667962a1.png', 1, 0, '2020-08-29 02:26:04', '2020-08-29 02:26:04'),
(2, '1213AA12', 'Kinglong', '2016', '9BWCC5X918003018', 'JDH-09692', 'motor cilindrada 2300cc', 3, '17500.00', '15978100524-1-4-kingwin-cng-van_0.jpg', 1, 0, '2020-08-19 04:46:26', '2020-08-19 04:07:34'),
(3, '178912CVB123', 'Jincheng', '2019', '99FGB561674DC', 'KLM-0521', 'motor cilindrada 2900cc corona montaña', 3, '58600.00', '1597870570a4.png', 1, 0, '2020-08-19 20:56:12', '2020-08-19 20:56:12'),
(5, '482473362ML', 'Tking', '2019', '789512488KL', 'KLH-784512', 'motor cc2300', 1, '78760.00', '1598660001a12.png', 1, 0, '2020-08-29 00:13:23', '2020-08-29 00:13:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `sitio_web` varchar(200) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `rl_nombre` varchar(50) NOT NULL,
  `rl_apellidos` varchar(40) NOT NULL,
  `rl_corrreo` varchar(40) NOT NULL,
  `rl_telefono` varchar(20) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `nombre`, `sitio_web`, `telefono`, `rl_nombre`, `rl_apellidos`, `rl_corrreo`, `rl_telefono`, `direccion`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(3, 'aaaaaaaaaa', 'www.motor.com.co', '1321355', 'sssss', 'sss', 'sss', '5555', 'ssssss', 1, 0, '2020-08-17 04:16:12', '2020-08-17 02:32:31'),
(2, 'ewq', 'ewq', 'ewq', 'ewqqe', 'ewq', 'eqwe', 'qweq', 'e', 1, 0, '2019-09-28 20:41:42', '2019-09-28 20:41:42'),
(1, 'rew', 'rewrew', 'rewrew', 'rew', 'rewr', 'ewrew', 'rew', 'rw', NULL, 0, '2019-09-27 01:27:37', '2019-09-27 01:27:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `descripcion` varchar(190) NOT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `descripcion`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(1, 'Administrador', 'Enagado global sistema', 1, 0, '2020-08-28 21:48:22', '2019-09-27 23:42:34'),
(4, 'Almacenero', 'encargado del almacen', 1, 0, '2020-08-28 21:47:59', '2020-08-26 02:00:11'),
(2, 'Cliente', 'cliente de la empresa', 1, 0, '2020-08-28 21:48:46', '2019-09-27 23:42:34'),
(3, 'Reportes', 'solo reporte', 1, 0, '2019-09-28 15:37:00', '2019-09-28 15:37:00'),
(8, 'Vededor2', 'encargado de la venta', 1, 0, '2020-08-29 02:23:36', '2020-08-29 02:23:36'),
(5, 'Vendedor', 'Es el encargado de las ventas', 1, 0, '2020-08-28 21:47:40', '2020-08-28 21:46:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `email` varchar(78) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_rol` int(11) DEFAULT '2',
  `id_persona` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL DEFAULT 'wicoder.png',
  `estado` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `email`, `password`, `id_rol`, `id_persona`, `logo`, `estado`, `eliminado`, `updated_at`, `created_at`) VALUES
(11, 'ana', 'ana@yahoo.es', '$2y$10$9k9cCO6I3OU0/kxNMk4S1.TrjOnViKGdgH1mIPATgqnbyfUFS8luu', 1, 4, 'wicoder.png', 1, 0, '2020-08-26 00:56:11', '2020-08-26 00:56:11'),
(1, 'Esther', 'admin@gmail.com', '$2y$10$b21pGDO99uWFwxs83j2TT.aPBRxNfW2guYC/u4fqldngmiT4Ghmo.', 1, 1, 'wicoder.png', 1, 0, '2020-08-21 03:06:55', '2019-09-27 23:42:05'),
(17, 'Gabiel', 'gabriel@gmail.com', '$2y$10$pgkQbM4TdLwIh2hdoU3JoOpIHqn9.OtUWvrjloZMpYWMTLQk7icQ.', 4, 11, 'wicoder.png', 1, 0, '2020-08-29 02:22:04', '2020-08-29 02:21:38'),
(9, 'genara', 'genara@gmail.com', '$2y$10$.swdb/0VqDnuva8ypXdOvu7PqDW907Z45YHmr66/T786..O9EQgKm', 2, 6, 'wicoder.png', 1, 0, '2020-08-26 00:41:18', '2020-08-26 00:41:18'),
(13, 'Guillermo', 'guillermo@gmail.com', '$2y$10$oy9D8KtVKLIZAcL5/OIUXOsbMvx1eFGRTJau4VKhD0jRYheZKkPF2', 4, 10, 'wicoder.png', 1, 0, '2020-08-28 22:27:28', '2020-08-28 22:27:28'),
(4, 'jaimito', 'jaimito@gmail.com', '$2y$10$FYJOLT7QzqIJDFntcJcFvOfgXQF8x3klCTLoyzFaFy3OhG76Dg8fG', 2, 2, 'wicoder.png', 1, 0, '2020-08-17 21:03:16', '2019-10-06 12:50:40'),
(16, 'Laliest', 'lali@gmail.com', '$2y$10$LoiBHe3e8OxZb7rr/TvEBODEKIJPhTa1v3UJ53EAY8Ngf4HnCnQN2', 1, 1, 'wicoder.png', 1, 0, '2020-08-28 22:49:20', '2020-08-28 22:37:54'),
(10, 'lolis', 'lol@hotmail.com', '$2y$10$0MgfnSnBUJtOrZk8EhHA/O4BBOwV17JWs0w2TcFMpF9wGhLASBebW', 3, 5, 'wicoder.png', 1, 0, '2020-08-26 00:54:16', '2020-08-26 00:54:16'),
(12, 'pepe', 'pepe@gmail.com', '$2y$10$aTLtNFKpi1ImvDAbghumgunIQbzs0qt82kVWz6AC0dn8Vhnsi4j3a', 4, 7, 'wicoder.png', 1, 0, '2020-08-26 02:02:39', '2020-08-26 02:02:39'),
(3, 'pruebas', 'pruebas@gmail.com', '$2y$10$Tj9KEk0O3UzMJ2hXEdtA5OEAVOuDvnJ/FsRZXAkbQ7KUla0/f9Ly2', 1, 3, 'wicoder.png', 1, 0, '2019-10-06 02:46:48', '2019-10-06 02:13:22'),
(8, 'pruebass', 'pruebas@gmail.com', '$2y$10$fI06Mu08O3AuNXpZI3B2dOsNziWZiZD1Q.EE67fTbirdzHSg3gWnK', 1, 2, 'wicoder.png', 1, 0, '2019-10-26 12:51:52', '2019-10-26 12:51:52'),
(2, 'will', 'will@gmail.com', '$2y$10$8FX.Pmtt5ZJYWg2iBD0kJubnFgIXKfaMXTIzR5Mgw5TMdNYE8TMeO', 3, 3, 'wicoder.png', 1, 0, '2019-09-29 00:56:53', '2019-09-29 00:56:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `total` varchar(10) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avance` varchar(20) NOT NULL DEFAULT 'pendiente',
  `tipo_pago` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `codigo`, `fecha`, `id_cliente`, `id_users`, `total`, `estado`, `eliminado`, `updated_at`, `created_at`, `avance`, `tipo_pago`) VALUES
(10, '456', '2020-08-28', 1, 1, '17,350.00', NULL, 0, '2020-08-29 03:54:17', '2020-08-29 03:54:17', 'pendiente', 'efectivo'),
(2, '123', '2020-08-17', 1, 1, '14,800.00', NULL, 0, '2020-08-19 22:57:38', '2020-08-18 07:17:42', 'pendiente', 'cheque'),
(3, '148874', '2020-08-17', 1, 1, '29,480.00', NULL, 0, '2020-08-20 01:35:22', '2020-08-18 07:21:30', 'cerrado', 'efectivo'),
(4, '457845', '2020-08-17', 1, 1, '14,800.00', NULL, 0, '2020-08-20 01:35:09', '2020-08-18 07:25:02', 'pendiente', 'cheque'),
(5, '12', '2020-08-19', 1, 1, '34,950.00', NULL, 0, '2020-08-20 02:43:54', '2020-08-19 08:15:41', 'pendiente', 'efectivo'),
(11, '145', '2020-08-28', 2, 1, '17,400.00', NULL, 0, '2020-08-29 00:02:14', '2020-08-29 04:01:31', 'cerrado', 'efectivo'),
(7, '52152', '2020-08-19', 1, 1, '14,667.00', NULL, 0, '2020-08-30 03:47:17', '2020-08-20 00:44:14', 'cerrado', 'cheque'),
(8, '45', '2020-08-25', 3, 1, '14,677.00', NULL, 0, '2020-08-26 06:11:37', '2020-08-26 06:11:37', 'pendiente', 'cheque'),
(9, '147', '2020-08-26', 8, 1, '17,500.00', NULL, 0, '2020-08-26 23:58:53', '2020-08-27 03:58:27', 'cerrado', 'efectivo'),
(12, '78', '2020-08-28', 10, 1, '17,350.00', NULL, 0, '2020-08-29 02:30:04', '2020-08-29 06:29:32', 'cerrado', 'efectivo'),
(13, '159', '2020-08-29', 1, 1, '78,360.00', NULL, 0, '2020-08-30 03:50:20', '2020-08-30 07:44:40', 'cerrado', 'efectivo'),
(14, '4567', '2020-08-29', 7, 1, '78,560.00', NULL, 0, '2020-08-30 03:46:48', '2020-08-30 07:46:03', 'cerrado', 'cheque'),
(15, '753', '2020-08-29', 6, 1, '157,120.00', NULL, 0, '2020-08-30 03:54:21', '2020-08-30 07:51:56', 'cerrado', 'efectivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) DEFAULT NULL,
  `id_producto` varchar(50) NOT NULL,
  `cantidad` decimal(10,0) NOT NULL,
  `precio_uni` decimal(10,2) NOT NULL,
  `eliminado` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `descuento` float DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id`, `id_venta`, `id_producto`, `cantidad`, `precio_uni`, `eliminado`, `updated_at`, `created_at`, `descuento`) VALUES
(1, 2, '1', '1', '14800.00', 0, '2020-08-18 07:27:28', '2020-08-18 03:17:42', 0),
(2, 3, '1', '2', '14800.00', 0, '2020-08-18 03:21:30', '2020-08-18 03:21:30', 0),
(3, 4, '1', '1', '14800.00', 0, '2020-08-20 02:38:27', '2020-08-18 03:25:02', 0),
(4, 5, '2', '2', '17500.00', 0, '2020-08-20 02:43:54', '2020-08-19 04:15:41', 50),
(6, 7, '1', '1', '14800.00', 0, '2020-08-19 20:44:14', '2020-08-19 20:44:14', 0),
(7, 6, '1', '1', '14800.00', 0, '2020-08-20 02:50:58', '2020-08-19 21:34:02', 50),
(8, 8, '1', '1', '14800.00', 0, '2020-08-26 02:11:37', '2020-08-26 02:11:37', 0),
(9, 9, '2', '1', '17500.00', 0, '2020-08-26 23:58:27', '2020-08-26 23:58:27', 0),
(10, 10, '2', '1', '17500.00', 0, '2020-08-28 23:54:17', '2020-08-28 23:54:17', 0),
(11, 11, '2', '1', '17500.00', 0, '2020-08-29 00:01:31', '2020-08-29 00:01:31', 0),
(12, 12, '2', '1', '17500.00', 0, '2020-08-29 02:29:33', '2020-08-29 02:29:33', 0),
(13, 13, '6', '1', '78560.00', 0, '2020-08-30 03:44:40', '2020-08-30 03:44:40', 0),
(14, 14, '6', '1', '78560.00', 0, '2020-08-30 03:46:03', '2020-08-30 03:46:03', 0),
(15, 15, '6', '1', '78560.00', 0, '2020-08-30 07:53:51', '2020-08-30 03:51:56', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`,`id_producto`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expedido`
--
ALTER TABLE `expedido`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `fabrica`
--
ALTER TABLE `fabrica`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`nombre`,`url`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`header`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id_grupo`,`id_menu`,`id_rol`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_permiso_rol` (`id_rol`),
  ADD KEY `fk_permiso_menu` (`id_menu`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`ci`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_persona_expedido` (`id_expedido`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_producto_fabrica` (`id_fabrica`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usuario`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_usuario_rol` (`id_rol`),
  ADD KEY `fk_usuario_persona` (`id_persona`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_cotizacion_cliente` (`id_cliente`),
  ADD KEY `fk_cotizacion_users` (`id_users`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `fk_vendetall_venta` (`id_venta`),
  ADD KEY `fk_vendetall_producto` (`id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `expedido`
--
ALTER TABLE `expedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `fabrica`
--
ALTER TABLE `fabrica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD CONSTRAINT `fk_permiso_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`),
  ADD CONSTRAINT `fk_permiso_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`),
  ADD CONSTRAINT `fk_permiso_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_persona_expedido` FOREIGN KEY (`id_expedido`) REFERENCES `expedido` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_usuario_persona` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`),
  ADD CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
