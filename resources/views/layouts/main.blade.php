<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="description" content="">

        <!-- Open Graph Meta-->
        <meta property="og:type" content="website">
        <meta property="og:description" content="">
        <title>VictoryMotorsBolivia</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/css/main.css">
        <link href="{{ asset('plugins') }}/css/toastr.css" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins') }}/choose/choose.min.css" rel="stylesheet" type="text/css"/>
        <link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- Essential javascripts for application to work-->
        <script src="{{ asset('plugins') }}/js/jquery-3.2.1.min.js"></script>

        <script src="{{ asset('plugins') }}/js/popper.min.js"></script>
        <script src="{{ asset('plugins') }}/js/bootstrap.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="{{ asset('plugins') }}/js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{{ asset('plugins') }}/js/plugins/dataTables.bootstrap.min.js"></script>

        <script src="{{ asset('plugins') }}/js/plugins/pace.min.js"></script>


        <script src="{{ asset('plugins') }}/js/toastr.min.js" type="text/javascript"></script>
        <script src="{{ asset('plugins') }}/choose/choose.min.js" type="text/javascript"></script>
        <!-- Page specific javascripts-->

        <script src="{{ asset('plugins') }}/js/util.min.js" type="text/javascript"></script>
        <script src="{{ asset('plugins') }}/js/validate.min.js" type="text/javascript"></script>
        <script src="{{ asset('plugins') }}/js/wicoder.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="{{ asset('plugins') }}/js/plugins/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="{{ asset('plugins') }}/js/plugins/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="{{ asset('plugins') }}/js/plugins/sweetalert.min.js"></script>
        <script>
            var csrf_token = "{{ csrf_token() }}";
        </script>
        <style>
            .accion{
                justify-content: space-evenly;
                float: right;
                right: 22px;
                position: absolute;
                margin-top: -8px;
            }
            .abc{
                padding: 1px 11px; border-bottom: 1px solid #989898;
            }
            .modal {
                background: rgba(82, 82, 82, 0.8) !important;
            }
            body {
                font-size: 14px !important;
            }
        </style>
    </head>
    <body class="app sidebar-mini rtl">
        <!-- Navbar-->
      <!--   <style>
        	.app-header{
        		background-color: limegreen !important;
        	}
        	.app-header__logo{
        		background-color: forestgreen !important;
        		font-family: "Open Sans", Georgia, Serif !important;
        	}	
        	
        		a:hover{
          background-color: forestgreen !important
        		}
        		.app-sidebar__toggle{
        		background-color: limegreen !important;
        	}
        	
        </style> -->
        <header class="app-header"><a class="app-header__logo" ><?= session('empresa')->nombre ?></a>
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">
                <!-- <li class="app-search">
                    <input class="app-search__input" type="search" placeholder="Search">
                    <button class="app-search__button"><i class="fa fa-search"></i></button>
                </li> -->
                <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
                    <ul class="dropdown-menu settings-menu dropdown-menu-right">
                        <!--<li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>-->
                        <!--<li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>-->
                        <li><a class="dropdown-item dropdown-item" href="{{ route('logout') }}" 
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-lg"></i> Cerrar Session

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a></li>
                    </ul>
                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="app-sidebar">
            <div class="app-sidebar__user"><img style="    width: 45px;" class="app-sidebar__user-avatar" src="{{ URL::asset('img/users.png')}}" alt="User Image">
                <div>
                    <p class="app-sidebar__user-name" style="font-size: 13px;"><?= session('usuario')->nombre . ' ' . session('usuario')->paterno . ' ' . session('usuario')->materno ?></p>
                    <p style="text-align: center;" class="app-sidebar__user-designation"> <?= session('rol')->nombre ?></p>
                </div>
            </div>
            <ul class="app-menu">
                <li>
                    <a class="app-menu__item" href="home"><i class="app-menu__icon fa fa-laptop"> </i><span class="app-menu__label">Inicio</span></a>
                </li>
                <?=
                session('menus')
                ?>
                <!-- <li>
                    <a class="app-menu__item" href="reporte"><i class="app-menu__icon fa fa-list"> </i><span class="app-menu__label">Reportes</span></a>
                </li> -->
            </ul>
        </aside>
        <main class="app-content">
            @yield('content')
        </main>


        <script src="{{ asset('plugins') }}/js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->


    </body>
</html>