<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Nota de Venta</title>
    </head>
    <body>
        <style type="text/css">
            body {
                background-color: #fff;
                margin: 30px;
                font-family: Arial;
                font-size: 12px;
                color: #272727;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                text-transform: uppercase;
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 24px 0 2px 0;
                padding: 5px 0 6px 0;
            }  
            .span,h4{
                text-align: center;
                width: 350px;
                color: #444;
                background-color: transparent;
                border-top: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 90px 0 2px 0;
                padding: 3px 100px 3px 100px;
            }


            table{

                background-color: white;
                text-align: left;
                border-collapse: collapse;
                width: 100%;
            }

            th, td{
                padding: 5px;

            }
            .tables  th , .tables td{
                border: 1px solid #999;
            }

            thead{
                color: #1b1919;
            }
            table thead tr th,
            table thead tr td,
            table tbody tr td{
                border-top: 1px solid #000;
                font-size: 12px;
                font-weight: 800;
            }
            table thead tr th,
            table thead tr td{
                text-align: center
            }
            table tbody tr td{
                font-weight: 400 !important ;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .center{
                text-align: center
            }
            .left{
                text-align: initial;
            }
            .border tbody tr td {
                border: none !important;
            }
            .font-s{
                font-size: 10px;font-weight: 500;
            }
            .titles{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .spanes{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .dellwidth{
                width:200px
            }
            .fecha{
                /*float: right;*/
                width:   630px;    
                font-size: 12px;
                text-align: right
            }
            .font-12{
                font-size: 12px;
            }
        </style>

        <?php
        if (!empty($venta)) {

            $html = '';

            foreach ($venta as  $row) {
                $html .= '<br>
            <table class="border">
                <tr > <td> <img style="width: 120px;" id="imagenes" src="' . asset('files/' . session('empresa')->logo) . '"></td>
                    <td style="width: 400px; padding-right:50px;">
                        <h2 style="font-size: 28px !important;text-align: center; padding: 10px; " class="font-12 titles">
                            <span style="border-bottom: 1px solid  #000">NOTA - VENTA</span>  </h2>
                    </td> <td></td>
                </tr>
            </table>';
                $html .= '<table class="border" >
                <tr><td style="padding: 0;width: 500px;"><span style="color: #000;font-weight: 800 !important;">SEÑORES: </span><span>' . strtoupper($row->nombre) . '</span></td></tr>
                <tr><td style="padding: 0;"> </td>
                    <td style="padding: 0;"><span  style="text-align: right;">
                    <strong style="color: #000;font-weight: 800 !important;">FECHA:</strong> ' . $row->fecha . '</span> </td></tr>
                <tr><td style="padding: 0;"><span style="color: #000;font-weight: 800 !important;">REFERENCIA:</span> <span>Nota de venta</span></td></tr>
            </table><br><br><br>';
            
                $html .= '<table border="1">
                <thead>
                    <tr>
                        <td style="width: 25px">CANTIDAD</td>
                        <td >PRODUCTO</td>
                        <td  style="width: 85px">PRECIO UNITARIO EN<br> BS</td>
                        <td style="width: 75px">PRECIO TOTAL EN<br> BS</td>
                    </tr>
                </thead>
                <tbody>';
                $cant = 0;
                $total = 0;
                $cansum = 0;
                $c = 1;
                foreach ($row->detalle as $rows) {
                    $cant = intval($cant + $rows->cantidad);
                    $total = intval($cant + $rows->precio_uni);
                    $cansum = $cansum + intval($rows->cantidad * $rows->precio_uni);
                    $html .= '<tr>
                            <td style="text-align: center">' . $rows->cantidad . '</td>
                            <td style="text-align: left">' . $rows->producto . '</td>
                            <td style="text-align: center">' . $rows->precio_uni . '</td>
                            <td style="text-align: center">' . intval($rows->cantidad * $rows->precio_uni) . '</td>
                        </tr>';
                    $c++;
                    $html .= '
                    <tr >
                        <td  colspan="4" style="border-bottom: 1px solid #000; text-align: right">Total: ' . number_format($cansum, 2, ',', '.') . '</td>
                    </tr>';
                }

                $html .= '</tbody> </table>';

                $html .= '<br><div style="page-break-before: always;"> </div>';
            }
            echo $html;
        } else {
            echo '<h1><strong style="text-align:center">No hay datos</strong></h1>';
        }
        ?>
    </body>
</html>