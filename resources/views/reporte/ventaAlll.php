<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Nota de Venta</title>
    </head>
    <body>
        <style type="text/css">
            body {
                background-color: #fff;
                margin: 30px;
                font-family: Arial;
                font-size: 12px;
                color: #272727;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                text-transform: uppercase;
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 24px 0 2px 0;
                padding: 5px 0 6px 0;
            }  
            .span,h4{
                text-align: center;
                width: 350px;
                color: #444;
                background-color: transparent;
                border-top: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 90px 0 2px 0;
                padding: 3px 100px 3px 100px;
            }


            table{

                background-color: white;
                text-align: left;
                border-collapse: collapse;
                width: 100%;
            }

            th, td{
                padding: 5px;

            }
            .tables  th , .tables td{
                border: 1px solid #999;
            }

            thead{
                color: #1b1919;
            }
            table thead tr th,
            table thead tr td,
            table tbody tr td{
                border-top: 1px solid #000;
                font-size: 12px;
                font-weight: 800;
            }
            table thead tr th,
            table thead tr td{
                text-align: center
            }
            table tbody tr td{
                font-weight: 400 !important ;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .center{
                text-align: center
            }
            .left{
                text-align: initial;
            }
            .border tbody tr td {
                border: none !important;
            }
            .font-s{
                font-size: 10px;font-weight: 500;
            }
            .titles{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .spanes{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .dellwidth{
                width:200px
            }
            .fecha{
                /*float: right;*/
                width:   630px;    
                font-size: 12px;
                text-align: right
            }
            .font-12{
                font-size: 12px;
            }
        </style>

        <?php
        $html = '';
        if (!empty($venta)) {

            
            $html .= '
            <table class="border">
                <tr > <td> <img style="width: 110px;" id="imagenes" src="' . asset('files/' . session('empresa')->logo) . '"></td>
                    <td style="width: 500px; padding-right:100px;">
                        <h2 style="font-size: 28px !important;text-align: center; padding: 10px; " class="font-12 titles">
                            <span style="border-bottom: 1px solid  #000">REPORTE GENERAL - VENTAS</span>  </h2>
                    </td> <td></td>
                </tr>
            </table>';
            
            $html .= '
            <table class="table" border="1">
                <thead>
                    <tr>
                        <th>Nº</th>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Unitario</th>
                        <th>Precio Total</th>
                    </tr>
                </thead>
                <tbody>';
            $c = 1; 
            $t = 0;
            foreach ($venta as  $row) {
            $cant = 0;
                $total = 0;
                $cansum = 0;
                    
                    $html .= '<tr>
                        <td>'.$c.'</td>
                        <td>'.$row->fecha.'</td>
                        <td>'.$row->nombre.'</td>';
                       
            foreach ($row->detalle as $rows) {

                    $cant = intval($cant + $rows->cantidad);
                    $total = intval($cant + $rows->precio_uni);
                    $cansum = $cansum + intval($rows->cantidad * $rows->precio_uni);
                    $t = $t + $cansum;
            
                    $html .= ' 
                        <td style="text-align:center">'. $rows->producto .'</td>
                        <td style="text-align:center">'.$rows->cantidad.'</td>
                        <td style="text-align:center">'.$rows->precio_uni.'</td>
                        <td style="text-align:center">'.intval($rows->cantidad * $rows->precio_uni).'</td>
                    </tr>';
            
            }
            $c = $c + 1; 
            }
            $html .= '<tr>
                <td colspan="6" >TOTAL</td>
                <td style="text-align:center">'.$t.'</td>
            </tr>';
            $html .= '</tbody>
            </table>
            ';
            echo $html;
        } else {
            echo '<h1><strong style="text-align:center">No hay datos</strong></h1>';
        }
        ?>
    </body>
</html>