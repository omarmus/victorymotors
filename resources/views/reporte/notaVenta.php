<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Nota de Venta</title>
    </head>
    <body>
        <style type="text/css">
            body {
                background-color: #fff;
                margin: 30px;
                font-family: Arial;
                font-size: 12px;
                color: #272727;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                text-transform: uppercase;
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 24px 0 2px 0;
                padding: 5px 0 6px 0;
            }
            .span,h4{
                text-align: center;
                width: 350px;
                color: #444;
                background-color: transparent;
                border-top: 1px solid #D0D0D0;
                font-size: 16px;
                font-weight: bold;
                margin: 90px 0 2px 0;
                padding: 3px 100px 3px 100px;
            }


            table{

                background-color: white;
                text-align: left;
                border-collapse: collapse;
                width: 100%;
            }

            th, td{
                padding: 5px;


            }
            .tables  th , .tables td{
                border: 1px solid #999;
            }

            thead{
                color: #1b1919;
            }
            table thead tr th,
            table thead tr td,
            table tbody tr td{
                border-top: 1px solid #000;
                font-size: 12px;
                font-weight: 800;
            }
            table thead tr th,
            table thead tr td{
                text-align: center
            }
            table tbody tr td{
                font-weight: 400 !important ;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .font{
                font-weight: 800;
                font-size: 10px;
            }
            .center{
                text-align: center
            }
            .left{
                text-align: initial;
            }
            .border tbody tr td {
                border: none !important;
            }
            .font-s{
                font-size: 10px;font-weight: 500;
            }
            .titles{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .spanes{
                color: black;
                text-transform: uppercase;
                font-size: 12px !important;
                font-weight: 800 !important;
            }
            .dellwidth{
                width:200px
            }
            .fecha{
                /*float: right;*/
                width:   630px;
                font-size: 12px;
                text-align: right
            }
            .font-12{
                font-size: 12px;
            }
        </style>

        <?php
        if (!empty($detalle)) {
            ?>
            <br>
            <table class="border">
                <tr >
                    <td> <img style="width: 120px;" id="imagenes" src="<?= asset('files/' . session('empresa')->logo) ?>">
                    </td>
                    <td style="width: 200px; padding-right:1px;" >
                            <span style="text-align: center;"> Av. Juan Pablo II 2844 <br> (Entre la Cruz Papal y el Tam) <br> Tel/Cel:70128283</span>
                    </td>
                    <td style="width: 350px; padding-right:20px;">
                         <!--<img style="width: 200px;" id="imagenes" src="' . asset('files/' . session('empresa')->logo) . '">-->
                        <h2 style="font-size: 28px !important;text-align: center; padding: 10px; " class="font-12 titles">

                            <span>NOTA DE VENTA</span>  </h2>
                    </td>

                </tr>
            </table>
            <table class="border">
                <tr><td style="padding: 0;width: 500px;"><span style="color: #000;font-weight: 800 !important;">VICTORY MOTORS BOLIVIA </span></td></tr>
            </table><br>
            <hr>
            <table class="border">
                <tr><td style="padding: 0;width: 500px;"><span style="color: #000;font-weight: 800 !important;">SEÑORES: </span></td></tr>
                <tr><td style="padding: 0;"><?= strtoupper($venta->nombre) ?> </td>
                    <td style="padding: 0;"><span  style="text-align: right;"><strong style="color: #000;font-weight: 800 !important;">FECHA:</strong> <?= $venta->fecha ?></span> </td></tr>
                <!--<tr><td style="padding: 0;"><b style="color: #000;font-weight: 800 !important;">PRESENTE:</b> </td></tr>-->
                <tr><td style="padding: 0;"><span style="color: #000;font-weight: 800 !important;">REFERENCIA:</span> <span>Nota de venta</span></td></tr>
            </table>
            <br>
            <hr>
            <span style="padding: 0;width: 500px;"><span style="color: #000;font-weight: 800 !important;">Estimado Cliente:</span><br>
            <span>Estimados Señores. Por medio de la presente, y de acuerdo a su solicitud, sometemos a su consideración nuestra oferta
                como sigue:</span>
            <br><br>

            <table border="1">
                <thead style="background: rgba(143, 188, 143);">
                    <tr>
                        <td style="width: 25px">CANTIDAD</td>
                        <td >PRODUCTO</td>
                        <td style="width: 25px">NO.POLIZA</td>
                        <td  style="width: 85px">PRECIO UNITARIO EN<br> BS</td>
                        <td style="width: 75px">PRECIO TOTAL EN<br> BS</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $cant = 0;
                    $total = 0;
                    $cansum = 0;
                    $c = 1;

                    foreach ($detalle as $row) {

                        $cant = intval($cant + $row->cantidad);
                        $total = intval($cant + $row->precio_uni);
                        $cansum = $cansum + intval($row->cantidad * $row->precio_uni);
                        ?>
                        <tr>
                            <td style="text-align: center"><?= $row->cantidad ?></td>
                            <td style='text-align: left'><?= $row->producto ?></td>
                            <td style='text-align: center'><?= $row->codigo ?></td>
                            <td style="text-align: center"><?= $row->precio_uni ?></td>
                            <td style="text-align: center"><?= intval($row->cantidad * $row->precio_uni); ?></td>
                        </tr>
                        <?php
                        $c++;
                    }
                    ?>
                    <tr >
                        <td style="border-right: 0px;">TOTAL</td>
                        <td  colspan="4" style="border-bottom: 1px solid #000; text-align: right"> <?= number_format($cansum, 2, ',', '.') ?></td>
                    </tr>
                </tbody>

            </table>
            <?php if ($venta->tipo_pago == 'credito') : ?>
            <!-- <hr> -->
            <h3>Cuotas: <?php echo $venta->nro_cuotas ?> cuotas </h3>
            <table border="1">
                <thead>
                    <tr>
                        <th># Cuota</th>
                        <th>Total cuota</th>
                        <th>Pagado</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $cuotas = json_decode($venta->cuotas) ?>
                    <?php
                        if (!is_array($cuotas)) {
                            $cuotas = json_decode($cuotas);
                        }
                    ?>
                    <?php foreach ($cuotas as $row) : ?>
                    <tr>
                        <td style="text-align: right"><?php echo $row->id ?></td>
                        <td style="text-align: center"><?php echo $row->total ?></td>
                        <td style="text-align: center"><?php echo $row->pagado == 'true' ? 'SI' : 'NO' ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <?php endif ?>
            <br><br><br>
            <table >
                <tbody>
                    <tr>
                        <td colspan="5" style="border:none; text-align: center; color: #000;font-weight: 800 !important;"><span>NUESTROS PRECIOS INCLUYEN IMPUESTOS DE LEY</span> </td>
                    </tr>
                </tbody>
            </table>
            <?php
        } else {
            echo '<h1><strong style="text-align:center">No hay datos</strong></h1>';
        }
        ?>
    </body>
</html>
