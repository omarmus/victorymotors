@extends('layouts.main')
@section('content')

<?php
if (Auth::user()->id_rol == 1) {
    ?>
    <style>
        hr {
            margin-top: 0rem !important;
            margin-bottom: 0rem !important;
            border: 0 !important;
            border-top: 1px solid rgba(0, 0, 0, 0.28) !important;
        }
        .ss{
            font-size: 10px;
            font-family: cursive;
        }
    </style>

    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> Estadistica</h1>
            <p>Detalle de las ventas e historial</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Estadistica</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                <div class="info">
                    <h4>Clientes</h4>
                    <hr>
                    <p style="display: inline-grid;">
                        <b><?= $cliente->cantidad ?></b>
                        <b class="ss">gestion de cliente</b>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
                <div class="info">
                    <h4>Ventas {{ date('Y') }}</h4>
                    <hr>
                    <p style="display: inline-grid;">
                        <b><?= $vendida->total ?></b>
                        <b class="ss">Cantidad vendidad:<strong><?= $vendida->cantidad ?></strong></b>
                    </p>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small warning coloured-icon"><i class="icon fa fa-files-o fa-3x"></i>
                <div class="info">
                    <h4>Venta Pendiente</h4>
                    <hr>
                    <p style="display: inline-grid;">
                        <b><?= $pendiente->total ?></b>
                        <b class="ss">Cantidad pendiente:<strong><?= $pendiente->cantidad ?></strong></b>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-3">
            <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
                <div class="info">
                    <h4>Stock Actual</h4>
                    <hr>
                    <p style="display: inline-grid;">
                        <b><?= $stock->total ?></b>
                        <b class="ss">Cantidad:<strong><?= $stock->cantidad ?></strong></b>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Ventas</h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Estadisticas de Ventas </h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-md-12">
        <div class="tile">
            <h3 class="tile-title">Historial de las ultimas Ventas</h3>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>cliente</th>
                            <th>Fecha</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $html = "";
                        $c = 1;
                        foreach ($historial as $row) {
                            $html .= "<tr>";
                            $html .= "<td>" . $c . "</td>";
                            $html .= "<td>" . $row->nombre . "</td>";
                            $html .= "<td>" . $row->producto . "</td>";
                            $html .= "<td>" . $row->fecha . "</td>";
                            $html .= "<td>" . $row->precio_uni . "</td>";
                            $html .= "</tr>";
                            $c++;
                        }
                        echo $html;
                        ?>
                    </tbody>
                </table>
            </div>
            <a href="{{ route('venta') }}"  rel="venta" data-table="venta" title="historial de venta"> Ir a Ventas </a>
        </div>
    </div>
    <!--</main>--> 


    <script src="{{ asset('plugins') }}/js/jquery-3.2.1.min.js"></script>
    <script src="{{ asset('plugins') }}/js/plugins/chart.js" type="text/javascript"></script>

    <script>
        function getRandom() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
    }

    var labels = '<?= $line ?> ', meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"];

    var lineChar = [], jsChar = [], val = JSON.parse(labels);

    for (var row in meses) {
    lineChar[row] = 0;
    jsChar[row] = {mes: row, total: 0};
    for (var r in val) {
        if (parseInt(val[r].mes) == parseInt(row) + 1) {
            lineChar[row] = val[r].total;
            jsChar[row].mes = val[r].mes;
            jsChar[row].total = val[r].total;
        }
    }
    }

    console.log(lineChar);
    var data = {labels: meses,
    datasets: [
        {
            label: "Venta detalle",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: lineChar,
        }
    ]
    };


    var jdata = [];

    var color = getRandom();
    for (var key in jsChar) {
    jdata[key] = {
        value: jsChar[key].total,
        color: color,
        highlight: color,
        label: meses[key]
    };
    }

    new Chart($("#lineChartDemo").get(0).getContext("2d")).Line(data);
    new Chart($("#pieChartDemo").get(0).getContext("2d")).Pie(jdata);
    </script>
    <?php
} else {
    echo '
       <div class="page-error tile">
       <img style="width: 200px;" id="imagenes" src="' . asset('files/' . session('empresa')->logo) . '">
        <h1 style="font-size: 80px;">Bienvenido</h1>
        <p style="font-size: 30px;">Hola,' . session('usuario')->nombre . ' ' . session('usuario')->paterno . ' ' . session('usuario')->materno . '</p>
       </div>';
}
?>
@endsection