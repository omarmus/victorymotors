@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)"enctype="multipart/form-data">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre Empresa</label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombre" id="nombre" >
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input type="email" class="form-control" name="correo" id="correo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono/Cel</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control"  name="telefono" id="telefono" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Direccion</label>
                                    <input type="text" class="form-control"  name="direccion" id="direccion" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="red_social">Red social</label>
                                    <input type="text" class="form-control"  name="red_social" id="red_social" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">Logo<br>
                                    <img style="width: 110px; border: 3px solid #bbbbbbf5;" id="imagenes"   >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="logo">Subir logo</label>
                                    <input type="file" class="form-control"  name="logo" id="logo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  data-id="#CargarData"  
                                             onclick="empresa.save(this)" class="btn btn-primary">Actualizar Empresa</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de empresa</h1>

    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['empresa'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Empresa"   onclick="empresa.new(this)" > Nueva Empresa</button>';
            }
            
            echo $html;
            ?>
        </div>
    </ul>


</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Nombre  empresa</th>
                                <th>Red Social</th>
                                <th>telefono</th>
                                <th>correo</th>                                
                                <th>direccion</th>
                                <th>logo</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <script>
//        $(function () {
        //            $("#example1").DataTable();
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/empresa' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'empresa'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "nombre"},
                {"data": "red_social"},
                {"data": "telefono"},
                {"data": "correo"},
                {"data": "direccion"},
                {"data": "logo"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });


        (function (extend) {
            'use strict';


            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo','direccion']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {

                        var files = document.getElementById("logo");
                        var data = new FormData();

                        for (var i = 0; i < files.files.length; i++) {
                            data.append('files' + i, files.files[i]);
                        }
                        var formd = $($(event).data('id')).serializeArray();
                        for (var item in formd) {
                            data.append(formd[item].name, formd[item].value)
                        }

                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/empresa' }}",
                            type: "POST",
                            dataType: "json",
                            contentType: false,
                            data: data,
                            processData: false,
                            cache: false,
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje)

                            }
                        });
                    }
                });
            }

               extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };

            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        var origen = "<?= asset('files/') ?>";
                        $('#CargarData #imagenes').attr('src', origen + '/' + data.data.logo);
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
             extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        console.log(data);
                    }
                });
            };

            window.empresa = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);
        $("#logo").click(function () {
            console.log($(this).files);
        });

         function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }

    </script>
</div>
@endsection
