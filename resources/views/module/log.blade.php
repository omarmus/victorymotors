@extends('layouts.main')
@section('content')

<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Logs del sistema</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Tipo de operación</th>
                                <th>Fecha y hora de la operación</th>
                                <th>Sección del cambio</th>
                                <th>Valores afectados en la operación</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <script>
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/log' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'cliente'
                }
            },
            "columns": [
                {"data": "tipo"},
                {"data": "fecha"},
                {"data": "tabla"},
                {"data": "cambios"},
                // {"data": "new"},
                // {"data": "valor_alterado"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });
    </script>
</div>
@endsection
