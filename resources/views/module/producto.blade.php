@extends('layouts.main')
@section('content')
<style>
    .accion{
        justify-content: space-evenly;
        float: right;
        right: 22px;
        position: absolute;
        margin-top: -8px;
    }
</style>

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" >
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="codigo">No. Póliza</label>
                                    <input type="text" class="form-control" name="codigo" id="codigo" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Marca</label>
                                    <input type="text"class="form-control" onkeypress="return soloLetras(event)" class="form-control" name="nombre" id="nombre" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="modelo">modelo</label>
                                    <input type="text" class="form-control" name="modelo" id="modelo" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="descripcion">descripcion</label>
                                    <input type="text" class="form-control"  name="descripcion" id="descripcion" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_producto">Categoria</label>
                                    <select class="form-control chosen-select"  name="id_fabrica" style="width: 100% !important;">
                                        <option value="0">Selecciona categoria..</option>
                                        <?php
                                        foreach (options('fabrica') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_fabrica ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Chasis</label>
                                    <input type="text" class="form-control" name="chasis" id="CHASIS" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Motor</label>
                                    <input type="text" class="form-control" name="motor" id="motor" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="costo">Costo</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control" class="form-control"  name="costo" id="costo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="logo">Subir logo</label>
                                    <input type="file" class="form-control"  name="logo" id="logo" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">Logo<br>
                                    <img style="width: 110px; border: 3px solid #bbbbbbf5;" id="imagenes"   >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="producto.save(this)" class="btn btn-primary">Registrar </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Producto</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['producto'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= ' <button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Producto"   onclick="producto.new(this)" > Nuevo Producto</button>';
            }
            $html .= ' <button type="button" class="btn  btn-success" data-title="Imprimir"   onclick="producto.imprimir(this)" >'
                    . '<i class="fa fa-print" aria-hidden="true"></i> Imprimir Producto</button>';
            echo $html;
            ?>
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>No. Póliza</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Chasis</th> 
                                <th>Motor</th>                                 
                                <th>Categoria</th>
                                <th>descipcion</th>
                                <th>costo de venta</th>
                                <th>Imagen</th>
                                <th>estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
        <div class="modal fade" id="modal-imprimir">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <form  id="ajax_form" method="post" action="javascript:void(0)"> 
                    <input name="id" type="hidden"  id="id" >
                    <div class="modal-header">
                        <h4 class="modal-title">Lista de Productos</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="closeModal('#modal-imprimir')" >
                            <span aria-hidden="true"  >&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" content-modal="content" id="htmpprint"></div>
                    </div>
        </div>

    <script>

        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/producto' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'producto'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "codigo"},
                {"data": "nombre"},
                {"data": "modelo"},
                {"data": "chasis"},
                {"data": "motor"},
                {"data": "text_fabrica"},
                {"data": "descripcion"},
                {"data": "costo"},
                {"data": "logo"},
                {"data": "estado"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });

        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        data.estado ? toastr.success(data.mensaje) : toastr.error(data.mensaje);

                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        var files = document.getElementById("logo");
                        var data = new FormData();
                        for (var i = 0; i < files.files.length; i++) {
                            data.append('files' + i, files.files[i]);
                        }
                        var formd = $($(event).data('id')).serializeArray();
                        for (var item in formd) {
                            data.append(formd[item].name, formd[item].value)
                        }
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/producto' }}",
                            type: "POST",
                            dataType: "json",
                            contentType: false,
                            data: data,
                            processData: false,
                            cache: false,
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        var origen = "<?= asset('files/') ?>";
                        $('#CargarData #imagenes').attr('src', origen + '/' + data.data.logo);
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
          
                extend.imprimir = function (event) { 
                let h = '<iframe  frameborder="0" src="printProductos" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprint').html(h);
                $(".modal-title").text($(event).data('title'));
                $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };


            window.producto = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);

        function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }

    </script>
</div>
@endsection
