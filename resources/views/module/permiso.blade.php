@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-12">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <span class="input-group-addon"><span class="wif wif-pencil"></span></span>
                    <select id="roles" name="roles" onchange="UnidAjax(this)"  id="roles"  class="form-control">
                        <option value="0">Seleccione Rol...</option>
                        <?php
                        foreach (options('rol') as $value) {
                            ?>
                            <option value="<?= $value->id ?>"><?= $value->text_rol ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <span class="input-group-addon"><span class="wif wif-pencil"></span></span>
                    <select id="grupo" name="grupo" id="grupo"  class="form-control">
                        <option value="0">Seleccione Grupo...</option>
                        <?php
                        foreach (options('grupo') as $value) {
                            ?>
                            <option value="<?= $value->id ?>"><?= $value->text_grupo ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>

    </div>
</div>
<br>

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva venta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre completo cliente</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" >
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input type="text" class="form-control" name="correo" id="correo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="cliente.save(this)" class="btn btn-primary">Agregar Producto</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-header row" >
                <div>
                    <h3 class="card-title">Asignar Permiso</h3>
                </div>
                <div class="accion"> 
                    <input id="primero"  class="btn btn-primary"  type="button" onclick="Asignar(this)"  value="Asignar Menu" />
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Menu </th>
                            </tr>
                        </thead>
                        <tbody  id="accesomenus">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header row" >
                <div>
                    <h3 class="card-title">Accesos Asignados</h3>
                </div>
                <div class="accion">
                    <?php
                    $permisos = session('permiso')['permiso'];
                    $html = '';
                    if ($permisos->isdelete == 1) {
                        $html .= '<input id="primero"  class="btn btn-danger"  type="button" onclick="Quitar()"   value="Quitar Menu" />';
                    }
//                    if ($permisos->isdelete == 1) {
                        $html .= '<button class="btn  btn-success" method="post" action="' . route('cambio') . '"  type="button" onclick="CambioAjax(this)"  >Actualizar los Accesos</button>';
//                    }

                    echo $html;
                    ?>

                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form class="formdata">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 50px"></th>
                                <th>Grupo</th>
                                <th>Menu</th>
                                <th>Crear</th>
                                <th>Actulizar</th>
                                <th>Eliminar</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody   id="habilitados">

                        </tbody>
                    </table>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <script>
        function UnidAjax(event) {
            var formData = 'roles=' + $('#roles').val();
            CargarMenu(formData);
            CargarHabil(formData);
        }
        function CargarMenu(from) {
            $('#accesomenus').append('');
            $.ajax({
                url: '{{route("setmenu")}}',
                type: 'post',
                headers: {'X-CSRF-TOKEN': csrf_token},
                data: from,
                success: function (respuesta) {
                    $('#accesomenus').html(respuesta);

                }
            });
        }
        function CargarHabil(from) {
            $('#habilitados').append('');
            $.ajax({
                url: '{{route("habilitado")}}',
                headers: {'X-CSRF-TOKEN': csrf_token},
                type: 'post',
                data: from,
                success: function (respuesta) {
                    $('#habilitados').html(respuesta);
                }
            });
        }


        function Asignar() {
            var rol = 'roles=' + $('select[name=roles]').val();
            var grupo = 'grupo=' + $('select[name=grupo]').val();
            var menu = 'menu=' + $('input:radio[name=menu]:checked').val();
            var check1 = false;
            var check2 = false;
            if ($('input:radio[name=menu]:checked').val() != undefined) {
                var check1 = true;
            } else {
                alert('Marqué El Menu');
            }
            if ($('select[name=grupo]').val() != '0') {
                var check2 = true;
            } else {
                alert('Seleccicone El grupo de Menu');
            }

            if (check1 == true && check2 == true) {
                $.ajax({
                    url: '{{route("asignar")}}',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    type: 'post',
                    dataType: 'json',
                    data: rol + '&' + grupo + '&' + menu,
                    success: function (data) {

                        if (data.estado == false) {
                            toastr.error(data.mensaje);
                        }
                        if (data.estado == true) {
                            toastr.success(data.mensaje);
                            CargarMenu(rol);
                            CargarHabil(rol);
                        }
                    }
                });
            }
        }
        function Quitar() {
            var rol = 'roles=' + $('select[name=roles]').val();
            var menu = 'permiso=' + $('input:radio[name=permiso]:checked').val();
            var check1 = false;
            if ($('input:radio[name=permiso]:checked').val() != undefined) {
                var check1 = true;
            } else {
                alert('Marqué El Menu Ya Asignadas');
            }
            if (check1 == true) {
                $.ajax({
                    url: '{{route("remover")}}',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    type: 'post',
                    data: menu,
                    success: function (respuesta) {
                        toastr.success('Permiso Retirado Exitosamente');
                        CargarMenu(rol);
                        CargarHabil(rol);
                    }
                });
            }
        }

        function CambioAjax(event) {
            var rol = 'roles=' + $('select[name=roles]').val();
            var formData = $('.formdata').serialize();
            console.log($(event).attr("action"));
            $.ajax({
                url: $(event).attr("action"),
                dataType: 'json',
                type: $(event).attr("method"),
                headers: {'X-CSRF-TOKEN': csrf_token},
                data: formData,
                success: function (data) {
                    if (data.estado == false) {
                        toastr.error('Error internal');
                    }
                    if (data.estado == true) {
                        toastr.success('Se Actualizo Satisfactoriamente');
                        CargarMenu(rol);
                        CargarHabil(rol);
                    }
                }, error: function (error) {
                    alert("Error internal")
                }
            });
        }


        (function (extend) {
            'use strict';
            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {

                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/cliente' }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
            extend.print = function (event) {
                var srcurl = base_url + 'Venta/NotaVenta' + '?id=' + event;
                let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprintRerp').html(h);

            };
            window.cliente = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);
    </script>
</div>
@endsection
