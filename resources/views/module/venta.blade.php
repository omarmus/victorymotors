@extends('layouts.main')
@section('content')


<script>
    var url_produto = "{{ route('venta.producto') }}";
    var url_delete = "{{ route('venta.destroy') }}";
</script>


<!-- modal interfaz agregar producto -->
<div class="modal fade" id="modal-producto">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="CargarProducto" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"  data-id="#modal-producto"  onclick="venta.close(this)" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <input type="text" class="form-control producto" id="productoBuscar"  name="productoBuscar" placeholder="Buscar Producto">
                        </div>
                        <div class="col-sm-4">
                            <strong class="codigo"></strong>
                        </div>
                    </div>
                    <hr>
                    <input type="hidden" class="form-control producto" id="id_producto" placeholder="busca producto">
                    <div class="row">
                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label for="venta">Categoria</label>
                                <input type="text" class="form-control" name="fabrica"id="fabrica" placeholder="categoria">
                            </div>
                        </div> -->

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="venta">Cantidad</label>
                                <input type="text" class="form-control" name="cantidad" id="cantidad" placeholder="cantidad">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="venta">Precio</label>
                                <input type="text" class="form-control"  name="precio" id="precio" placeholder="Precio">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="venta">Stock: <strong class="StockVal">00</strong></label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group text-right">
                                <button  type="button" id="agregarid"  data-id="0"   onclick="venta.agregarProducto(this)"class="btn btn-primary">Agregar Producto</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- cierre del modal agregar producto -->


<!-- modal para realizar nueva venta -->
<div class="modal fade" id="modal-venta">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form  id="ajax_form" method="post" action="javascript:void(0)">
                <input name="id" type="hidden"  id="id" >
                <div class="modal-header">
                    <h4 class="modal-title titulo-modal-venta">Nueva Venta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-venta"  onclick="venta.close(this)" >
                        <span aria-hidden="true"  >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select class="form-control chosen-select" id="chansecliente"  name="cliente" style="width: 100% !important;">
                                        <option value="0">Selecciona cliente..</option>
                                        <?php foreach ($cliente as $value) { ?>
                                            <option value="<?= $value->id ?>"><?= $value->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="venta">Fecha de Venta</label>
                                    <input name="fecha" type="text" class="form-control " id="datepicker"  placeholder="fecha venta">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="codigo">Codigo</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="codigo" class="form-control" id="codigo" placeholder="Ingrese el codigo">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <select class="form-control chosen-select" id="avance"  name="avance" style="width: 100% !important;">
                                        <option value="0">Selecciona Estado..</option>
                                        <option value="pendiente">Pendiente</option>
                                        <option value="cencelada" >Cencelada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tipo de Pago</label>
                                    <select class="form-control chosen-select" id="tipo_pago"  name="tipo_pago" style="width: 100% !important;">
                                        <option value="0">Selecciona tipo de pago ..</option>
                                        <option value="efectivo">Efectivo</option>
                                        <option value="cheque">Cheque</option>
                                        <option value="credito">Crédito</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="nro_cuotas_container">
                                <div class="form-group">
                                    <label for="codigo">Número de cuotas</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="nro_cuotas" class="form-control" id="nro_cuotas" placeholder="Ingrese el número de cuotas" maxlength="2">
                                </div>
                            </div>
                            <input type="hidden" name="cuotas" value="" id="cuotas">
                        </div>
                        <div class="row">
                            <div class="table-responsive" id="container_tables">
                                <h4>Productos:</h4>
                                <button type="button" style="margin-bottom: 10px" data-id="#modal-producto"
                                        onclick="venta.getProducto(this)"class="btn btn-primary buttonProducto btn-sm">Agregar producto</button>
                                <table class="table table-condensed" id="getProductoCard" style="margin-bottom: 0rem;">
                                    <thead>
                                        <tr>
                                            <th style="width: 40px">Acción</th>
                                            <th style="width: 100px">No. Póliza</th>
                                            <th style="width: 110px">Cant.</th>
                                            <th>Descripcion</th>
                                            <th style="width: 100px">Descuento.</th>
                                            <th style="width: 100px">Precio Unit.</th>
                                            <th style="width: 120px">Precio Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cargarDetalle"></tbody>
                                    <tfoot id="costoTotal"></tfoot>
                                </table>
                            </div>
                            <div>
                                <h4>Cuotas:</h4>
                                <div class="cuotas-container">
                                    <table class="table table-condensed" id="cuotasTabla">
                                        <thead>
                                            <tr>
                                                <th># Cuota</th>
                                                <th>Total</th>
                                                <th>Pagado</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cuotasRegistros"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" data-id="#modal-venta"  onclick="venta.close(this)"  class="btn btn-default" data-dismiss="modal">cerrar</button>
                    <button type="submit"  class="btn btn-primary buttonGuardar">Guardar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-info">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form  id="ajax_form" method="post" action="javascript:void(0)">
                <input name="id" type="hidden"  id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Venta Cerrada</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-info"  onclick="venta.close(this)" >
                        <span aria-hidden="true"  >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select class="form-control chosen-select" id="chansecliente" disabled  name="cliente" style="width: 100% !important;">
                                        <option value="0">Selecciona cliente..</option>
                                        <?php foreach ($cliente as $value) { ?>
                                            <option value="<?= $value->id ?>"><?= $value->nombre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="venta">Fecha de Venta</label>
                                    <input name="fecha" type="text" class="form-control " disabled id="datepicker"  placeholder="fecha venta">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="codigo">Codigo</label>
                                    <input type="text"  name="codigo" class="form-control" disabled id="codigo" placeholder="Codigo">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <select class="form-control chosen-select" id="avance"  disabled name="avance" style="width: 100% !important;">
                                        <option value="0">Selecciona Estado..</option>
                                        <option value="pendiente">Pendiente</option>
                                        <option value="cerrado">Cerrado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tipo de Pago</label>
                                    <select class="form-control chosen-select" id="tipo_pago2" disabled name="tipo_pago" style="width: 100% !important;">
                                        <option value="0">Selecciona tipo de pago ..</option>
                                        <option value="efectivo">Efectivo</option>
                                        <option value="cheque">Cheque</option>
                                        <option value="credito">Crédito</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 hide" id="container_nro_cuotas2">
                                <div class="form-group">
                                    <label for="codigo">Número de cuotas</label>
                                    <input type="text" disabled class="form-control" name="nro_cuotas" id="nro_cuotas2">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="table-responsive" id="col-productos">
                                <h4>Productos:</h4>
                                <table class="table table-condensed" id="" style="margin-bottom: 0rem;">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px">No. Póliza</th>
                                            <th style="width: 110px">Cant.</th>
                                            <th>Descripcion</th>
                                            <th style="width: 100px">Precio Unit.</th>
                                            <th style="width: 100px">Precio Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cargarDetalle-info">
                                    </tbody>
                                    <tfoot id="costoTotal-info">
                                    </tfoot>
                                </table>
                            </div>
                            <div id="col-cuotas">
                                <h4>Cuotas:</h4>
                                <div class="cuotas-container">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th># Cuota</th>
                                                <th>Total</th>
                                                <th>Pagado</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cuotasRegistros2"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-imprimir">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form  id="ajax_form" method="post" action="javascript:void(0)">
                <input name="id" type="hidden"  id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva venta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="venta.close(this)" >
                        <span aria-hidden="true"  >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="content" content-modal="content" id="htmpprint"></div>
                </div>
        </div>
    </div>
</div>

<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Ventas</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['venta'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary"  onclick="venta.new()" >Realizar Nueva Venta</button>';
            }
            echo $html;
            ?>
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Codigo</th>
                                <th>Nombre Cliente</th>
                                <th>Fecha venta</th>
                                <th>Total</th>
                                <th style="width: 100px">Avance</th>
                                <th style="width: 100px">tipo Pago</th>
                                <th>Vendedor</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <script>
        $('.chosen-select').chosen({allow_single_deselect: true, width: '95%', no_results_text: "Oops, no disponible"});

        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",

            "ajax": {
                "url": "{{ route('venta.list') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'venta'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "codigo"},
                {"data": "text_cliente"},
                {"data": "fecha"},
                {"data": "total"},
                {"data": "avance"},
                {"data": "tipo_pago"},
                {"data": "text_users"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, 5, 6, -1]
                }
            ],
        });

        setInterval(function () {
            table.ajax.reload();
        }, 30000);

        var cal = null;

        if ($("#ajax_form").length > 0) {
            $("#ajax_form").validate({
                rules: {
                    cliente: {required: true},
                    fecha: {required: true},
                    codigo: {required: true},
                    getproducto: {required: true}
                },
                messages: {
                    cliente: {required: "Es Requerido el cliente", },
                    fecha: {required: "Es Requerido el fecha", },
                    codigo: {required: "Es Requerido el uso", },
                    getproducto: {required: "No tiene ningun producto selecionado"},
                },
                submitHandler: function (form) {
                    var cuotas = [];
                    $('#cuotasRegistros tr').each(function (index, item) {
                        var cells = $(item).find('td');
                        cuotas.push({
                            id: cells[0].innerHTML,
                            total: cells[1].innerHTML,
                            pagado: $(cells[2]).find('input')[0].checked
                        });
                    });
                    $('#cuotas').val(JSON.stringify(cuotas));
                    $('#send_form').html('Sending..');
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                        url: "{{ route('venta.crear') }}",
                        type: "POST",
                        data: $('#ajax_form').serialize(),
                        dataType: "json",
                        success: function (response) {
                            console.log(response);
                            table.ajax.reload();
                            $("#modal-venta").attr('aria-modal', "true").addClass('show').css('z-index', '0').hide();
                            response.estado ? toastr.success(response.data) : toastr.error(response.data);
                            $('#ajax_form').trigger("reset");
                        }
                    });
                }
            })
        }

//        2019-01-01
        $('#datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true,
            language: 'es'
        });

        $( document ).ready(function() {
            // Eventos de las cuotas
            var $table = $('#container_tables');
            $('#tipo_pago').on('change', function () {
                $('#nro_cuotas_container').css('display', this.value === 'credito' ? 'block' : 'none');
                $table.next()
                      .css('display', this.value === 'credito' ? 'block' : 'none')
                      .removeClass('col-sm-4')
                      .toggleClass(this.value === 'credito' ? 'col-sm-4' : '');
                $table.removeClass('col-sm-8')
                      .removeClass('col-sm-12')
                      .toggleClass(this.value === 'credito' ? 'col-sm-8' : 'col-sm-12');
            });
            $('#nro_cuotas').on('keyup', function () {
                renderCuotas();
            });
            $('#nro_cuotas_container').css('display', 'none');
            $table.next().css('display', 'none');
        });

        function renderCuotas (cuotas, id) {
            var total = 0;
            var nroCuotas = parseInt($('#nro_cuotas').val());
            if ($('#total-productos').length) {
                total = parseInt($('#total-productos').data('total'));
            }
            if (total) {
                total = total / nroCuotas;
                var html = [];
                if (cuotas) {
                    for (let i in cuotas) {
                        html.push('<tr>' + '<td>' + cuotas[i].id + '</td><td>' + cuotas[i].total + '</td><td class="text-center"><input type="checkbox" ' + (cuotas[i].pagado ? 'checked' : '') + '></td></tr>');
                    }
                } else {
                    for (var i = 0; i < nroCuotas; i++) {
                        html.push('<tr>' + '<td>' + (i + 1) + '</td><td>' + total.toFixed(2) + '</td><td class="text-center"><input type="checkbox" disabled></td></tr>');
                    }

                }
                $('#cuotasRegistros').html(html.join(''));
            } else {
                $('#nro_cuotas').val('');
                $('#cuotasRegistros').html('<tr><td colspan="3"><div class="alert alert-warning">Debe agregar por lo menos un producto para generar las cuotas.</div></td></tr>');
            }
        }
    </script>
</div>
<script src="{{ asset('plugins') }}/builder/venta.js" type="text/javascript"></script>
<script>
    venta.imprimir = function (event) {
        console.log(event);
        let h = '<iframe  frameborder="0" src="reporte/' + $(event).data('id') + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
        $('#htmpprint').html(h);

        $(".modal-title").text($(event).data('title'));
        $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();

    };
    venta.aprobar = function (event) {
        swal({
            title: "Esta seguro de aprobar?",
            text: "Una vez aprobada ya no podra modificar mas...!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Si, Aprobar!",
            cancelButtonText: "No, Cancelar!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: "{{ route('venta.aprobar') }}",
                    data: {id: $(event).data('id'), "_token": csrf_token},
                    success: function (data) {
                        swal("satisfactoria!", data.mensaje, "success");
                        table.ajax.reload();
                        $('#cuotasRegistros').html('');
                        $('#costoTotal').html('');
                    }
                });
            } else {
                swal("Cancelled", "Se Cancela el accion", "error");
            }
        });
    };
</script>
<style>
.cuotas-container {
    max-height: 250px;
    overflow-y: auto;
}
</style>
@endsection
