@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Usuario</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                                <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Persona</label>
                                    <select class="form-control  chosen-select"  name="id_persona"  id="id_persona" required>
                                        <option value="0">Selecciona Persona..</option>
                                        <?php
                                        foreach (options('persona') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_persona ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefono">Rol</label>
                                    <select class="form-control chosen-select"  name="id_rol"  id="id_rol" required>
                                        <option value="0">Selecciona Rol..</option>
                                        <?php
                                        foreach (options('rol') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_rol ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre usuario</label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="usuario" id="usuario" >
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input type="email" class="form-control" name="email" id="email" >
                                </div>
                            </div>
                        </div>
                
                        <div class="row" id="ocultarPass">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Contraseña</label>
                                    <input type="password" class="form-control" name="password" id="password" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="users.save(this)" class="btn btn-primary">Agregar Usuario</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-reset">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarDataPass" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" class="passid" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Usuario</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-reset')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <span>Usuario : <strong id="nombrerest"></strong></span>
                        <hr>
                        <div class="row" id="ocultarPass">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="reset_password">Cambiar Contraseña</label>
                                    <input type="password" class="form-control" name="reset_password" id="reset_password" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarDataPass"  
                                             onclick="users.resetpass(this)" class="btn btn-primary">Actualizar Contraseña</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Usuario</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
              <?php
            $permisos = session('permiso')['users'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Usuario"   onclick="users.new(this)" > Nueva Usuario</button>';
            }
            echo $html;
            ?>
            
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Usuario</th>
                                <th>Persona</th>
                                <th>Correo</th>
                                <th>Categoria</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <script>

        $('.chosen-select').chosen({allow_single_deselect: true, width: '95%', no_results_text: "Oops, no disponible"});

        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/users' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'cliente'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "usuario"},
                {"data": "text_persona"},
                {"data": "email"},
                {"data": "text_rol"},
                {"data": "estado"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });


        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        data.estado ? toastr.success(data.mensaje) : toastr.error(data.mensaje);

                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['usuario', 'id_persona', 'email','id_rol']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ route('users.create') }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }



            extend.new = function (event) {
                $("#ocultarPass").show();
                $("[name='password']").attr('disabled', false)
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };

            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $("#ocultarPass").hide();
                        $("[name='password']").attr('disabled', true);

                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();

                        loaderFrom('#CargarData', data.data);
                        $("#id_persona option[value=" + data.data.id_persona + "]").attr("selected", true).trigger("chosen:updated");
                        $("#id_rol option[value=" + data.data.id_rol + "]").attr("selected", true).trigger("chosen:updated");


                    }
                });
            };

            extend.reset = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $("#nombrerest").text(data.data.usuario);
                        $(".passid").val(data.data.id);
                        $('#modal-reset').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                    }
                });
            };
            extend.resetpass = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ route('users.pass') }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-reset');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarDataPass').trigger("reset");
                                $(".passid").val('');
                            }
                        });
                    }
                });
            }
            extend.print = function (event) {
                var srcurl = base_url + 'Venta/NotaVenta' + '?id=' + event;
                let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprintRerp').html(h);

            };
            window.users = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);

        function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }
    </script>
</div>
@endsection
