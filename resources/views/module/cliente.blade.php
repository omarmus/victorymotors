@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar NuevoCliente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre completo cliente</label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombre" id="nombre">
                                    

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input type="email" class="form-control" name="correo" id="correo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono/Cel</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control"  name="telefono" id="telefono" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Direccion</label>
                                    <input type="text" class="form-control"  name="direccion" id="direccion" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="cliente.save(this)" class="btn btn-primary">Agregar Cliente</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Listado de Cliente</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['cliente'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Cliente"   onclick="cliente.new(this)" > Nuevo Cliente</button>';
                $html .= ' <button type="button" class="btn  btn-success" data-title="Imprimir"   onclick="cliente.imprimir(this)" >'
                    . '<i class="fa fa-print" aria-hidden="true"></i> Imprimir Cliente</button>';
            }
            echo $html;
            ?>
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Nombre del Cliente</th>
                                <th>Telfeno</th>
                                <th>correo</th>
                                <th>direccion</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
               <div class="modal fade" id="modal-imprimir">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <form  id="ajax_form" method="post" action="javascript:void(0)"> 
                    <input name="id" type="hidden"  id="id" >
                    <div class="modal-header">
                        <h4 class="modal-title">Lista de Clientes</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="closeModal('#modal-imprimir')" >
                            <span aria-hidden="true"  >&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" content-modal="content" id="htmpprint"></div>
                    </div>
        </div>
    </div>
    <!-- /.col -->
    <script>
//        $(function () {
        //            $("#example1").DataTable();
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/cliente' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'cliente'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "nombre"},
                {"data": "telefono"},
                {"data": "correo"},
                {"data": "direccion"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });


        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        data.estado ? toastr.success(data.mensaje) : toastr.error(data.mensaje);

                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo', 'direccion']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/cliente' }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
           extend.imprimir = function (event) {
       
                let h = '<iframe  frameborder="0" src="printClientes" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprint').html(h);
                $(".modal-title").text($(event).data('title'));
                $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            window.cliente = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);   

        function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }
    </script>
</div>
@endsection
