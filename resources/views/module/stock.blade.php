@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nuevo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombre producto </label>
                                    <select class="form-control chosen-select"  name="id_producto"  id="id_producto" style="width: 100% !important;">
                                        <option value="0">Selecciona producto..</option>
                                        <?php
                                        foreach (_options()->almacen as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_producto ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="precio">Precio </label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control" class="form-control" name="precio" id="precio" >

                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="stock">Stock</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control" class="form-control" name="stock" id="stock" >
                                    <input type="text" class="form-control" style="display: none" disabled="" name="aumentar" id="aumentar" >
                                </div>
                            </div>

                        </div>
                        <div class="row AumenteRow" style="display: none">
                            <div class="col-sm-12">
                                <span >Stock:<strong style="padding: 3px 12px; background: #212020; color: #8bff21; margin-left: 5px;border-radius: 40px;font-size: 22px;" class="StockAumentar"></strong></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="inventario.save(this)" class="btn btn-primary">Agregar Stock</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Stock</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['inventario'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Stock"   onclick="inventario.new(this)" > Nueva Stock</button>';
            }
//            if ($permisos->isreporte == 1) {
                $html .= ' <button type="button" class="btn  btn-success" data-title="Imprimir"   onclick="inventario.imprimir(this)" ><i class="fa fa-print" aria-hidden="true"></i> Imprimir Stock</button>';
//            }
            echo $html;
            ?>
        </div>
    </ul>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Nombre Producto</th>
                                <th>Precio</th>
                                <th>stock</th>
                                <th style="width: 150px">Estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="modal fade" id="modal-imprimir">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <form  id="ajax_form" method="post" action="javascript:void(0)"> 
                    <input name="id" type="hidden"  id="id" >
                    <div class="modal-header">
                        <h4 class="modal-title">Lista De Stock Actual</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="closeModal('#modal-imprimir')" >
                            <span aria-hidden="true"  >&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content" content-modal="content" id="htmpprint"></div>
                    </div>
            </div>
        </div>
    </div>
    <script>
//        $(function () {
        //            $("#example1").DataTable();
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/inventario' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'cliente'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "text_producto"},
                {"data": "precio"},
                {"data": "stock"},
                {"data": "estado"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });



        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        console.log(data);
                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'categoria']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ route('inventario.create') }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $(".AumenteRow").hide();
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        $("[name='stock']").hide();
                        $("[name='aumentar']").attr('disabled', false).show();
                        $(".AumenteRow").show();
                        $(".StockAumentar").text(data.data.stock);
                        $("[name='id_producto']").val(data.data.id_producto);

                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
            extend.imprimir = function (event) {
                let h = '<iframe  frameborder="0" src="printInventario" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprint').html(h);
                $(".modal-title").text($(event).data('title'));
                $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };

            window.inventario = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);
    </script>
</div>
@endsection
