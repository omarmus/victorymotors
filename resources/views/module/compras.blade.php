@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva Compra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_producto">Nombre completo producto</label>
                                    <select class="form-control chosen-select"  name="id_producto" style="width: 100% !important;">
                                        <option value="0">Selecciona producto..</option>
                                        <?php
                                        foreach (options('producto') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_producto ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="id_proveedor">Nombre completo Proveedor</label>
                                    <select class="form-control chosen-select"  name="id_proveedor" style="width: 100% !important;">
                                        <option value="0">Selecciona proveedor..</option>
                                        <?php
                                        foreach (options('proveedor') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_proveedor ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="datepicker">Fecha</label>
                                    <input type="text" class="form-control"  name="fecha" id="datepicker" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Stock</label>
                                    <input type="text" class="form-control"  name="stock" id="direccion" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="precio">Precio</label>
                                    <input type="text" class="form-control"  name="precio" id="precio" >
                                </div>
                            </div>
                            <input type="hidden"   name="id_users" id="id_users" value="{{ Auth::user()->id }}" >

                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="compras.save(this)" class="btn btn-primary">Agregar Producto</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Compras</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
            <?php
            $permisos = session('permiso')['compras'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo compras"   onclick="compras.new(this)" > Nueva Compras</button>';
            }
            $html .= ' <button type="button" class="btn  btn-success" data-title="Imprimir"   onclick="compras.imprimir(this)" >'
                    . '<i class="fa fa-print" aria-hidden="true"></i> Imprimer Compras</button>';

            echo $html;
            ?>
        </div>
    </ul>
</div>
<div class="modal fade" id="modal-imprimir">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form  id="ajax_form" method="post" action="javascript:void(0)"> 
                <input name="id" type="hidden"  id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Lista De Compras Actual</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="closeModal('#modal-imprimir')" >
                        <span aria-hidden="true"  >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="content" content-modal="content" id="htmpprint"></div>
                </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Nombre producto</th>
                                <th>Proveedor</th>
                                <th>Fecha</th>
                                <th>Stock</th>
                                <th>Usuario</th>
                                <th>Precio  </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <!--protected $fillable = ['id_producto', 'id_proveedor', 'fecha', 'stock', 'id_usuario', 'precio',  'estado'];-->
    <script>
   $('#datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true,
            language: 'es'
        });
        
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",

            "ajax": {
                "url": "{{ 'datatable/compras' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'compras'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "text_producto"},
                {"data": "text_proveedor"},
                {"data": "fecha"},
                {"data": "stock"},
                {"data": "text_users"},
                {"data": "precio"}
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });

        (function (extend) {
            'use strict';

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ route('compras.create') }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };

            extend.imprimir = function (event) {
                let h = '<iframe  frameborder="0" src="printCompras" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprint').html(h);
                $(".modal-title").text($(event).data('title'));
                $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };

            window.compras = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);

    </script>
</div>
@endsection
