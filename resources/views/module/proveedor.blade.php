@extends('layouts.main')
@section('content')
<style>
    .accion{
        justify-content: space-evenly;
        float: right;
        right: 22px;
        position: absolute;
        margin-top: -8px;
    }
</style>


<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nuevo Proveedor</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Proveedor </label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" >
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="sitio_web">Sitio web</label>
                                    <input type="text" class="form-control" name="sitio_web" id="paterno" >
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="text" class="form-control" name="telefono" id="telefono" >
                                </div>
                            </div>
                        </div>
                    <fieldset>
                    
                        <legend>
                            <b><h4>Datos Representate</h4></b>
                        </legend>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="rl_nombre">Nombres</label>
                                    <input type="text" class="form-control"  name="rl_nombre" id="rl_nombre" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="rl_apellidos">Apellidos</label>
                                    <input type="text" class="form-control"  name="rl_apellidos" id="rl_apellidos" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="rl_corrreo">Correo Electronico</label>
                                    <input type="text" class="form-control"  name="rl_corrreo" id="rl_corrreo" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="rl_telefono">Telefono</label>
                                    <input type="number" class="form-control"  name="rl_telefono" id="rl_telefono" >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Dirección</label>
                                    <input type="text" class="form-control"  name="direccion" id="direccion" >
                                </div>
                            </div>
                        </div>
                    </fieldset>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="proveedor.save(this)" class="btn btn-primary">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Proveedor</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
               <?php
            $permisos = session('permiso')['proveedor'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Proveedor"   onclick="proveedor.new(this)" > Nueva Proveedor</button>';
            }
            echo $html;
            ?>
        </div>
    </ul>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-body">
                <div class="table-responsive">
                    <table id="Gridtables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style="width: 5px"></th>
                                <th>Proveedor</th>
                                <th>Siteo Web</th>
                                <th>Nombres </th>
                                <th>Apellidos  </th>
                                <th>Correo  </th>
                                <th>Telefono  </th>
                                <th>Direccion  </th>
                                <th>estado</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <!--protected $fillable = ['nombre', 'sitio_web', 'rl_nombre', 'rl_apellidos',  'rl_corrreo',  'rl_telefono',  'direccion',  'estado'];-->

    <script>

        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",

            "ajax": {
                "url": "{{ 'datatable/proveedor' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'proveedor'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "nombre"},
                {"data": "sitio_web"},
                {"data": "rl_nombre"},
                {"data": "rl_apellidos"},
                {"data": "rl_corrreo"},
                {"data": "rl_telefono"},
                {"data": "direccion"},
                {"data": "estado"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });

        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        table.ajax.reload();
                        data.estado ? toastr.success(data.mensaje) : toastr.error(data.mensaje);

                    }
                });
            };

            extend.save = function (event) {
                var data = validateFrom(['nombre', 'telefono', 'correo']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/proveedor' }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
            extend.print = function (event) {
                var srcurl = base_url + 'Venta/NotaVenta' + '?id=' + event;
                let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprintRerp').html(h);

            };
            window.proveedor = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);

    </script>
</div>
@endsection
