@extends('layouts.main')
@section('content')

<div class="modal fade" id="modal-content">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <form id="CargarData" method="post"  action="javascript:void(0)">
                <input type="hidden" name="id" id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nueva venta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span    onclick="closeModal('#modal-content')" >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nombre">Nombres </label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombre" id="nombre" >
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="paterno">AP_Paterno</label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="paterno" id="paterno" >
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="materno">AP_Materno</label>
                                    <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="materno" id="materno" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="ci">Cédula de Identidad</label>
                                    <input type="text" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" class="form-control"  name="ci" id="ci" >
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="direccion">Expedido</label>
                                    <select name="id_expedido" id="id_expedido"  class="form-control">
                                        <option value="0">Selecone Expedido...</option>
                                        <?php
                                        foreach (options('expedido') as $value) {
                                            ?>
                                            <option value="<?= $value->id ?>"><?= $value->text_expedido ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <div class="form-group justify-content-between">
                                    <button  type="submit" data-id="#CargarData"  
                                             onclick="persona.save(this)" class="btn btn-primary">Agregar Persona</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="app-title">
    <h1><i class="fa fa-th-list"></i> Lista de Persona</h1>
    <ul class="app-breadcrumb breadcrumb side">
        <div class="accion">
              <?php
            $permisos = session('permiso')['persona'];
            $html = '';
            if ($permisos->isnew == 1) {
                $html .= '<button type="button" class="btn  btn-primary" data-title="Registrar Nuevo Persona"   onclick="persona.new(this)" > Nueva Persona</button>';
            }
            echo $html;
            ?>
        </div>
    </ul>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                <table id="Gridtables" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 5px"></th>
                            <th>Nombre Persona</th>
                            <th>Apellido Paterno</th>
                            <th>Apellido Materno</th>
                            <th>Cédula de Identidad</th>
                            <th>Expedido</th>
                        </tr>
                    </thead>
                </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
    <script>
//        $(function () {
        //            $("#example1").DataTable();
        var table = $('#Gridtables').DataTable({
            "processing": true,
            "serverSide": true,
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sSearch": "Buscar:",
            "ajax": {
                "url": "{{ 'datatable/persona' }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}",
                    route: 'cliente'
                }
            },
            "columns": [
                {"data": "action"},
                {"data": "nombre"},
                {"data": "paterno"},
                {"data": "materno"},
                {"data": "ci"},
                {"data": "text_expedido"},
            ],
            order: [['1', 'asc']],
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0, -1]
                }
            ],
        });


        (function (extend) {
            'use strict';

            extend.destroy = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (response) {
                        table.ajax.reload();
                        console.log(data); 
                        response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                    }
                });
            };


           extend.save = function (event) {
                var data = validateFrom(['nombre', 'paterno', 'materno', 'ci']);
                $($(event).data('id')).validate({
                    rules: data.rules,
                    messages: data.mensaje,
                    submitHandler: function (form) {
                        $.ajax({
                            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                            url: "{{ 'create/persona' }}",
                            type: "POST",
                            data: $($(event).data('id')).serialize(),
                            dataType: "json",
                            success: function (response) {
                                closeModal('#modal-content');
                                 table.ajax.reload();
                                response.estado ? toastr.success(response.mensaje) : toastr.error(response.mensaje);
                                $('#CargarData').trigger("reset");
                            }
                        });
                    }
                });
            }

            extend.new = function (event) {
                $("[name='id']").val('');
                $('#CargarData').trigger("reset");
                $(".modal-title").text($(event).data('title'));
                $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
            };
            extend.editar = function (event) {
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': csrf_token},
                    url: $(event).data('route'),
                    data: {id: $(event).data('id')},
                    success: function (data) {
                        $(".modal-title").text($(event).data('title'));
                        $('#modal-content').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
                        loaderFrom('#CargarData', data.data);
                    }
                });
            };
            extend.print = function (event) {
                var srcurl = base_url + 'Venta/NotaVenta' + '?id=' + event;
                let h = '<iframe  frameborder="0" src="' + srcurl + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
                $('#htmpprintRerp').html(h);

            };
            window.persona = typeof define === 'function' && define.amd ? define(extend) : extend;
        })(Object);

        function soloLetras(e) {
    var key = e.keyCode || e.which,
      tecla = String.fromCharCode(key).toLowerCase(),
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
      especiales = [8, 37, 39, 46],
      tecla_especial = false;

    for (var i in especiales) {
      if (key == especiales[i]) {
        tecla_especial = true;
        break;
      }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
      return false;
    }
  }
    </script>
</div>
@endsection
