@extends('layouts.main')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="CargarData" method="post"  action="javascript:void(0)">
                    <div class="modal-header">
                        <h4 class="modal-title">Reportes Generales</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label> <input type="radio"  name="radioName" value="venta" /> Ventas</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">

                                        <label><input type="radio" name="radioName" value="almacen" /> Almacen</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">

                                        <label><input type="radio"  name="radioName" value="stock" /> Stock</label>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-2">
                                    <div class="form-group">

                                        <label><input type="radio" name="radioName" value="compras" /> Compras</label>
                                    </div>
                                </div> -->

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="venta">Fecha Desde</label>
                                        <input name="desde" type="text" class="form-control datepicker" id="desde"  placeholder="fecha desde">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="venta">Fecha Hasta</label>
                                        <input name="hasta" type="text" class="form-control datepicker" id="hasta"  placeholder="fecha hasta">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <div class="form-group justify-content-between">
                                        <button  type="submit" data-id="#CargarData"   data-route="<?= route('reporte.all') ?>"
                                                 onclick="generar(this)" class="btn btn-primary">Generar Reporte</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="modal-imprimir">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form  id="ajax_form" method="post" action="javascript:void(0)"> 
                <input name="id" type="hidden"  id="id" >
                <div class="modal-header">
                    <h4 class="modal-title">Visualización de Reporte</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-id="#modal-imprimir"  onclick="closeModal('#modal-imprimir')" >
                        <span aria-hidden="true"  >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="content" content-modal="content" id="htmpprint"></div>
                </div>
        </div>
    </div>
</div>

<script>

    $('#desde').attr('disabled', true);
    $('#hasta').attr('disabled', true);

    $("[name='radioName']").change(function (e) {
        if ($(this).val() == 'venta') {
            $('#desde').attr('disabled', false);
            $('#hasta').attr('disabled', false);
        } else {
            $('#desde').attr('disabled', true);
            $('#hasta').attr('disabled', true);
        }
    });

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        language: 'es'
    });

    function generar(event) {
        var data = $($(event).data('id')).serialize();
        if (data != '') {
            let h = '<iframe  frameborder="0" src="printAll?' + data + '" scrolling="no" id="idframifrma"  style=" width: 100%; height: 400px" ></iframe>';
            $('#htmpprint').html(h);
            $(".modal-title").text($(event).data('title'));
            $('#modal-imprimir').attr('aria-modal', "true").addClass('show').css('z-index', '9991').show();
        } else {
            alert("Seleccione un tipo de Reporte");
        }

    }
    function imprimir(event) {

    }

</script>
<!--</div>-->
@endsection
