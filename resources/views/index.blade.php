<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Victory Motors Bolivia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="icon" type="image/png" href="favicon.ico"> -->
         <!-- favicons
   		 ================================================== -->
    	<link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">
    	<link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">

        <!--Google Font link-->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


        <link rel="stylesheet" href="{{url('')}}/assets/css/slick/slick.css"> 
        <link rel="stylesheet" href="{{url('')}}/assets/css/slick/slick-theme.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/animate.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/iconfont.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/bootstrap.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{url('')}}/assets/css/bootsnav.css">

        <!-- xsslider slider css -->
        

        <!--<link rel="stylesheet" href="assets/css/xsslider.css">-->

        <!--For Plugins external css-->
        <!--<link rel="stylesheet" href="assets/css/plugins.css" />-->

        <!--Theme custom css -->
        <link rel="stylesheet" href="{{url('')}}/assets/css/style.css">
        <!--<link rel="stylesheet" href="assets/css/colors/maron.css">-->

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{url('')}}/assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

    <body data-spy="scroll" data-target=".navbar-collapse">


        <!-- Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                </div>
            </div>
        </div><!--End off Preloader -->


        <div class="culmn">
            <!--Home page style-->


            <nav class="navbar navbar-default bootsnav navbar-fixed">
                <div class="navbar-top bg-grey fix">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="navbar-callus text-left sm-text-center">
                                    <ul class="list-inline">
                                        <li><a href=""><i class="fa fa-phone"></i> Celular: (+591) 70128283</a></li>
                                        <li><a href=""><i class="fa fa-envelope-o"></i> Contactos: victorymotors@gmail.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="navbar-socail text-right sm-text-center">
                                    <ul class="list-inline">
                                       <!--  <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href=""><i class="fa fa-behance"></i></a></li>
                                        <li><a href=""><i class="fa fa-dribbble"></i></a></li> -->
                                        <li><a href="{{ url('/login') }}">Iniciar Sesión</a></a></li>
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Start Top Search -->
                <div class="top-search">
                    <div class="container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <!-- End Top Search -->

                <div class="container"> 
                    <!-- <div class="attr-nav">
                        <ul>
                            <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>  -->

                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="#brand">
                            <img src="{{url('')}}/assets/images/logo.png" class="logo" alt="">
                            <!--<img src="assets/images/footer-logo.png" class="logo logo-scrolled" alt="">-->
                        </a>

                    </div>
                    <!-- End Header Navigation -->

                    <!-- navbar menu -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#home">Principal</a></li>                    
                            <li><a href="#features">Acerca de Nosotros</a></li>
                            <li><a href="#business">Servicios</a></li>
                            <li><a href="#product">Galeria</a></li>
                            <li><a href="#test">Blog</a></li>
                            <li><a href="#contact">Contactos</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div> 

            </nav>

            <!--Home Sections-->

            <section id="home" class="home bg-black fix">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="main_home text-center">
                            <div class="col-md-12">
                                <div class="hello_slid">
                                    <div class="slid_item">
                                        <div class="home_text ">
                                            <h1 class="text-white"><strong>Victory Motors Bolivia Es una empresa</strong></h1>
                                            <h1 class="text-white">Dedicada a</h1>
                                        </div>

                                        <div class="home_btns m-top-40">
                                            <a href="#features" class="btn btn-primary m-top-20">Visión</a>
                                            <a href="#features" class="btn btn-default m-top-20">Misión</a>
                                        </div>
                                    </div><!-- End off slid item -->
                                    <div class="slid_item">
                                        <div class="home_text ">
                                            <h1 class="text-white">Victory <strong>Motors Bolivia </strong></h1>
                                            <h2 class="text-white">Ofrecer Diferentes Marcas</h2>
                                            <!-- <h3 class="text-white">- We Create a <strong>Concept</strong> into The Market -</h3> -->
                                        </div>

                                        <div class="home_btns m-top-40">
                                            <a href="#features" class="btn btn-primary m-top-20">Visión</a>
                                            <a href="#features" class="btn btn-default m-top-20">Misión</a>
                                        </div>
                                    </div><!-- End off slid item -->
                                    <div class="slid_item">
                                        <div class="home_text ">
                                            <h2 class="text-white">Victory <strong>Motors Bolivia </strong></h2>
                                            <h1 class="text-white">A los clientes</h1>
                                            <h3 class="text-white">Vehiculos <strong>Nuevos </strong> </h3>
                                        </div>

                                        <div class="home_btns m-top-40">
                                            <a href="" class="btn btn-primary m-top-20">Visión</a>
                                            <a href="" class="btn btn-default m-top-20">Misión</a>
                                        </div>
                                    </div><!-- End off slid item -->
                                </div>
                            </div>

                        </div>


                    </div><!--End off row-->
                </div><!--End off container -->
            </section> <!--End off Home Sections-->



            <!--Featured Section-->
            <section id="features" class="features">
                <div class="container">
                    <div class="row">
                        <div class="main_features fix roomy-70"></br></br></br>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="fa fa-thumbs-o-up"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>VICTORY MOTORS BOLIVIA</h3>
                                        <p style="text-align:justify">Es una empresa dedicada al rubro de la venta y a la satisfacción a nuestros clientes ofreciendo vehículos a precios competitivos con la  efectividad en el servicio brindado. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="fa fa-tablet"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>MISIÓN</h3>
                                        <p style="text-align:justify">Ser la empresa líder en la comercialización de vehículos y prestación de servicios de ventas, brindando una buena atencion a  nuestros clientes. Ser reconocida por la calidad humana y profesional de nuestra gente, tanto por clientes.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="fa fa-sliders"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>VISIÓN</h3>
                                        <p style="text-align:justify">Ser la mejor opción del mercado en venta de vehículos y servicios posteriores.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->


            <!--Business Section-->
            <section id="business" class="business bg-info roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="main_business">
                            <div class="col-md-6">
                                <div class="business_slid">
                                    <div class="slid_shap bg-primary"></div>
                                    <div class="business_items text-center">
                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="{{url('')}}/assets/images/kinglong.png" alt="" />
                                            </div>
                                        </div>

                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="{{url('')}}/assets/images/jincheng.png" alt="" />
                                            </div>
                                        </div>

                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="{{url('')}}/assets/images/Kingstor.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase"><strong>vehículos</strong> importados</h2>
                                    <ul>
                                        <li><i class="fa fa-arrow-circle-right"></i> MINIBÚS</li>
                                        <li><i class="fa fa-arrow-circle-right"></i> Kinglong</li>
                                        <li><i class="fa fa-arrow-circle-right"></i> Foton</li></br>
                                        <li><i class="fa fa-arrow-circle-right"></i> VAGONETAS</li>
                                        <li><i class="fa fa-arrow-circle-right"></i> Change </li>
                                        <li><i class="fa fa-arrow-circle-right"></i> Keyton</li>
                                        <li><i class="fa  fa-arrow-circle-right"></i> Foton</li></br>
                                        <li><i class="fa  fa-arrow-circle-right"></i> CAMIONES</li>
                                        <li><i class="fa fa-arrow-circle-right"></i> T-king</li>
                                    </ul>
                                    <p class="m-top-20">Estos vehículos son importados con total regularidad para la conformidad del cliente.</p>

                                    <!-- <div class="business_btn">
                                        <a href="" class="btn btn-default m-top-20">Read More</a>
                                        <a href="" class="btn btn-primary m-top-20">Buy Now</a>
                                    </div>
 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->


            <!--product section-->
            <section id="product" class="product bg-black">
                <div class="container">
                    <div class="main_product roomy-80">
                        <div class="head_title text-center fix">
                            <h2 class="text-uppercase"><p style="color:#00a885";>Galeria</p></h2>
                            <h5>En esta sección podemos observar las imagenes de los vehículos</h5>
                        </div>

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a1.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="assets/images/a1.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>FOTON</p></h5>
                                                        <h6><p style="color:#999999";> Foton una colección de los minibues <br>Caracteristicas:<br>Modelo: Foton Tunland 2020.
Motor: Cummins ISF Turbodiésel 2.8 litros Common Rail, 16 válvulas.
Potencia: 161 caballos @ 3.600 rpm.<br>
Torque: 360 Nm @ 1.800 – 3.000 rpm.<br>
Transmisión: Mecánica GETRAC de 5 velocidades.<br>
Tracción: 4×4.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a2.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a2.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>KINGLONG</p></h5>
                                                        <h6><p style="color:#999999" style="text-align: justify;";> kinglong una colección de los minibues <br>Caracteristicas:<br>
                                                        Motor 4Y Tecnología Toyota.
Caja Mecánica 5 velocidades y 1 de retro.
Cilindrada 2300 CC.
Aire Acondicionado Existen dos versiones<br>
Aire solo parte delantera del conductor<br>
Capacidad 15 pasajeros.
Corona Montaña super GLS.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a3.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a3.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>KINGSTOR</p></h5>
                                                        <h6><p style="color:#999999" style="text-align: justify;";>
                                                            Nueva línea de minibuses<br> Caracteristicas:<br>
                                                        Motor 4Y Tecnología Toyota.
Caja Mecánica 5 velocidades, corona montaña.
Cilindrada 2300 CC.
Aire Acondicionado Existen dos versiones<br>
Aros de 14 y suspesion a muelles<br>
Capacidad 15 pasajeros.
Corona Montaña super GLS.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a4.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a4.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>CHANGHE</p></h5>
                                                        <h6><p style="color:#999999";> Vagoneta<br>Caracteristicas:<br>
                                                        Largo: 4,29 metros.
Ancho sin espejos: 1,81 metros.
Alto: 1,66 metros.
Distancia entre ejes: 2,56 metros.
Altura al suelo: 15 centímetros.
Capacidad Baúl: 427 litros, expandible a 940 litros.
Peso en vacío: 1.250 kilogramos.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a5.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a5.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>FOTON</p></h5>
                                                        <h6><p style="color:#999999";>Linea de minibuses<br>Caracteristicas:<br>
                                                        La caja es manual de 5 velocidades, la tracción es delantera y además cuenta con un sistema que emplea medición y control para las emisiones de gases del vehículo. Gracias a estos sistemas, la View CS2 reduce sustancialmente la contaminación. Su tanque de combustible es de 65L. Su autonomía es de 10.8 l cada 100 Km.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a6.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a6.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>KEYTON</p></h5>
                                                        <h6><p style="color:#999999";>Vagoneta en color claro<br>Caracteristicas:
                                                        sport v60 que tiene una impresionante potencia de 621 caballos de fuerza. En el CL63, un paquete de rendimiento AMG opcional aumenta la potencia del motor a 563 hp y 664 lb-ft y lo convierte en la elección para quien planee llevar este gran vehículo de lujo a las pistas, además de ser full equipado.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a7.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a7.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>KINGLONG</p></h5>
                                                        <h6><p style="color:#999999";>Colección de minibues<br>Caracteristicas:<br>
                                                         motor JM491Q-ME, con una cilindrada de 2.237L, el cual garantiza su excelente desempeño. Además, con un rango de longitud entre 4.9 y 5.2 metros de largo, este tipo de minivan para pasajeros se caracteriza por ser un vehiculo compacto y fácil de manejar. </p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a8.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a8.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>T-KING</p></h5>
                                                        <h6><p style="color:#999999";>Camineones de transporte<br>Caracteristicas:
                                                        Aire forzado.
El Incapower King Long Yuchai posee un largo total de 5.34 metros, 1.70 metros de ancho, 2.25 metros de alto y una distancia entre ejes de 2.89 metros.
Radio AM/FM+USB+SD</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a9.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a9.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>KEYTON</p></h5>
                                                        <h6><p style="color:#999999";>Vagoneta exclusiva y comoda<br>Caracteristicas:
                                                        Keyton V60 Sport ,  está equipado para 8 pasajeros, posee aros de magnesio, luces retrovisoras en los espejos laterales, frenos ABS, Molle, asientos de cuero, doble aire, corona super montaña , cilindrada de 1500 cc, y transmisión mecánica</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a10.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a10.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>FOTON</p></h5>
                                                        <h6><p style="color:#999999";>- Vagoneta foton en color plateado.<br>
                                                            Caracteristicas:<br>
                                                        Vagoneta con capacidad para 7 pasajeros, motor 1500cc.caja de cambios de 5 velocidades mas una de retro 112 HP/600rpm.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a11.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a11.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>CHANGAN</p></h5>
                                                        <h6><p style="color:#999999";>Vagoneta en nueva gama de color.<br>Caracteristicas:<br>
                                                        Con unas dimensiones de 4.360 mm de largo, 1.685 mm de ancho y 1.850 mm de alto se tiene un amplio espacio para lograr comodidad a todos los pasajeros a bordo en sus 3 filas de asientos.</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="port_item xs-m-top-30">
                                                    <div class="port_img">
                                                        <img src="{{url('')}}/assets/images/a12.png" alt="" />
                                                        <div class="port_overlay text-center">
                                                            <a href="{{url('')}}/assets/images/a12.png" class="popup-img">+</a>
                                                        </div>
                                                    </div>
                                                    <div class="port_caption m-top-20">
                                                        <h5><p style="color:#00a885";>T-KING</p></h5>
                                                        <h6><p style="color:#999999" text-aling="justify";>Camioneta en una gama diferente para transporte.<br>Caratericas: 
T-KING camión ligero con cabina.
Condición:Nuevo
Rueda motriz:4X2
Tipo de combustible:Diesel
Capacidad del motor:< 4L
Marca:Dongfeng
Color:Opcional
Modelo:N300
Modelo del motor:DongfengZD30D14-3N
Desplazamiento:2953cc
</p></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <span class="sr-only">Previous</span>
                            </a>

                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Product section -->



            <!--Test section-->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase"><p style="color:#00a885";>BLOG DE VICTORY</p></h2>
                                    <h4><p style="color:#000";>Información breve  de la empesa</p></h4>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="{{url('')}}/assets/images/b1.png" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h4>Vehiculos</h4>
                                        <h3>Nuevos</h3>

                                        <p>Los vehiculos son exclusivamente nuevos que son de importación extranjera.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="{{url('')}}/assets/images/b2.png" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h4>Cada vez</h4>
                                        <h3> Nuevos Modelos</h3>

                                        <p>A la empresa llega nuevos modelos o puedes hacer tus pedidos para tus exportaciones.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off test section -->


            <!--Brand Section-->
            <section id="brand" class="brand fix roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="main_brand text-center">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l1.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l2.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l3.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l4.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l5.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="{{url('')}}/assets/images/l6.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Brand section -->


            <!--Call to  action section-->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">Contactos</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="" class="btn btn-default">CLICK PRINCIPAL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>




            <footer id="contact" class="footer action-lage bg-black p-top-80">
                <!--<div class="action-lage"></div>-->
                <div class="container">
                    <div class="row">
                        <div class="widget_area">
                            <div class="col-md-3">
                                <div class="widget_item widget_about">
                                    <h5 class="text"><p style="color:#00a885";>Más de Nosotros</p></h5>
                                    <p class="m-top-20">Para mayor información apersonarse a la oficina.</p>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-location-arrow"></i></div>
                                        <div class="widget_ab_item_text">
                                            <h6 class="text-white">Ubicación</h6>
                                            <p>
                                                Dirección: Av. Juan Pablo II (Entre cruz papal y el TAM)</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-phone"></i></div>
                                        <div class="widget_ab_item_text">
                                            <h6 class="text-white">Teléfono :</h6>
                                            <p>+591 2444156</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-30">
                                        <div class="item_icon"><i class="fa fa-envelope-o"></i></div>
                                        <div class="widget_ab_item_text">
                                            <h6 class="text-white">Correo Electrónico :</h6>
                                            <p>jorgearamayo12@mail.com</p>
                                        </div>
                                    </div>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->

                            <div class="col-md-3">
                                <div class="widget_item widget_latest sm-m-top-50">
                                    <h5 class="text-white"><p style="color:#00a885";>Cada día creciendo como Empresa</p></h5>
                                    <div class="widget_latst_item m-top-30">
                                        <div class="item_icon"><img src="{{url('')}}/assets/images/c1.png" alt="" /></div>
                                        <div class="widget_latst_item_text">
                                            <p>Victory</p>
                                            <a href="">Simbolo de la empresa</a>
                                        </div>
                                    </div>
                                    <div class="widget_latst_item m-top-30">
                                        <div class="item_icon"><img src="{{url('')}}/assets/images/c2.png" alt="" /></div>
                                        <div class="widget_latst_item_text">
                                            <p>Motors</p>
                                            <a href="">Simbolo de la empresa</a>
                                        </div>
                                    </div>
                                    <div class="widget_latst_item m-top-30">
                                        <div class="item_icon"><img src="{{url('')}}/assets/images/c3.png" alt="" /></div>
                                        <div class="widget_latst_item_text">
                                            <p>Bolivia</p>
                                            <a href="">Simbolo de la empresa</a>
                                        </div>
                                    </div>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->

                            <div class="col-md-3">
                                <div class="widget_item widget_service sm-m-top-50">
                                    <h5 class="text-white"><p style="color:#00a885";>Plus de la Empresa</p></h5>
                                    <ul class="m-top-20">
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> Cada vez mas renovados</li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> Lideres en el Mercado</li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> Continuando hasta llegar alto</li>
                                    </ul>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->

                            <div class="col-md-3">
                                <div class="widget_item widget_newsletter sm-m-top-50">
                                    <h5 class="text-white"><p style="color:#00a885";>Redes Sociales</p></h5>
                                   <!--  <form class="form-inline m-top-30">
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter you Email">
                                            <button type="submit" class="btn text-center"><i class="fa fa-arrow-right"></i></button>
                                        </div>

                                    </form> -->
                                    <div class="widget_brand m-top-40">
                                        <a href="" class="text-uppercase">V m - bolvia</a>
                                        <p>Otras opciónes de comunicación.</p>
                                    </div>
                                    <ul class="list-inline m-top-20">
                                        <li>-  <a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href=""><i class="fa fa-behance"></i></a></li>
                                        <li><a href=""><i class="fa fa-dribbble"></i></a>  - </li>
                                    </ul>

                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                        </div>
                    </div>
                </div>
                <div class="main_footer fix bg-mega text-center p-top-40 p-bottom-30 m-top-80">
                    <div class="col-md-12">
                        <p class="wow fadeInRight" data-wow-duration="1s">
                            Elaborado 
                            <i class="fa fa-heart"></i>
                            para 
                            <a target="_blank" href="http://">VictoryMotorsBolivia.com</a> 
                            Todos los derechos son recervados
                        </p>
                    </div>
                </div>
            </footer>




        </div>

        <!-- JS includes -->

        <script src="{{url('')}}/assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>

        <script src="{{url('')}}/assets/js/owl.carousel.min.js"></script>
        <script src="{{url('')}}/assets/js/jquery.magnific-popup.js"></script>
        <script src="{{url('')}}/assets/js/jquery.easing.1.3.js"></script>
        <script src="{{url('')}}/assets/css/slick/slick.js"></script>
        <script src="{{url('')}}/assets/css/slick/slick.min.js"></script>
        <script src="{{url('')}}/assets/js/jquery.collapse.js"></script>
        <script src="{{url('')}}/assets/js/bootsnav.js"></script>



        <script src="{{url('')}}/assets/js/plugins.js"></script>
        <script src="{{url('')}}/assets/js/main.js"></script>

    </body>
</html>
