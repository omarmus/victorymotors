<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/css/main.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <title>Login</title>
        <link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon">
    </head>
    <body>
        <section class="material-half-bg">
            <div class="cover"></div>
        </section>
        <section class="login-content">
            <div class="logo">
                <h1><font face="Comic Sans MS,arial,verdana">SISTEMA DE INVENTARIO</font></h1>
            </div>
            <div class="login-box">
                <form class="login-form" action="{{ route('login') }}" method="post">
                    @csrf
                    <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Inicie Session</h3>
                    <div class="form-group">
                        <label class="control-label">CORREO</label>
                        <input  id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="control-label">PASSWORD</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <!-- <div class="form-group">
                        <div class="utility">
                            <div class="animated-checkbox">
                                <label>
                                    <input type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><span class="label-text">remember</span>
                                </label>
                            </div>
                        </div>
                    </div>-->   
                    </br>   

                     <div class="form-group btn-container">
                        <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>INGRESAR</button>
                    </div>
                </form>
            </div>
        </section>
        <!-- Essential javascripts for application to work-->
        <script src="{{ asset('plugins') }}/js/jquery-3.2.1.min.js"></script>
        <script src="{{ asset('plugins') }}/js/popper.min.js"></script>
        <script src="{{ asset('plugins') }}/js/bootstrap.min.js"></script>
        <script src="{{ asset('plugins') }}/js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="{{ asset('plugins') }}/js/plugins/pace.min.js"></script>
        <script type="text/javascript">
            // Login Page Flipbox control
            $('.login-content [data-toggle="flip"]').click(function () {
                $('.login-box').toggleClass('flipped');
                return false;
    });
        </script>
    </body>
</html>